#! /bin/bash
## Setup Python Packages

# Home Folder should be set to the top folder in the repo
_HOME_FOLDER=$PWD

install_ds_python_packages(){
  pip3 install -r ${_HOME_FOLDER}/scripts/linux/python/ds_requirements.txt 
  python3 -m spacy download en_core_web_sm
  python3 -m spacy download en
  python3 -m spacy download en_core_web_md
  python3 -c "import nltk;nltk.download('punkt');nltk.download('averaged_perceptron_tagger');nltk.download('wordnet')"
}

install_python_packages(){
  pip3 install --no-cache-dir -r ${_HOME_FOLDER}/scripts/linux/python/requirements.txt 
}
