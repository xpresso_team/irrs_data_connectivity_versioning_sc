#! /bin/bash
## Script is used to install packages in an ubuntu VM
## Global Argument:
##    ${1} -- Command to run. First argument is called assuming it to be a valid shell function

# Setting exit on error fail
set -e

_DISTRIBUTION=$(. /etc/os-release;echo $ID$VERSION_ID)
_HOME_FOLDER=$PWD

_FUNCTION_TO_RUN=${1}

if [[ ${_DISTRIBUTION} == "ubuntu*" ]]; then
  echo "${_DISTRIBUTION} is not supported currently. We only support ubuntu based distribution"
fi


function update_and_upgrade(){
  apt-get -y update && apt-get -y upgrade
}

function install_essentials(){
  apt-get -y install build-essential \
                          vim \
                          curl \
			                    libcurl4-openssl-dev \
                          libssl-dev \
                          libffi-dev \
                          git \
                          wget \
                          tmux \
                          libfreetype6 \
                          apt-transport-https \
                          ca-certificates \
                          curl \
                          software-properties-common \
                          locales \
                          pkg-config \
                          nano \
                            cmake \
                          libxml2-dev \
                          libxmlsec1-dev
}

function install_docker(){
  # Update local cache repo for docker and nvidia-dokcer
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
  apt-key add -
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
  
  curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
  apt-key add -
  curl -s -L https://nvidia.github.io/nvidia-docker/$_DISTRIBUTION/nvidia-docker.list | \
  tee /etc/apt/sources.list.d/nvidia-docker.list
  apt-get -y update
  apt-cache policy docker-ce
  # Install docker and nvidia docker
  apt-get install -y docker-ce nvidia-docker2

  echo "Installing docker-compose"
  curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose

  echo "Installing docker-compose completed"
}

# Installing both python2.7 and python3. But setting python3 as default
function install_python(){
  apt-get install -y python python-pip python3 python3-pip libpython-dev libpython-stdlib \
                       python-numpy python-dev python3-dev libpython2.7 libpython2.7-dev

  pip install --upgrade pip
  pip3 install --upgrade pip
}

function install_common_dev(){
  apt-get install -y apache2 libapache2-mod-wsgi nginx
  source ${_HOME_FOLDER}/scripts/linux/python/install-python-packages.sh
  install_python_packages
}

# TODO this requires testing on GPU machines only.
function install_nvidia-setup(){

  export CUDNN_VERSION=7.1.12
  apt-get purge -y nvidia*
  # Note this might remove your cuda installation as well
  apt-get -y autoremove

  # Install dependencies and cuda drivers
  apt-get install -y freeglut3 freeglut3-dev libxi-dev libxmu-dev
  wget https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux
  sh cuda_9.0.130_410.48_linux

  # Insall CudaNN libraries as well
  apt-get install -y libcudnn7-dev=$CUDNN_VERSION-1+cuda9.0 -y
                          libcudnn7=${CUDNN_VERSION}-1+cuda9.0
  ldconfig
  export PATH=$PATH:/usr/local/cuda/bin
  export CUDADIR=/usr/local/cuda

  # Setting nvidia in persistent mode.
  systemctl status nvidia-persistenced

}

function install_datascience_dev(){
  apt-get -y install ipython ipython-notebook
  source ${_HOME_FOLDER}/scripts/linux/python/install-python-packages.sh
  install_ds_python_packages
}

# Computer vision dependencies
function install_cv(){

   # Install ImageMagic
   apt-get install -y imagemagick python3-opencv

   # Check if installed correctly
   python3 -c "\
    import cv2
    print(cv2.__version__)"
}

function install_s2i(){

  # Install dependencies
  apt-get install -y go
  go get github.com/openshift/source-to-image/cmd/s2i

}

function install_kubeflow(){

  # Install KSonnet
  wget -O ks.tar.gz  https://github.com/ksonnet/ksonnet/releases/download/v0.13.1/ks_0.13.1_linux_amd64.tar.gz
  tar zxvf  ks.tar.gz;
  mv ks/ks /usr/bin/
  chown root:root /usr/bin/ks
  chmod a+x /usr/bin/ks

  # Install kubeflow
  ks registry add kubeflow github.com/google/kubeflow/tree/master/kubeflow
  ks pkg install kubeflow/core
  ks pkg install kubeflow/tf-serving
  ks pkg install kubeflow/tf-job

  # Installing Seldon as part of Kubeflow
  ks pkg install kubeflow/seldon
  ks generate seldon seldon

}

# Called the required functions
${_FUNCTION_TO_RUN}
