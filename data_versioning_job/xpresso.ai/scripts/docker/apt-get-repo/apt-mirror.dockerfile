FROM httpd:2

# ARG PRIVATEKEY_PATH
# ARG KEYID
# ARG PASSPHRASE

ENV TZ=US/Pacific

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone
#RUN apt-get update -y && apt-get -y install apt-rdepends \
#      apache2 dpkg-dev dpkg-sig expect apt-utils

# COPY scripts/linux/ubuntu/apt-get-repo/download-dependencies.sh /tmp/
# COPY scripts/linux/ubuntu/apt-get-repo/package-list.txt /tmp/

RUN mkdir -p /usr/local/apache2/htdocs/deb
WORKDIR /usr/local/apache2/htdocs/deb

# RUN bash /tmp/download-dependencies.sh /tmp/package-list.txt
# COPY ${PRIVATEKEY_PATH} /tmp/
# RUN gpg --import --batch /tmp/${PRIVATEKEY_PATH} && \
#    expect -c "spawn gpg --edit-key ${KEYID} trust quit; send \"5\ry\r\"; expect eof" && \
#    dpkg-sig -g "--pinentry-mode loopback --passphrase ${PASSPHRASE}" --sign builder *.deb && \
#    apt-ftparchive packages . > Packages  && \
#    gzip -c Packages > Packages.gz && \
#    apt-ftparchive release . > Release && \
#    gpg --pinentry-mode loopback --passphrase ${PASSPHRASE} --clearsign -o InRelease Release  && \
#    gpg --pinentry-mode loopback --passphrase ${PASSPHRASE} -abs -o Release.gpg Release
# RUN rm -r /tmp/download-dependencies.sh /tmp/package-list.txt /var/lib/apt/lists/*

# Make sure to copy gpg public key into key file in the home directory
ADD ./public_key /usr/local/apache2/htdocs/

# EXPOSE 80
# COPY scripts/docker/apt-get-repo/docker-entrypoint.sh /
# ENTRYPOINT [ "sh", "/docker-entrypoint.sh" ]
