from setuptools import setup
from xpresso.ai.core.utils.generic_utils import get_version


# Setup configuration
setup(
    name='xprctl',
    version=get_version(),
    packages=['xpresso'],
    entry_points={
        'console_scripts': [
            'xprctl = xpresso.ai.admin.controller.client.xprctl:cli_options'
        ]
    },
    description="Python command line application bare bones template.",
    author="Naveen Sinha",
    author_email="naveen.sinha@abzooba.com",
    install_requires=[
        'atlassian-python-api',
        'python-jenkins',
        'paramiko>=2.4.1',
        'networkx>=2.0',
        'matplotlib>=3.0.2',
        'tqdm',
        'docker>=3.7.0',
        'certifi>=2019.3.9',
        'chardet>=3.0.4',
        'docker>=3.7.0',
        'docker-pycreds>=0.4.0',
        'idna>=2.8',
        'requests>=2.21.0',
        'six>=1.12.0',
        'urllib3>=1.24.1',
        'websocket-client>=0.55.0',
        'limits>=1.3',
        'pylogbeat>=1.0.2',
        'python-logstash-async>=1.5.0',
        'six>=1.12.0',
        'passlib>=1.7.1',
        'pytest>=4.3.1',
        'pytest-cov>=2.6.1',
        'pymongo>=3.7.2',
        'Flask>=1.0.2',
        'Click>=7.0',
        'Beaker>=1.10.1',
        'simplejson>=3.16.0',
    ]
)