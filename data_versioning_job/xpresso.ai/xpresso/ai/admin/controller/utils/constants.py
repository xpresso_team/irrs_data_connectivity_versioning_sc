""" Stores constant variable """

# General constants to be used anywhere
HTTP_OK = 200
permission_755 = 0o755
log_path_flag = 'log'
config_path_flag = 'config'

# Pipeline constants
variable_indicator = "##"
PROJECTS_SECTION = 'projects'
DECLARATIVE_PIPELINE_FOLDER = 'declarative_pipeline_folder'
KUBEFLOW_TEMPLATE = 'kubeflow_template'
open_parenthesis = '('
double_quote = '"'
escape_quote = '\\"'

# Local test repo for testing skeleton string replace
LOCAL_TEST_REPO = "skeleton-build"

# DB Setup
replset_initiate_cmd = "replSetInitiate"

# Bundle Input Json
bundle_local = "local"
bundle_cluster = "cluster"
xpresso_cluster_bundle = "XpressoClusterBundle"

# Kubernetes Constants
pv_size = "10Gi"
type_pvc = "volume-claim"
type_pv = "volume"
pipeline_pv_prefix = "pipeline"
DEPLOYMENT_FILES_FOLDER = 'deployment_files_folder'
