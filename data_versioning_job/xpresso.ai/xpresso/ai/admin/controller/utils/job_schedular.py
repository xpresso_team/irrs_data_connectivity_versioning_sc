import logging
import schedule

# A scheduling class with a method that can execute any function on every interval


class XpressoSchedule:

    # schedules function(function_name) after interval(interval_time)
    def process_periodically(self, function_name):
        try:
            schedule.every().day.do(function_name)
            return True
        except Exception as err:
            logging.info(err)
