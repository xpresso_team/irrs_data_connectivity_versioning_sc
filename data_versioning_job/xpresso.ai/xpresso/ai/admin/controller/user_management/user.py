import re

from xpresso.ai.admin.controller.xprobject import XprObject
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.admin.controller.authentication.tokeninfo import TokenInfo
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    PasswordStrengthException, EmailFormatException, EmailDomainException, \
    UidFormatException


class User(XprObject):
    """
    This class represents a User
    """
    def __init__(self, user_json=None):
        self.logger = XprLogger()
        """
        Constructor:
        """
        self.logger.debug(f"User constructor called with {user_json}")
        super().__init__(user_json)
        self.logger.info(f"user info : {self.data}")
        # These are mandatory fields that needs to be provided in user_json
        self.mandatory_fields = [
            "uid", "pwd", "firstName", "lastName",
            "email", "primaryRole"
        ]

        # primaryRole of a user has to one of these
        self.valid_values = {"primaryRole": ["Dev", "PM", "DH", "Admin", "Su"]}

        # fields that cannot be modified
        self.unmodifiable_fields = ["pwd"]

        # allowed domains in email
        # TODO: Make this configurable based on Project
        self.allowed_domains = ["abzooba.com", "gmail.com", "outlook.com"]

        # fields that should be displayed in the output
        self.display_fields = ["uid", "firstName", "lastName", "email",
                               "primaryRole", "nodes", "activationStatus"]
        self.logger.debug("User constructed successfully")

    def get_token_info(self):
        token_info = TokenInfo(self.data['token'])
        token_info.token = self.get("token")
        token_info.token_expiry = self.get("tokenExpiry")
        token_info.login_expiry = self.get("loginExpiry")
        return token_info

    def validate_field_values(self):
        """
        checks if all the fields have valid input values
        and throws exception in case of invalid values
        """
        super().validate_field_values()
        if "email" in self.data:
            self.check_email()
        if "uid" in self.data:
            self.check_uid()

    @staticmethod
    def check_password(password):
        """
        checks if the password is secure enough

        :param password:
            password as a string
        """
        if len(password) < 6:
            raise PasswordStrengthException("Password is too short")
        reg_exp = "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|"\
                  "((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})"
        match = re.search(reg_exp, password)
        if not match:
            raise PasswordStrengthException(
                "Password is weak. Choose a strong password")

    def check_email(self):
        """
        checks if the email provided is in valid format

        checks the email format, local part & domain part of the
        email and throws exception if it doesn't follow specified standard

        :param email:
            (str) email of the user
        """
        email = self.data["email"]
        email_regex = ".+@.+"
        if not re.match(email_regex, email):
            raise EmailFormatException()
        # split the email in local part & domain part
        local_name, domain_name = email.split("@", 1)
        if domain_name not in self.allowed_domains:
            raise EmailDomainException()

        # local name regex rules:
        # 1)local name should not start or end with period(.)
        # 2)local name length should not be greater than 64
        # 3)local name should not contain '@'
        local_name_regex = "^[^.][^@]{0,62}[^.]$"
        if not re.match(local_name_regex, local_name):
            raise EmailFormatException()

    def check_uid(self):
        """
            checks if the uid provided is in valid format
        """
        # uid format rules
        # 1) should not be greater than 36 characters
        # 2) should start with a alphabetic character
        # 3) can only comprise of alphanumeric characters and a period
        uid_regex = "^[a-zA-Z]{1,16}[.0-9]{0,4}[a-zA-Z]{1,16}$"
        uid = self.data["uid"]
        if not re.match(uid_regex, uid):
            raise UidFormatException()
