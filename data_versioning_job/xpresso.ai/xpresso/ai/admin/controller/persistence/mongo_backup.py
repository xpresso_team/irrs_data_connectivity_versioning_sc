import logging
import subprocess
from pymongo import errors
from pymongo import MongoClient


class MongoBackup:
    # a method that connects to mongodb and takes a dump of complete database into a local file and returns path of it
    def backup(self, database):
        try:
            client = MongoClient('mongodb://172.16.3.1:27017')
            connection = client.server_info()['ok']                     # return 1 if connection is okay
            host = '172.16.3.1'
            path = '/home/abzooba/Desktop/backup'
            port = '27017'
            # subprocess.call(["mkdir", path])
            if database == 'xprdb':
                logging.info(f'Backing up {database} from {host} to {path}')
                mongodumpvalue = subprocess.call(["mongodump", "--host", host, "--port", port, "-d", "xprdb", '-u', 'xprdb_admin', '-p',
                                 "xprdb@Abz00ba", "--out", path])
                # returns 0 if call is successful
                print("Backup done")
                return connection, mongodumpvalue
            else:
                logging.info(f'Backing up {database} from {host} to {path}')
                mongodumpvalue = subprocess.call(["mongodump", "--host", host, "--port", port, "-d", database, "--out", path])
                print("Backup done")
                return connection, mongodumpvalue

        except errors.ServerSelectionTimeoutError:
            logging.error(f"Unable to connect to 172.16.3.1.")

        except errors.InvalidName:
            logging.error(f"Database {database} is invalid.")

        except errors.ConfigurationError as err:
            logging.error(err)

        except errors.OperationFailure:
            logging.error("Database operation timed out.")
