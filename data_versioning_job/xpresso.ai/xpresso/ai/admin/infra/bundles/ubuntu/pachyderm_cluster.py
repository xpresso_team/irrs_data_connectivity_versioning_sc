"""
package to deploy pachyderm in a kubernetes cluster
"""

__all__ = ["PachydermPackage"]
__author__ = ["Gopi Krishna"]

import argparse
from os.path import join as path_join
from time import sleep
import yaml

from xpresso.ai.admin.infra.bundles.abstract_bundle import AbstractBundle
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils import linux_utils
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import *
from xpresso.ai.admin.infra.bundles.ubuntu.kubernetes.kubectl_wrapper import \
    KubernetesClient
from xpresso.ai.admin.infra.bundles.ubuntu.helm_client_wrapper import \
    HelmClient


class PachydermPackage(AbstractBundle):
    """
    Installs Pachyderm on the kubernetes cluster

    Installs pachyderm client, pachyderm package using helm chart
    then deploys it onto the kubernetes cluster
    """

    ENV = "env"
    BUNDLES = "bundles_setup"
    PACHYDERM = "pachyderm"
    KUBECTL = "kubectl"
    KUBEADM = "kubeadm"
    MINIO_NAMESPACE = "minio_namespace"
    DEFAULT_KUBERNETES_CLUSTER = "kubernetes"
    DEFAULT_KUBERNETES_USER = "kubernetes-admin"
    MINIO_SPEC_PATH = "minio_spec_path"
    MINIO_PV_FILE_NAME = "minio-pv.yaml"
    MINIO_NFS_MOUNT_PATH = "minio_pv_nfs_mount_path"
    ENV_REF_IN_MINIO_PV_SPEC = "ENV"
    HELM_CHART_NAME = "stable/pachyderm"
    PACHYDERM_SPEC_PATH = "pachyderm_spec_path"
    PACHYDERM_NAMESPACE = "pachyderm_namespace"
    PACHYDERM_RELEASE = "release_name"
    METADATA = "metadata"
    NAME_IN_METADATA = "name"
    PV = "pv"
    SPEC = "spec"
    PATH = "path"
    NFS = "nfs"
    GET_PACHCTL_COMMAND = "$ curl -o /tmp/pachctl.deb -L {PACHCTL_DEB_PACKAGE} " \
                          "&& sudo dpkg -i /tmp/pachctl.deb"
    PACHCTL_PKG_LINK = "pachctl_pkg_link"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 executor=None):
        """
        PachydermPackage class constructor method

        :param config_path:
            path of the XprConfigParser file
        :param executor:
            executor object
        """
        if not executor:
            executor = LocalShellExecutor()

        super().__init__(executor)
        self.env = XprConfigParser(config_path)[self.ENV]
        self.config = XprConfigParser(config_path)[self.BUNDLES][self.PACHYDERM]
        self.kubernetes_client = KubernetesClient()
        self.helm_client = HelmClient()

    def environment_check(self, logger):
        """
        checks if the environment is stable for setting up pachyderm

        :param logger:
            logger object is provided as a parameter
        """
        if not linux_utils.check_root():
            raise PachydermEnvironmentException("Login as root and try again")
        logger.info("Checking if kubernetes tools are installed")
        if not linux_utils.check_if_a_command_installed(self.KUBECTL) or \
                not linux_utils.check_if_a_command_installed(self.KUBEADM):
            logger.error("Kubernetes is not setup")
            raise PachydermEnvironmentException("Setup kubernetes and try again")
        # checks if current system is a kubernetes master node
        try:
            logger.info("Checking if the package is being run on master node")
            master_node_check = self.kubernetes_client.check_master_node()
            if master_node_check:
                # return code is not zero
                logger.error("package setup needs to be done on master node")
                raise PachydermEnvironmentException(
                    "Please run the setup on kubernetes master node")
        # check if ports 39495, 2379 & 30650 are open
        # 39495 - minio service, 2379 - etcd service, 30650 - pachyderm service

        except CommandExecutionFailedException:
            logger.error("master-node check command failed")
            raise PachydermEnvironmentException(
                "Please run the setup on kubernetes master node"
            )

    def prerequisite_package_setup(self, logger):
        """
        installs required prerequisite packages to start a pachyderm cluster

        installs helm &  minio object store
        :param logger:
            logger object provided as a parameter
        :return:
        """
        self.helm_client.setup_helm()
        # send process to sleep for 20 seconds for helm setup & tiller deployment
        sleep(20)
        # setting up new namespace & context for minio setup
        logger.info("fetching current kubernetes context")
        current_context = self.kubernetes_client.get_current_context()
        new_namespace = self.config[self.MINIO_NAMESPACE]
        logger.info("creating new namespace for pachyderm cluster")
        self.kubernetes_client.create_namespace(new_namespace)
        temp_context = "temp_context"
        logger.info("create new context to deploy minio store")
        self.kubernetes_client.create_new_context(
            temp_context, new_namespace,
            self.DEFAULT_KUBERNETES_CLUSTER, self.DEFAULT_KUBERNETES_USER
        )
        logger.info("updating current context for minio setup")
        self.kubernetes_client.update_context(temp_context)
        # sets up minio store
        self.minio_setup(logger)
        # delete temp context
        self.kubernetes_client.delete_context(temp_context, current_context)

    def minio_setup(self, logger):
        """
        sets up minio store as backend storage for pachyderm

        :param logger:
            logger is provided as a parameter
        """
        try:
            minio_pv_spec_path = path_join(
                self.config[self.MINIO_SPEC_PATH],
                self.MINIO_PV_FILE_NAME
            )
            minio_pv_nfs_mount_path = self.config[self.MINIO_NFS_MOUNT_PATH]
            minio_pv_nfs_mount_path = minio_pv_nfs_mount_path.replace(
                self.ENV_REF_IN_MINIO_PV_SPEC, self.env
            )
            with open(minio_pv_spec_path, "r") as spec_file:
                spec_content = yaml.safe_load(spec_file)

            spec_content[self.SPEC][self.NFS][self.PATH] = minio_pv_nfs_mount_path

            with open(minio_pv_spec_path, "w+") as yaml_file:
                yaml.safe_dump(spec_content, yaml_file)

        except (FileNotFoundError, yaml.YAMLError) as err:
            logger.error(err)
            raise MinioSetupException()
        try:
            logger.info("setting up minio object store")
            self.kubernetes_client.create_resource(
                self.config[self.MINIO_SPEC_PATH],
                self.config[self.MINIO_NAMESPACE]
            )
            # wait for some time for minio setup to complete
            # send process to sleep for 60 seconds
            sleep(10)

        except CommandExecutionFailedException as err:
            logger.error("error while setting up minio is: \n", err)
            raise MinioSetupException()

    def execute(self):
        """
        entry-point for starting the Pachyderm installation process

        checks the environment stability, then installs and setup pre-requisite
        packages and then starts the pachyderm cluster using helm
        """
        logger = XprLogger()
        # check if environment is stable for pachyderm setup
        self.environment_check(logger)
        # install pre-requisite packages
        self.prerequisite_package_setup(logger)
        # start pachyderm cluster using helm chart
        logger.info("Starting pachyderm cluster")
        self.start()
        pachctl_pkg = self.GET_PACHCTL_COMMAND.replace(
            "{PACHCTL_DEB_PACKAGE}",
            self.config[self.PACHCTL_PKG_LINK]
        )
        logger.info("Downloading pachyderm terminal client(pachctl)")
        self.executor.execute(pachctl_pkg)

    def status(self):
        pass

    def get_resource_name(self, yaml_path):
        """

        :param yaml_path:
        :return:
        """
        try:
            with open(yaml_path, 'r') as yaml_file:
                yaml_data = yaml.safe_load(yaml_file)
                # name is fetched from metadata inside yaml spec
                return yaml_data[self.METADATA][self.NAME_IN_METADATA]
        except (yaml.YAMLError, FileNotFoundError) as err:
            raise PachydermRemovalException(str(err))

    def remove_helm(self):
        """
        removes helm from the system
        """
        # TODO: check if any other release is present on helm, then leave it
        try:
            # undeploy tiller from cluster
            self.helm_client.stop_helm()
            # wait for tiller to get un-deployed
            sleep(15)
            self.helm_client.delete_helm()
        except CommandExecutionFailedException as err:
            raise HelmRemovalException(str(err))

    def remove_minio(self):
        """
        removes minio store from the cluster
        """
        try:
            # deleting namespace deletes service, deployment, pods & pvc
            self.kubernetes_client.delete_namespace(
                self.config[self.MINIO_NAMESPACE]
            )
            sleep(10)
            minio_pv = self.get_resource_name(
                path_join(self.config[self.MINIO_SPEC_PATH],
                          self.MINIO_PV_FILE_NAME)
            )
            # TODO: Manually delete the used storage by pv
            self.kubernetes_client.delete_resource(self.PV, minio_pv)
            sleep(10)
        except CommandExecutionFailedException as err:
            raise MinioRemovalException(str(err))

    def cleanup(self):
        """
        cleanup the cluster after deleting pachyderm

        removes helm, snap and minio store
        """
        # first remove helm charts from system
        self.remove_helm()
        # remove minio store and its resources
        self.remove_minio()

    def install(self):
        """
        starts the pachyderm installation process
        """
        self.execute()

    def uninstall(self):
        """
        uninstalls pachyderm from the cluster and removes other packages
        setup with it
        """
        # stops running pachyderm cluster
        self.stop()
        # cleans up installed pre-requisite packages
        self.cleanup()

    def upgrade(self):
        pass

    def start(self):
        """
            starts the pachyderm cluster using helm chart
        """
        # all pachyderm services and pods are deployed in this step
        try:
            self.helm_client.install_new_chart(
                self.HELM_CHART_NAME,
                self.config[self.PACHYDERM_NAMESPACE],
                self.config[self.PACHYDERM_RELEASE],
                self.config[self.PACHYDERM_SPEC_PATH]
            )
        except CommandExecutionFailedException as err:
            raise PachydermSetupException("Pachyderm installation failed")

    def stop(self):
        """
        stops pachyderm cluster by removing the release using helm
        """
        # Deleting pachyderm release from kubernetes using helm
        try:
            self.helm_client.delete_chart(self.config[self.PACHYDERM_RELEASE])
        except CommandExecutionFailedException as err:
            raise PachydermRemovalException(
                "Error while deleting helm release of pachyderm"
            )


def parse_arguments():
    """ Reads commandline argument to identify which clients group to
    install
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--run",
                        required=True,
                        help="Method to execute")

    return parser


if __name__ == "__main__":
    pachyderm_package = PachydermPackage()
    arg_parser = parse_arguments()
    args = arg_parser.parse_args()
    if 'run' in args:
        if args.run == 'execute':
            pachyderm_package.install()
        elif args.run == 'clean':
            pachyderm_package.cleanup()
        elif args.run == 'stop':
            pachyderm_package.stop()
        elif args.run == 'uninstall':
            pachyderm_package.uninstall()
        else:
            print("Unknown argument. Please provide valid argument")
    else:
        pachyderm_package.install()
