"""Abstract base class for bundles object"""
from xpresso.ai.admin.controller.db.db_setup import XprDbSetup
from xpresso.ai.admin.infra.bundles.abstract_bundle import AbstractBundle
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils import linux_utils
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

__all__ = ['JenkinsKubernetesBundle']
__author__ = 'Naveen Sinha'


class JenkinsKubernetesBundle(AbstractBundle):
    """
    Installs Jenkins on kubernetes
    """

    PARAMETER_ADDRESS_NAME = "address"
    PARAMETER_KEY = "parameters"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 executor=None):
        if not executor:
            executor = LocalShellExecutor()
        super().__init__(executor)
        self.config = XprConfigParser(config_path)["bundles_setup"]

    def status(self, **kwargs):
        """
        Checks the status of existing running application

        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug("Checking current status of jenkins")
        check_jenkins_up = "kubectl get deployment xpresso-jenkins -n xpresso"
        rc = self.executor.execute(check_jenkins_up)
        if rc != 0:
            self.logger.debug("Jenkins is not installed")
            return False
        self.logger.debug("Status check succeeded")
        return True

    def get_address_from_parameter(self, **kwargs):
        address = ""
        if (self.PARAMETER_KEY in kwargs and kwargs[self.PARAMETER_KEY] and
                self.PARAMETER_ADDRESS_NAME in kwargs[self.PARAMETER_KEY]):
            address = kwargs[self.PARAMETER_KEY][self.PARAMETER_ADDRESS_NAME]

        return address

    def install(self, **kwargs):
        """
        Install Jenkins locally
        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug(kwargs)
        logger = XprLogger()
        if not linux_utils.check_root():
            logger.fatal("Please run this as root")

        jenkins_setup = "kubectl create -f jenkins-kubernetes.yaml"
        self.executor.execute(jenkins_setup)
        self.logger.debug("Jenkins has been installed")
        return True

    def uninstall(self, **kwargs):
        """
        Removes Jenkins from server
        Returns:
            True, if uninstall is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug("Uninstalling jenkins from the server")
        remove_jenkins = "kubectl delete persistentvolume/xpresso-jenkins-pv " \
                         "persistentvolumeclaim/xpresso-jenkins-pvc " \
                         "deployment.extensions/xpresso-jenkins " \
                         "service/xpresso-jenkins -n xpresso"
        self.executor.execute(remove_jenkins)
        self.logger.debug("Jenkins un-installation completed")
        return True

    def start(self, **kwargs):
        """ Start docker service for Kong """
        return True

    def stop(self, **kwargs):
        """ Stop the docker service for Kong """
        return True


if __name__ == "__main__":
    jenkins_bundle = JenkinsKubernetesBundle()
    jenkins_bundle.install()
