"""Abstract base class for bundles object"""
from xpresso.ai.admin.controller.db.db_setup import XprDbSetup
from xpresso.ai.admin.infra.bundles.abstract_bundle import AbstractBundle
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

__all__ = ['MongoBundle']
__author__ = 'Naveen Sinha'


class MongoBundle(AbstractBundle):
    """
    Installs Mongo service locally
    """

    PARAMETER_ADDRESS_NAME = "address"
    PARAMETER_KEY = "parameters"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 executor=None):
        if not executor:
            executor = LocalShellExecutor()
        super().__init__(executor)
        self.config = XprConfigParser(config_path)["bundles_setup"]

    def status(self, **kwargs):
        """
        Checks the status of existing running application

        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug("Checking current status of mongo")
        rc, stdout, stderr = self.executor.execute_with_output(
            command="systemctl status mongod")
        if rc != 0:
            return False
        self.logger.debug("Status check succeeded")
        return True

    def get_address_from_parameter(self, **kwargs):
        address = ""
        if (self.PARAMETER_KEY in kwargs and kwargs[self.PARAMETER_KEY] and
            self.PARAMETER_ADDRESS_NAME in kwargs[self.PARAMETER_KEY]):
            address = kwargs[self.PARAMETER_KEY][self.PARAMETER_ADDRESS_NAME]

        return address

    def install(self, **kwargs):
        """
        Install Mongodb locally
        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug(kwargs)
        if self.status():
            self.logger.debug("Mongodb already installed")
            return True
        address = self.get_address_from_parameter(**kwargs)
        self.logger.debug(address)
        xpr_db_setup = XprDbSetup()
        xpr_db_setup.install_mongo(address=address)
        xpr_db_setup.enable_replication()
        self.logger.debug("Mongodb has been installed")
        return True

    def uninstall(self, **kwargs):
        """
        Removes mongodb from server
        Returns:
            True, if uninstall is successful. False Otherwise
        Raises:
            BundleFailedException
        """

        self.logger.debug("Uninstalling mongodb from the server")
        self.executor.execute(command='systemctl stop mongod')
        self.executor.execute(command='apt-get remove -y mongodb-org')
        self.logger.debug("MongoDB un-installation completed")
        return True

    def start(self, **kwargs):
        """ Start docker service for Kong """
        return True

    def stop(self, **kwargs):
        """ Stop the docker service for Kong """
        return True


if __name__ == "__main__":
    mongo_bundle = MongoBundle()
    mongo_bundle.install()
