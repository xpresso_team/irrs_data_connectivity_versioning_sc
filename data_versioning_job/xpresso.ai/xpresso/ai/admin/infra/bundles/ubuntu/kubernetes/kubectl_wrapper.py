"""
wrapper class for kubectl commands
"""

__all__ = ["KubernetesClient"]
__author__ = ["Gopi Krishna"]

from copy import deepcopy

from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor


class KubernetesClient:
    """
    A python wrapper to execute kubectl commands locally
    """

    GET_NODES_COMMAND = "kubectl get nodes"
    CREATE_NAMESPACE_COMMAND = "kubectl create namespace {NAMESPACE}"
    GET_CURRENT_CONTEXT = "kubectl config current-context"
    ENCODING = "utf-8"
    NEWLINE = "\n"
    NEW_CONTEXT_COMMAND = "kubectl config set-context {NEW_CONTEXT} "\
                          "--namespace={NAMESPACE} "\
                          "--cluster={CLUSTER} --user={USER}"
    UPDATE_CONTEXT_COMMAND = "kubectl config use-context {CONTEXT_NAME}"
    DEFAULT_KUBERNETES_CONTEXT = "kubernetes-admin@kubernetes"
    DELETE_CONTEXT_COMMAND = "kubectl config delete-context {CONTEXT_NAME}"
    CREATE_RESOURCE_COMMAND = "kubectl create -f {SPEC_PATH} -n {NAMESPACE}"
    DELETE_RESOURCE_COMMAND = "kubectl delete {RESOURCE_TYPE} {RESOURCE_NAME} "\
                              "-n {NAMESPACE}"
    DEFAULT_KUBERNETES_NAMESPACE = "default"
    DELETE_NAMESPACE_COMMAND = "kubectl delete namespace {NAMESPACE}"

    def __init__(self):
        self.executor = LocalShellExecutor()

    def check_master_node(self):
        """
        checks if the current system is a master node or not

        :return:
        """
        # status will be zero in case iff it is a master node
        status = self.executor.execute(self.GET_NODES_COMMAND)
        return status

    def create_namespace(self, name):
        """
        creates a new namespace on kubernetes cluster
        :param name:
            name of the namespace
        :return:
        """
        create_namespace_command = deepcopy(self.CREATE_NAMESPACE_COMMAND)
        create_namespace_command = create_namespace_command.replace(
            "{NAMESPACE}", name)
        self.executor.execute(create_namespace_command)

    def get_current_context(self):
        """
        checks current context on kubernetes cluster

        :return:
            returns current context as a string
        """
        status, stdout, stderr = self.executor.execute_with_output(
            self.GET_CURRENT_CONTEXT)
        # decode the stdout to string format as terminal return
        current_context = stdout.decode(self.ENCODING)
        # terminal adds newline at the end of output, hence removing it if present
        current_context = current_context.rstrip(self.NEWLINE)
        return current_context

    def create_new_context(self, new_context, namespace, cluster, user):
        """
        creates a new context & namespace for pachyderm cluster

        :param new_context:
            new context name as a string
        :param namespace:
            name of the namespace
        :param cluster:
            name of the cluster
        :param user:
            name of the user
        """
        new_context_command = deepcopy(self.NEW_CONTEXT_COMMAND)
        new_context_command = new_context_command.replace("{NEW_CONTEXT}",
                                                          new_context)
        new_context_command = new_context_command.replace("{NAMESPACE}",
                                                          namespace)
        new_context_command = new_context_command.replace("{CLUSTER}", cluster)
        new_context_command = new_context_command.replace("{USER}", user)
        self.executor.execute(new_context_command)

    def update_context(self, context_name):
        """
        updates current context to new_context

        :param context_name:
            context that needs to be set as current context
        """
        update_context_command = deepcopy(self.UPDATE_CONTEXT_COMMAND)
        update_context_command = update_context_command.replace("{CONTEXT_NAME}",
                                                                context_name)
        self.executor.execute(update_context_command)

    def delete_context(self, current_context, new_context):
        """
        deletes the provided context and updates current context to new_context

        :param current_context:
            context that needs to be deleted
        :param new_context:
            context that needs to be set as current context
        """
        # first update the current context to new one provided
        self.update_context(new_context)
        if current_context != self.DEFAULT_KUBERNETES_CONTEXT:
            delete_context_command = deepcopy(self.DELETE_CONTEXT_COMMAND)
            delete_context_command = delete_context_command.replace(
                "{CONTEXT_NAME}", current_context)
            self.executor.execute(delete_context_command)

    def create_resource(self, spec_path, namespace="default"):
        """
        creates a resource from file or directory

        :param spec_path:
            path to the spec file or directory
        :param namespace:
            name of the namespace to be deployed
        """
        create_command = deepcopy(self.CREATE_RESOURCE_COMMAND)
        create_command = create_command.replace("{SPEC_PATH}", spec_path)
        create_command = create_command.replace("{NAMESPACE}", namespace)
        self.executor.execute(create_command)

    def delete_resource(self, resource_type, resource_name,
                        namespace="default"):
        """
        deletes a resource from kubernetes cluster

        :param resource_type:
            type of the resource
        :param resource_name:
            name of the resource
        :param namespace:
            name of the namespace
        """
        delete_resource_command = deepcopy(self.DELETE_RESOURCE_COMMAND)
        delete_resource_command = delete_resource_command.replace(
            "{RESOURCE_TYPE}", resource_type
        )
        delete_resource_command = delete_resource_command.replace(
            "{RESOURCE_NAME}", resource_name
        )
        delete_resource_command = delete_resource_command.replace(
            "{NAMESPACE}", namespace
        )
        self.executor.execute(delete_resource_command)

    def delete_namespace(self, namespace):
        """
        deletes the namespace and its resources

        :param namespace:
        name of the namespace to be deleted
        """
        if namespace != self.DEFAULT_KUBERNETES_NAMESPACE:
            delete_namespace_command = deepcopy(self.DELETE_NAMESPACE_COMMAND)
            delete_namespace_command = delete_namespace_command.replace(
                "{NAMESPACE}", namespace
            )
            self.executor.execute(delete_namespace_command)

    @staticmethod
    def replace_string(current_string, **kwargs):
        """

        :param current_string:
        :param kwargs:
        :return:
        """
        new_string = current_string
        for key, val in kwargs:
            new_string = new_string.replace(key, val)
        return new_string
