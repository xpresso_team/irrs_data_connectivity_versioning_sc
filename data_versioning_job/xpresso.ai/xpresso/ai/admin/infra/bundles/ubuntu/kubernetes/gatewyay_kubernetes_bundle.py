"""Abstract base class for bundles object"""
from xpresso.ai.admin.infra.bundles.abstract_bundle import AbstractBundle
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils import linux_utils
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

__all__ = ['GatewayKubernetesBundle']
__author__ = 'Naveen Sinha'


class GatewayKubernetesBundle(AbstractBundle):
    """
    Installs Gateway on kubernetes
    """

    PARAMETER_ADDRESS_NAME = "address"
    PARAMETER_KEY = "parameters"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 executor=None):
        if not executor:
            executor = LocalShellExecutor()
        super().__init__(executor)
        self.config = XprConfigParser(config_path)["bundles_setup"]

    def status(self, **kwargs):
        """
        Checks the status of existing running application

        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug("Checking current status of gateway")
        check_gateway_up = "kubectl get deployment xpresso-gateway -n xpresso"
        rc = self.executor.execute(check_gateway_up)
        if rc != 0:
            self.logger.debug("Gateway is not installed")
            return False
        self.logger.debug("Status check succeeded")
        return True

    def get_address_from_parameter(self, **kwargs):
        address = ""
        if (self.PARAMETER_KEY in kwargs and kwargs[self.PARAMETER_KEY] and
            self.PARAMETER_ADDRESS_NAME in kwargs[self.PARAMETER_KEY]):
            address = kwargs[self.PARAMETER_KEY][self.PARAMETER_ADDRESS_NAME]

        return address

    def install(self, **kwargs):
        """
        Install Gateway locally
        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug(kwargs)
        logger = XprLogger()
        if not linux_utils.check_root():
            logger.fatal("Please run this as root")

        gateway_setup = "kubectl create -f gateway-kubernetes.yaml"
        self.executor.execute(gateway_setup)
        self.logger.debug("Gateway has been installed")
        return True

    def uninstall(self, **kwargs):
        """
        Removes Gateway from server
        Returns:
            True, if uninstall is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.debug("Uninstalling gateway from the server")
        remove_gateway = "kubectl delete persistentvolume/xpresso-gateway-pv " \
                      "persistentvolumeclaim/xpresso-gateway-pvc " \
                      "deployment.extensions/xpresso-gateway " \
                      "service/xpresso-gateway -n xpresso"
        self.executor.execute(remove_gateway)
        self.logger.debug("Gateway un-installation completed")
        return True

    def start(self, **kwargs):
        """ Start docker service for Kong """
        return True

    def stop(self, **kwargs):
        """ Stop the docker service for Kong """
        return True


if __name__ == "__main__":
    gateway_bundle = GatewayKubernetesBundle()
    gateway_bundle.install()
