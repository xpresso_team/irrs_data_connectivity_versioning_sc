"""Responsible for installing the os packages in a machine. """

__all__ = ['BundleManager', 'ExecutionType']
__author__ = 'Naveen Sinha'

import sys
import inspect
import pkgutil
import platform
from enum import Enum

from xpresso.ai.admin.infra.bundles.bundle_dependency import BundleDependency
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.admin.infra import bundles
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import BundleFailedException
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    BundleNotSupportedException


class ExecutionType(Enum):
    """
    Enum class to standardize all the execution type
    """
    INSTALL = "install"
    UNINSTALL = "uninstall"
    START = "start"
    STOP = "stop"
    STATUS = "status"

    def __str__(self):
        return self.value


class BundleManager:
    """Manages the request for bundle installation and setup
    """

    MANIFEST_SCRIPT_KEY = "script_path"
    MANIFEST_MULTI_ARG_KEY = "script_multi_arguments"
    MANIFEST_DEPENDENCY_KEY = "dependency_bundles"
    MANIFEST_SSH_CONFIG = "ssh_config"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH):
        """
        1. Generate metadata of existing VM
        2. Reads the arguments and initiates the instance variable
        """

        self.logger = XprLogger()

        self.python_version = sys.version.split('\n')
        self.system = platform.system()
        self.machine = platform.machine()
        self.platform = platform.platform()
        self.uname = platform.uname()
        self.version = platform.version()
        self.arch = platform.architecture()
        self.config_path = config_path
        self.bundle_dependency = BundleDependency(config_path=config_path)

    def list(self):
        """ List all supported bundle group name

        Returns:
            list: list of available bundles
        """
        return self.bundle_dependency.list_all()

    def check_if_supported(self, bundle_to_install):
        """ Check if bundle name is supported or not """
        if not self.bundle_dependency.check_if_supported(bundle_to_install):
            self.logger.error("Unsupported Bundle Name : {}"
                              .format(bundle_to_install))
            raise BundleNotSupportedException()
        return True

    def run(self, bundle_to_install: str, execution_type: ExecutionType,
            parameters: dict = None):
        """
        Perform provided execution type on the given bundle name

        Args:
            parameters(dict): Additional parameters
            bundle_to_install(str): name of the bundle to install. Must match
                                     the supported bundle names
            execution_type(ExecutionType): Type of execution
        """
        if not self.check_if_supported(bundle_to_install=bundle_to_install):
            return False

        dependency_list = self.bundle_dependency.get_dependency(
            bundle_to_install)
        self.logger.info(dependency_list)
        response = self.execute_recursive(dependency_list, 0, execution_type,
                                          parameters=parameters)
        if not response:
            raise BundleFailedException("Bundle installation failed!!")
        self.logger.info("{} installed successfully".format(bundle_to_install))
        return True

    def execute_recursive(self, dependency_list: list, current_index: int,
                          execution_type: ExecutionType,
                          parameters: dict = None):
        """
        Execute  recursively. If something failed then rollback
        """
        if current_index >= len(dependency_list):
            return True

        bundle_string = dependency_list[current_index]
        self.logger.info(bundle_string)
        try:
            self.execute(
                bundle_class=self.bundle_str_to_class(
                    bundle_string,
                    bundles),
                execution_type=execution_type,
                parameters=parameters)
            current_index += 1
            return self.execute_recursive(dependency_list, current_index,
                                          execution_type, parameters=parameters)
        except BundleFailedException:
            self.logger.error("Failed to execute bundle {}"
                              .format(str(bundle_string)))
        return False

    def execute(self,
                bundle_class,
                execution_type: ExecutionType,
                parameters: dict = None):
        """
        Perform provided execution type on the given bundle name

        Args:
            parameters: Additional parameter required for installation
            bundle_class: name of the bundle to install.
                           Must match the supported bundle names.
            execution_type(ExecutionType): Type of execution
        Returns:
            bool: True,if the execution is successful
        """
        if bundle_class is None:
            self.logger.info("{} Not Found in the hirarchy".
                             format(bundle_class))
            return False
        self.logger.info(f"Running bundle {bundle_class} with parameters"
                         f"{parameters}")
        bundle_obj = bundle_class(config_path=self.config_path)
        if (execution_type == ExecutionType.INSTALL and
                bundle_obj.status(parameters=parameters)):
            return True
        elif execution_type == ExecutionType.INSTALL:
            return bundle_obj.install(parameters=parameters)
        elif execution_type == ExecutionType.UNINSTALL:
            return bundle_obj.uninstall(parameters=parameters)
        elif execution_type == ExecutionType.STATUS:
            return bundle_obj.status(parameters=parameters)
        elif execution_type == ExecutionType.START:
            return bundle_obj.start(parameters=parameters)
        elif execution_type == ExecutionType.STOP:
            return bundle_obj.stop(parameters=parameters)

        self.logger.error(str(bundle_obj) + " Not defined")
        return False

    @staticmethod
    def bundle_str_to_class(target_class_name: str, bundle_name):
        """ Converts class name into python class object. It looks for all
        classes in a bundle and matches the name to the class object

        Args:
            bundle_name(bundle): Find the target class name within this
                                   bundle name
            target_class_name(str): exact name of the class. It should match the
                                    class name as well

        Returns:
            Object: returns python class object, None otherwise
        """

        for _, modname, is_bundle in \
            pkgutil.iter_modules(bundle_name.__path__,
                                 bundle_name.__name__ + "."):
            imported_module = __import__(modname, fromlist=["dummy"])
            matched_class_name = None
            if is_bundle:
                matched_class_name = BundleManager.bundle_str_to_class(
                    target_class_name,
                    imported_module)
            if matched_class_name:
                return matched_class_name
            for name, obj in inspect.getmembers(imported_module):
                if inspect.isclass(obj) and name == target_class_name:
                    return obj
        return None
