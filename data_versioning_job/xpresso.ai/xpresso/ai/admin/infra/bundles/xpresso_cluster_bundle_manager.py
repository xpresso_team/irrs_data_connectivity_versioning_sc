""" Responsible for installing xpresso bundle on a cluster """

from xpresso.ai.admin.infra import bundles
from xpresso.ai.admin.infra.bundles.bundle_dependency import BundleDependency
from xpresso.ai.admin.infra.bundles.bundle_manager import BundleManager
from xpresso.ai.admin.infra.bundles.remote_shell_executor import RemoteShellExecutor
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.admin.infra.bundles.ubuntu.kubernetes.kubeadm_bundle import KubeadmBundle

__all__ = ['XpressoClusterBundleManager']
__author__ = 'Naveen Sinha'


class XpressoClusterBundleManager:
    """
    Sets up the xpresso cluster and install all required tools in the cluster.
    It maintains a sequence of step required to setup the xpresso cluster

    """

    XPRESSO_CLUSTER_BUNDLE_NAME = "XpressoClusterBundle"

    INPUT_JSON_XPRESSO_CTRL = "xpresso-controller"
    INPUT_JSON_DB = "db"
    INPUT_JSON_KUBE_MASTER = "kubernetes_master"
    INPUT_JSON_KUBE_NODE = "kubernetes_node"
    INPUT_JSON_EXCLUDE = "exclude"
    INPUT_VM_ADDRESS = "address"

    MONGO_BUNDLE = "MongoBundle"
    XPRESSO_CTRL_BUNDLE = "XpressoControllerBundle"
    KUBE_DASH_BUNDLE = "KubeadmDashboardBundle"
    KUBE_MASTER_BUNDLE = "KubeadmMasterBundle"
    KUBE_NODE_BUNDLE = "KubeadmNodeBundle"

    LOCALHOST_IDENTIFIER = "localhost"

    def __init__(self, config):
        self.logger = XprLogger()
        self.config = config
        self.config_path = config.config_file_path
        self.bundle_dependency = BundleDependency(
            config_path=config.config_file_path)

    def start_installation(self, cluster_installation_json):
        """
        Start the installations of the xpresso cluster.

        Args:
            cluster_installation_json: Cluster installation
        json should look like this:
                "xpresso-controller": {
                      "address": ["localhost"]
                    },
                    "db": {
                      "address": ["localhost"]
                    },
                    "kubernetes_master": {
                      "address" : ["172.16.2.30"]
                    },
                    "kubernetes_node": {
                      "address":["172.16.2.31","172.16.2.32"]
                    },
                    "exclude": {"address": []
                }
        """

        bundle_list = self.bundle_dependency.get_dependency(
            bundle_name=self.XPRESSO_CLUSTER_BUNDLE_NAME)

        for bundle_name in bundle_list:
            bundle_class = BundleManager.bundle_str_to_class(
                target_class_name=bundle_name,
                bundle_name=bundles)
            if bundle_name == self.MONGO_BUNDLE:
                self.install(
                    bundle_class=bundle_class,
                    parameters=cluster_installation_json[self.INPUT_JSON_DB]
                )
            elif bundle_name == self.XPRESSO_CTRL_BUNDLE:
                self.install(
                    bundle_class=bundle_class,
                    parameters=
                    cluster_installation_json[self.INPUT_JSON_XPRESSO_CTRL]
                )
            elif bundle_name == self.KUBE_DASH_BUNDLE or bundle_name == self.KUBE_MASTER_BUNDLE:
                self.install(
                    bundle_class=bundle_class,
                    parameters=
                    cluster_installation_json[self.INPUT_JSON_KUBE_MASTER]
                )
            elif bundle_name == self.KUBE_NODE_BUNDLE:
                current_parameter = cluster_installation_json[self.INPUT_JSON_KUBE_NODE]
                self.install(
                    bundle_class=KubeadmBundle,
                    parameters=current_parameter
                )
                self.install(
                    bundle_class=bundle_class,
                    parameters=current_parameter
                )
            elif bundle_name in cluster_installation_json[
                    self.INPUT_JSON_EXCLUDE][self.INPUT_VM_ADDRESS]:
                continue
            else:
                self.install(
                    bundle_class=bundle_class,
                    parameters=
                    cluster_installation_json[self.INPUT_JSON_KUBE_MASTER]
                )

    def install(self, bundle_class, parameters):
        """
        Perform provided execution type on the given bundle name
        Args:
            parameters: Additional parameter required for installation
            bundle_class: name of the bundle to install.
                           Must match the supported bundle names.
        """
        if bundle_class is None:
            self.logger.info(f"{bundle_class} Not Found in the hierarchy")
            return False

        self.logger.info(f"Running bundle {bundle_class} with parameters"
                         f"{parameters}")

        if self.INPUT_VM_ADDRESS not in parameters:
            self.logger.info(f"{bundle_class} Not Found in the hierarchy")
            return False

        for ip_address in parameters[self.INPUT_VM_ADDRESS]:
            if ip_address == self.LOCALHOST_IDENTIFIER:
                bundle_obj = bundle_class(config_path=self.config_path)
                bundle_obj.install(parameters=parameters)
            else:
                remote_executor = RemoteShellExecutor(ip_address)
                bundle_obj = bundle_class(config_path=self.config_path,
                                          executor=remote_executor)
                bundle_obj.install(parameters=parameters)
        return True
