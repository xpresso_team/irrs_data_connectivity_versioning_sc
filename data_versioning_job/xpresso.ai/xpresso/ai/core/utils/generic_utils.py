"""
This file contains generic utility functions
"""
import socket


def get_version():
    """
    Fetches client and server versions and prints out in the stdout

    Returns:
        str: version string of the project
    """
    try:
        version_file_name = 'VERSION'
        version_fs = open(version_file_name)
        version = version_fs.read().strip()
        version_fs.close()
    except FileNotFoundError:
        # Using default version
        version = '-1'
    return version


def check_if_valid_ip_address(ip_address):
    """ Checks if the ip address is valid """
    try:
        socket.inet_aton(ip_address)
        return True
    except socket.error:
        return False
