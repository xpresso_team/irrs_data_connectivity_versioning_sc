class Error(Exception):
    pass


class AlluxioIPMissingInConfig(Error):
    pass


class AlluxioPortMissingInConfig(Error):
    pass


class AlluxioFileNameMissing(Error):
    pass
