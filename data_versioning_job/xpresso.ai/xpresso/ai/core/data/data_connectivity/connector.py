__author__ = 'Shlok Chaudhari'
__all__ = 'DataConnector'

from xpresso.ai.core.data.data_connectivity.Alluxio import alluxio_exception, alluxio_connector as alluxio
from xpresso.ai.core.data.data_connectivity.Presto import presto_exception, presto_connector as presto
import xpresso.ai.core.data.data_connectivity.connector_exception as connector_exception
from xpresso.ai.core.logging.xpr_log import XprLogger
import json


class DataConnector:
    """DataConnector class is for the xpresso developer to connect to various file systems and
    databases and import data from those in the form of a dataframe for further data exploration,
    visualization, etc. """

    def __init__(self, data_config):
        self.data_config = data_config
        self.logger = XprLogger()
        return

    def alluxio_conn(self, filename):
        """Connection to Alluxio is established"""

        with open(self.data_config) as config_file:
            config = json.load(config_file)

        alluxio_config = config.get('alluxio')

        alluxio_ip = alluxio_config.get('alluxio_ip')
        if alluxio_ip is None:
            self.logger.info("Invalid IP address for Alluxio.")
            raise alluxio_exception.AlluxioIPMissingInConfig

        alluxio_port = alluxio_config.get('alluxio_port')
        if alluxio_port is None:
            self.logger.info("Invalid Port for Alluxio.")
            raise alluxio_exception.AlluxioPortMissingInConfig

        if filename is None:
            self.logger.info("Invalid File Name or File missing.")
            raise alluxio_exception.AlluxioFileNameMissing

        alx_conn = alluxio.AlluxioConnector(alluxio_ip, alluxio_port)
        return alx_conn

    def presto_conn(self, catalog, schema, table, col):
        """Connection to Presto is established"""

        with open(self.data_config) as config_file:
            config = json.load(config_file)

        presto_config = config.get('presto')

        presto_ip = presto_config.get('presto_ip')
        if presto_ip is None:
            self.logger.info("Invalid IP address for Presto.")
            raise presto_exception.PrestoIPMissingInConfig

        presto_port = presto_config.get('presto_port')
        if presto_port is None:
            self.logger.info("Invalid Port for Presto.")
            raise presto_exception.PrestoPortMissingInConfig

        if catalog is None:
            self.logger.info("Invalid Catalog or Catalog missing.")
            raise presto_exception.PrestoCatalogMissingInConfig

        if schema is None:
            self.logger.info("Invalid Schema or Schema missing.")
            raise presto_exception.PrestoSchemaMissingInConfig

        if table is None:
            self.logger.info("Invalid Table or Table missing.")
            raise presto_exception.PrestoTableMissingInConfig

        if col is None:
            self.logger.info("Invalid no. of columns.")
            raise presto_exception.PrestoColumnsMissingInConfig

        pr_conn = presto.PrestoConnector(presto_ip, presto_port, catalog, schema)
        return pr_conn

    def import_data(self, filename=None, catalog=None, schema=None, table=None, col=None):
        """This function is for the xpresso developer to basically import data from any
        data source (Here, alluxio and presto are used to connect to various data sources.)
        and return a data frame"""

        df = None
        text_file = '.txt'
        csv_file = '.csv'
        excel_file = 'xlsx'

        if filename is None:
            pr_conn = self.presto_conn(catalog, schema, table, col)

            if pr_conn is None:
                self.logger.info("Connection to Presto failed.")
                raise connector_exception.ConnectorInvalidObject

            self.logger.info("Connection to Presto successful.")
            df = pr_conn.import_data(table, col)
        elif catalog is None and schema is None and table is None and col is None:
            alx_conn = self.alluxio_conn(filename)

            if alx_conn is None:
                self.logger.info("Connection to Alluxio failed.")
                raise connector_exception.ConnectorInvalidObject

            self.logger.info("Connection to Alluxio successful.")

            a_f = list(filename)
            ft = a_f[-4:-1]
            ft.append(a_f[-1])
            filetype = "".join(ft)
            if filetype == text_file:
                df = alx_conn.get_data_text(filename)
            elif filetype == csv_file:
                df = alx_conn.get_data_csv(filename)
            elif filetype == excel_file:
                df = alx_conn.get_data_excel(filename)
            else:
                raise connector_exception.ConnectorInvalidFilename
        else:
            self.logger.info("Invalid Arguments passed.")
            raise connector_exception.ConnectorInvalidArguments
        return df

    def import_raw_file(self, filename=None):
        """This function is for the xpresso developer to basically import data from any
        data source (Here, alluxio  to connect to various data sources.)
        and return a data frame"""

        alx_conn = self.alluxio_conn(filename)

        if alx_conn is None:
            self.logger.info("Connection to Alluxio failed.")
            raise connector_exception.ConnectorInvalidObject

        self.logger.info("Connection to Alluxio successful.")

        return alx_conn.get_data_raw(filename)


if __name__ == '__main__':
    xpr_con = DataConnector('./config/common.json')
    xpr_con.import_data('filename')
    xpr_con.import_data('catalog', 'schema', 'table', 'columns')
