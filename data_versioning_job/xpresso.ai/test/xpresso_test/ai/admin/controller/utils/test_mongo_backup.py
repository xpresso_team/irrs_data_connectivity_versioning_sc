from xpresso.ai.admin.controller.persistence.mongo_backup import MongoBackup as backup_class
from xpresso.ai.admin.controller.utils.job_schedular import XpressoSchedule as schedule_class
from xpresso.ai.admin.controller.persistence.mongo_schedular import MongoSchedular as mongobackup_class
import logging


def test_backup():  # for mongo_backup
    backup_object = backup_class()
    connection_value, dump_value = backup_object.backup(database=None)                             # placing database name as None for now
    # connection_value checks whether the connection to mongodb is successful
    # dump_value checks whether dump operation was successfully done or not
    if connection_value == 1.0 and dump_value == 0:
        logging.debug("connection to mongodb")
        assert True
    else:
        assert False


def test_schedule():  # for job_schedular
    schedule_object = schedule_class()
    schedule_value = schedule_object.process_periodically(backup_class.backup(database=None))      
    if schedule_value:
        assert True
    else:
        assert False


def test_mongodb_backup():  # for mongo_schedular
    mongobackupobject = mongobackup_class()
    mongobackupvalue = mongobackupobject.backup_mongodb()
    if mongobackupvalue:
        assert True
    else:
        assert False
