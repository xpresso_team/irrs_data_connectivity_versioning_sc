""" Test the functionality of local project manager module """
import os

from xpresso.ai.admin.controller.project_management.local_project_manager import \
    skeleton_path, replace_string_in_folder, logger
from xpresso.ai.admin.controller.utils.constants import LOCAL_TEST_REPO


def clone_sample_repo_for_testing():
    """ Git clone skeleton repository locally for testing """
    logger.info("Cloning the repo")
    try:
        os.system(f"git clone {''.join(skeleton_path)}")
    except OSError:
        # ignoring it
        logger.warning("Git clone failed")


def delete_sample_repo():
    """ Delete the local repo """
    logger.info("Deleting local repo")
    try:
        remove_cmd = f"rm -rf {LOCAL_TEST_REPO}"
        os.system(remove_cmd)
    except OSError:
        # ignoring it
        print("can not remove")
        logger.warning("Got remove failed")


def test_replace_string_in_folder():
    """ Check if replacing a string in folder is working correctly """
    logger.info("testing string replacement")
    delete_sample_repo()
    clone_sample_repo_for_testing()

    key = "{{XPRESSO_PROJECT_NAME}}"
    replacement = "this-is-my-project-name"
    replace_string_in_folder(key=key,
                             replacement=replacement,
                             directory_path=LOCAL_TEST_REPO)

    exit_cmd = f"grep -R -I '{key}' {LOCAL_TEST_REPO}"
    exit_status = os.system(exit_cmd)
    if exit_status > 0:
        # Exit status 1 means, it did not found the key in the directory
        assert True, "Key replacement is working as expected"
    else:
        assert False, " Key replacement is not working"

    exit_cmd = f"grep -R -I '{replacement}' {LOCAL_TEST_REPO}"
    exit_status = os.system(exit_cmd)
    if exit_status == 0:
        # Exit status 0 means, it found the key in the directory
        assert True, "Key replacement is working as expected"
    else:
        assert False, " Key replacement is not working"
    delete_sample_repo()
    logger.info("String replacement succeeded")
