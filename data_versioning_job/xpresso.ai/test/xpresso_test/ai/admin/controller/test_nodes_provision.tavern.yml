---
# Tests for provision_node
test_name: 9.1 Provision node as development VM

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register node "172.16.1.176"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: POST
      json:
        address: "172.16.1.176"
        name: "Node 176"
        nodetype: ""
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: provision 172.16.1.176 as a development VM

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.176"
        nodetype: "DEVVM"
        flavor: "python"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_1

---

test_name: 9.2 Provision node as cluster master

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: provision 172.16.7.51 as a cluster master

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.7.51"
        nodetype: "CLUSTER_MASTER"
        cluster: "cluster2"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        #$ext:
        #function: test.xpresso_test.ai.admin.controller.validators:validate9_2

---
test_name: 9.3 Provision node as cluster worker

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register node "172.16.7.52"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: POST
      json:
        address: "172.16.7.52"
        name: "Node 52"
        nodetype: ""
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: provision 172.16.7.52 as a cluster worker

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.7.52"
        nodetype: "CLUSTER_WORKER"
        masterip: "172.16.7.51"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

---
test_name: 9.4 Provision another node as cluster worker

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register node "172.16.1.174"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: POST
      json:
        address: "172.16.1.174"
        name: "Node 174"
        nodetype: ""
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: provision 172.16.1.174 as a cluster worker

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.174"
        nodetype: "CLUSTER_WORKER"
        masterip: "172.16.1.172"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_4

---
test_name: 9.5 Attempt to provision non-existent node

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: provision 172.16.1.179 as a cluster worker

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.179"
        nodetype: "CLUSTER_WORKER"
        masterip: "172.16.172"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 144
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_5

---
test_name: 9.6 Attempt to provision node with incorrect node type

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register node "172.16.1.175"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: POST
      json:
        address: "172.16.1.175"
        name: "Node 175"
        nodetype: ""
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: provision 172.16.1.175 as "junk"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.175"
        nodetype: "junk"
        flavor: "172.16.1.172"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 147
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_6

---
test_name: 9.7 Attempt to provision node with incorrect user

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register node "172.16.1.174"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: POST
      json:
        address: "172.16.1.174"
        name: "Node 174"
        nodetype: ""
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: provision 172.16.1.174 as "DEV_VM" with user "junk"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.174"
        nodetype: "DEV_VM"
        user_or_cluster: "junk"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 143
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_6

---
test_name: 9.8 Attempt to provision node with incorrect cluster

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register node "172.16.1.175"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: POST
      json:
        address: "172.16.1.175"
        name: "Node 175"
        nodetype: ""
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: provision 172.16.1.175 as "CLUSTER_MASTER" with cluster "junk"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.175"
        nodetype: "CLUSTER_WORKER"
        masterip: "junk"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 211
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_6

---
test_name: 9.9 Attempt to provision node that is already provisioned

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: provision 172.16.1.174 as "CLUSTER_MASTER" with cluster "cluster1"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.174"
        nodetype: "CLUSTER_MASTER"
        user_or_cluster: "cluster1"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 213

        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_9

---
test_name: 9.10 Attempt to provision node by a non-superuser user

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as user1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"user1", "pwd":"user1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: provision 172.16.1.175 as "CLUSTER_MASTER" with cluster "cluster1"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.175"
        nodetype: "CLUSTER_MASTER"
        cluster: "cluster1"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 113
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate9_6

---
test_name: 9.11 Attempt to provision node without logging in

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
      - setup_clusters
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: provision node without logging in

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/nodes"
      method: PUT
      json:
        address: "172.16.1.175"
        nodetype: "CLUSTER_MASTER"
        cluster: "cluster1"
      headers:
        token: "junk"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 113

---
