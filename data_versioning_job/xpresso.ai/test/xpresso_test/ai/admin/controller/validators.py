from pymongo import MongoClient
from pymongo.database import Database


def connect():
    mongo_db = None
    if mongo_db is None:
        mongo_client = MongoClient(host="mongodb://172.16.3.1:27017/?replicaSet=rs0", w=1)
        mongo_db = mongo_client["xprdb"]
        #mongo_db = mongo_client["xpresso_ai"]
        mongo_db.authenticate("xprdb_admin", "xprdb@Abz00ba")
    return mongo_db


def disconnect(mongo_client:MongoClient):
    mongo_client.close()


def check_db (mongo_db:Database, collection:str, doc_filter:object, valid_data: []):
    res = mongo_db[collection].find(doc_filter)
    # Each element of valid_data should be present in the result
    found = False
    for record in valid_data:
        #print("Checking record %s" %record)
        found = True
        foundRecord = False
        for result in res:
            #print("Checking against result %s" %result)
            foundRecord = True
            for attr, val in record.items():
                #print("Checking attribute %s, value %s" % (attr, val))
                if result[attr] != val:
                    #print("Not found")
                    foundRecord = False
                    break
            if foundRecord:
                #print("Record found")
                break
        if not foundRecord:
            #print ("Returning false")
            found = False
            break
    return found


def validate1_1(response):
    print(response)
    mongo_db = connect()
    # login status of user 1 should have been updated
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"loginStatus": True}]), "Login Status of xprdb_admin not updated correctly"


def validate1_2(response):
    print(response)
    mongo_db = connect()
    # login status of admin1 should have been updated
    assert check_db(mongo_db, "users", {"uid": "admin1"}, [{"loginStatus": True}]), "Login Status of admin1 not updated correctly"

def validate2_1(response):
    print(response)
    mongo_db = connect()
    # login status of user1 should have been updated to False
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"loginStatus": False}]), "Login Status of user1 not updated correctly"


def validate3_1(response):
    print(response)
    mongo_db = connect()
    # login status of user2 should have been updated to true
    assert check_db(mongo_db, "users", {"uid": "user2"}, [{"loginStatus": True}]), "Login Status of user2 not updated correctly"
    assert check_db(mongo_db, "users", {"uid": "user2"}, [{"firstName": "user2"}]), "first name of user2 not updated correctly"
    assert check_db(mongo_db, "users", {"uid": "user2"}, [{"lastName": "user2"}]), "last name of user2 not updated correctly"


def validate3_2(response):
    print(response)
    mongo_db = connect()
    # there should be only 1 user with uid "user1", and firstName and lastName should be "user1"
    results = mongo_db["users"].find({"uid": "user1"})
    assert results.count() is 1, "Duplicate user created"
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"firstName": "user1", "lastName": "user1"}]), "User1 data modified incorrectly"


def validate3_3(response):
    print(response)
    mongo_db = connect()
    # loginStatus of user with uid !@#$%^&*(){}[]\":; should have changed to True
    assert check_db(mongo_db, "users", {"uid": "!@#$%^&*()[]\":;"}, [{"loginStatus": True}]), "User !@#$%^&*(){}[]\":; not logged in"


def validate3_4(response):
    print(response)
    mongo_db = connect()
    # there should be no user with uid "user4"
    results = mongo_db["users"].find({"uid": "user4"})
    assert results.count() is 0, "User created incorrectly"


def validate3_6(response):
    print(response)
    mongo_db = connect()
    # there should be no user with uid ""
    results = mongo_db["users"].find({"uid": ""})
    assert results.count() is 0, "User created incorrectly"


def validate4_1(response):
    print(response)
    # response should have 4 users - xprdb_admin, user1, admin1 and superuser1
    users = response.json()["results"]
    assert len(users) is 4, "Incorrect number of users"
    for user in users:
        assert user["uid"] in ["xprdb_admin", "admin1", "user1", "superuser1"], "Incorrect user id"


def validate4_2(response):
    print(response)
    # response should have 5 users - xprdb_admin, user1, admin1, superuser1 and user2
    users = response.json()["results"]
    assert len(users) is 5, "Incorrect number of users"
    for user in users:
        assert user["uid"] in ["xprdb_admin", "user1", "admin1", "superuser1", "user2"], "Incorrect user id"


def validate4_5(response):
    print(response)
    # response should have 2 users - xprdb_admin and admin1
    users = response.json()["results"]
    assert len(users) is 2, "Incorrect number of users"
    for user in users:
        assert user["uid"] in ["xprdb_admin", "admin1"], "Incorrect user id"


def validate4_6(response):
    print(response)
    # response should have 1 user - user1
    users = response.json()["results"]
    assert len(users) is 1, "Incorrect number of users"
    for user in users:
        assert user["uid"] in ["user1"], "Incorrect user id"


def validate4_7(response):
    print(response)
    # response should have 0 users
    users = response.json()["results"]
    assert len(users) is 0, "Incorrect number of users"


def validate5_1(response):
    print(response)
    mongo_db = connect()
    # last name of user1 should have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"lastName": "user1 modified"}]), "User not modified"


def validate5_2(response):
    print(response)
    mongo_db = connect()
    # role of user1 should have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"primaryRole": "Admin"}]), "User not modified"


def validate5_3(response):
    print(response)
    mongo_db = connect()
    # pwd of user1 should not have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"pwd": "user1"}]), "Password modified"


def validate5_4(response):
    print(response)
    mongo_db = connect()
    # last name of user1 should have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"lastName": "user1 modified"}]), "User not modified"


def validate5_5(response):
    print(response)
    mongo_db = connect()
    # last name of admin1 should not have been modified
    assert check_db(mongo_db, "users", {"uid": "admin1"}, [{"lastName": "admin1"}]), "User modified without permission"


def validate5_6(response):
    print(response)
    mongo_db = connect()
    # role of user1 should not have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"primaryRole": "Dev"}]), \
        "Role of user modified without permission"


def validate5_7(response):
    print(response)
    mongo_db = connect()
    # last name of user1 should have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"lastName": "!@#$%^&*()[]\":;"}]), \
        "User not modified"


def validate5_8(response):
    print(response)
    mongo_db = connect()
    # no user with uid "user3" should be present
    results = mongo_db["users"].find({"uid": "user3"})
    assert results.count() is 0, "User created incorrectly during attempted modification"


def validate5_9(response):
    print(response)
    mongo_db = connect()
    # last name of user1 should not have been modified
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"lastName": "user1"}]), \
        "User modified without permission"


def validate6_1(response):
    print(response)
    mongo_db = connect()
    # activation status of user1 should be false
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"activationStatus": False}]), \
        "User activation status not changed"


def validate6_2(response):
    print(response)
    mongo_db = connect()
    # no user with uid "user3" should be present
    results = mongo_db["users"].find({"uid": "user3"})
    assert results.count() is 0, "User created incorrectly during attempted deactivation"


def validate6_3(response):
    print(response)
    mongo_db = connect()
    # activation status of admin1 should be true
    assert check_db(mongo_db, "users", {"uid": "admin1"}, [{"activationStatus": True}]), \
        "User activation status changed incorrectly"


def validate6_4(response):
    print(response)
    mongo_db = connect()
    # activation status of user1 should be true
    assert check_db(mongo_db, "users", {"uid": "user1"}, [{"activationStatus": True}]), \
        "User activation status changed incorrectly"


def validate7_1(response):
    print(response)
    mongo_db = connect()
    # node 172.16.1.171 should have been created
    results = mongo_db["nodes"].find({"address": "172.16.1.171"})
    assert results.count() is 1, "Node not registered"
    #assert check_db(mongo_db, "nodes", {"address": "172.16.1.171"}, [{"activationStatus": True}]), \
    #    "Node not registered correctly"


def validate7_2(response):
    print(response)
    mongo_db = connect()
    # should be only 1 instance of node 172.16.1.171
    results = mongo_db["nodes"].find({"address": "172.16.1.171"})
    assert results.count() is 1, "There should be only 1 instance of node 172.16.1.171"


def validate7_3(response):
    print(response)
    mongo_db = connect()
    # node with address "junk" should not have got registered
    results = mongo_db["nodes"].find({"address": "junk"})
    assert results.count() is 0, "Invalid node registered"


def validate7_4(response):
    print(response)
    mongo_db = connect()
    # node with address "172.16.1.179" should not have got registered
    results = mongo_db["nodes"].find({"address": "172.16.1.179"})
    assert results.count() is 0, "Invalid node registered"


def validate7_5(response):
    print(response)
    mongo_db = connect()
    # node with address "172.16.1.171" should not have got registered
    results = mongo_db["nodes"].find({"address": "172.16.1.171"})
    assert results.count() is 0, "Invalid node registered"


def validate7_7(response):
    print(response)
    mongo_db = connect()
    # no nodes should be present
    results = mongo_db["nodes"].find({})
    assert results.count() is 0, "Invalid node registered"


def validate8_1(response):
    print(response)
    nodes = response.json()["results"]
    assert len(nodes) is 0, "Incorrect node count"


def validate8_2(response):
    print(response)
    nodes = response.json()["results"]
    assert len(nodes) is 2, "Incorrect node count"
    for node in nodes:
        assert node["address"] in ["172.16.1.171", "172.16.1.172"], "Invalid node address"


def validate8_5(response):
    print(response)
    nodes = response.json()["results"]
    assert len(nodes) is 1, "Incorrect node count"
    for node in nodes:
        assert node["address"] in ["172.16.1.171"], "Invalid node address"


def validate8_9(response):
    print(response)
    nodes = response.json()["results"]
    assert len(nodes) is 1, "Incorrect node count"
    for node in nodes:
        assert node["address"] in ["172.16.1.172"], "Invalid node address"


def validate9_1(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.176" should now be a DEV_VM
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.176"}, [{"nodetype": "DEVVM"}]), \
      "Node not provisioned correctly"


def validate9_2(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.172" should now be a Cluster Master
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.172"}, [{"nodetype": "CLUSTER_MASTER"}]), \
      "Node not provisioned correctly"


def validate9_3(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.173" should now be a Cluster worker
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.173"}, [{"nodetype": "CLUSTER_WORKER"}]), \
      "Node not provisioned correctly"


def validate9_4(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.174" should now be a Cluster Master
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.174"}, [{"nodetype": "CLUSTER_WORKER"}]), \
      "Node not provisioned correctly"


def validate9_5(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.179" should not exist in database
    results = mongo_db["nodes"].find({"address":"172.16.1.179"})
    assert results.count() is 0, "Node incorrectly inserted"


def validate9_6(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.175" should have provisionStatus false
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.175"}, [{"provisionStatus": False}]), \
      "Node not provisioned correctly"


def validate9_9(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.174" should have nodetype CLUSTER_WORKER
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.174"}, [{"nodetype": "CLUSTER_WORKER"}]), \
      "Node not provisioned correctly"


def validate10_1(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.171" should have been deactivated
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.171"}, [{"activationStatus": False}]), \
      "Node not deactivated"


def validate10_2(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.172" should have been deactivated
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.172"}, [{"activationStatus": False}]), \
      "Node not deactivated"


def validate10_3(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.173" should have been deactivated
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.173"}, [{"activationStatus": False}]), \
      "Node not deactivated"


def validate10_5(response):
    print(response)
    mongo_db = connect()
    # node "172.16.1.175" should not have been deactivated
    assert check_db(mongo_db, "nodes", {"address": "172.16.1.175"}, [{"activationStatus": True}]), \
      "Node deactivated incorrectly"


def validate11_1(response):
    print(response)
    mongo_db = connect()
    # cluster cluster2 should have been created
    results = mongo_db["clusters"].find({"name": "cluster2"})
    assert results.count() is 1, "Cluster not created"
    assert check_db(mongo_db, "clusters", {"name": "cluster2"}, [{"activationStatus": True}]), \
      "Cluster status not set correctly"


def validate11_2(response):
    print(response)
    mongo_db = connect()
    # cluster cluster1 should have only 1 instance
    results = mongo_db["clusters"].find({"name": "cluster1"})
    assert results.count() is 1, "Duplicate cluster created"


def validate11_3(response):
    print(response)
    mongo_db = connect()
    # cluster !@#$%^&*()[]\":; should have been created
    results = mongo_db["clusters"].find({"name": "!@#$%^&*()[]\":;"})
    assert results.count() is 1, "Cluster not created"


def validate11_4(response):
    print(response)
    mongo_db = connect()
    # cluster cluster4 should have been created
    results = mongo_db["clusters"].find({"name": "cluster4"})
    assert results.count() is 0, "Cluster created incorrectly"


def validate11_6(response):
    print(response)
    mongo_db = connect()
    # there should only be 1 cluster
    results = mongo_db["clusters"].find({})
    assert results.count() is 1, "Cluster created incorrectly"


def validate12_1(response):
    print(response)
    clusters = response.json()["results"]
    assert len(clusters) is 1, "Incorrect cluster count"
    for cluster in clusters:
        assert cluster["name"] in ["cluster1"], "Incorrect cluster information"


def validate12_2(response):
    print(response)
    clusters = response.json()["results"]
    assert len(clusters) is 3, "Incorrect cluster count"
    for cluster in clusters:
        assert cluster["name"] in ["cluster1", "cluster2", "cluster3"], "Incorrect cluster information"


def validate12_6(response):
    print(response)
    clusters = response.json()["results"]
    assert len(clusters) is 0, "Incorrect cluster count"

def validate13_1(response):
    print(response)
    mongo_db = connect()
    assert check_db(mongo_db, "clusters", {"name": "cluster1"}, [{"activationStatus": False}]), \
      "Cluster not deactivated corectly"


def validate14_1(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project1"})
    assert results.count() is 1, "Project not created"
    project = results[0]
    assert project["name"] == "project1", "Incorrect project name"
    assert project["owner"]["uid"] == "user1", "Project owner incorrect"
    assert len (project["developers"]) is 2, "Incorrect number of developers"
    assert len (project["components"]) is 3, "Incorrect number of components"


def validate14_2(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "!@#$%^&*()[]\":;"})
    assert results.count() is 1, "Project not created"
    project = results[0]
    assert project["name"] == "!@#$%^&*()[]\":;", "Incorrect project name"
    assert project["owner"]["uid"] == "user1", "Project owner incorrect"
    assert len (project["developers"]) is 2, "Incorrect number of developers"
    assert len (project["components"]) is 3, "Incorrect number of components"


def validate14_3(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": ""})
    assert results.count() is 0, "Project created incorrectly"


def validate14_5(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project2"})
    assert results.count() is 0, "Project created incorrectly"


def validate14_10(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project2"})
    assert results.count() is 1, "Project not created"
    project = results[0]
    assert project["name"] == "project2", "Incorrect project name"
    assert project["owner"]["uid"] == "user1", "Project owner incorrect"
    assert len (project["developers"]) is 0, "Incorrect number of developers"
    assert len (project["components"]) is 3, "Incorrect number of components"


def validate14_13(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project2"})
    assert results.count() is 1, "Project not created"
    project = results[0]
    assert project["name"] == "project2", "Incorrect project name"
    assert project["owner"]["uid"] == "user1", "Project owner incorrect"
    assert len (project["developers"]) is 2, "Incorrect number of developers"
    assert len (project["components"]) is 3, "Incorrect number of components"


def validate18_1(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project2"})
    project = results[0]
    assert project["components"][0]["versions"][0]["version_description"] == "trial version - 1", "Build incomplete"


def validate18_6(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project2"})
    project = results[0]
    assert project["components"][0]["versions"][0]["version_description"] == "trial version - 1", "Build incomplete"
    assert project["components"][0]["versions"][1]["version_description"] == "trial version - 2", "Build incomplete"


def validate18_7(response):
    print(response)
    mongo_db = connect()
    results = mongo_db["projects"].find({"name": "project2"})
    project = results[0]
    assert project["components"][0]["versions"][0]["version_description"] == "trial version - 1", "Build incomplete"
    assert project["components"][1]["versions"][0]["version_description"] == "trial version - 1", "Build incomplete"
    assert project["components"][2]["versions"][0]["version_description"] == "trial version - 1", "Build incomplete"


def main(*argv):
    response = {}
    validate18_6 (response)


if __name__ == "__main__":
    main()
