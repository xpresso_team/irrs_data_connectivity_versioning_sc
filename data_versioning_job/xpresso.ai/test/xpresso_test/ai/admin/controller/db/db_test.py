import pytest

from xpresso.ai.admin.controller.exceptions.xpr_exceptions import UnsuccessfulConnectionException
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import UnsuccessfulOperationException
from xpresso.ai.admin.controller.persistence.mongopersistencemanager import MongoPersistenceManager
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

config_path = XprConfigParser.DEFAULT_CONFIG_PATH
config = XprConfigParser(config_path)

MONGO_SECTION = 'mongodb'
URL = 'mongo_url'
DB = 'database'
UID = 'mongo_uid'
PWD = 'mongo_pwd'
W = 'w'
MONGOURL = config[MONGO_SECTION][URL]
MONGODB = config[MONGO_SECTION][DB]
MONGOUID = config[MONGO_SECTION][UID]
MONGOPWD = config[MONGO_SECTION][PWD]
MONGOW = config[MONGO_SECTION][W]

def test1_1():
    # test 1.1: connection happy case
    print("Starting Test1.1")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD, w=MONGOW)
        mongo_db = db_MongoPersistenceManager.connect()
        assert mongo_db is not None, "Test 1.1 failed: Unable to connect to database"
        print("Test1.1 Successful")
    except UnsuccessfulConnectionException:
        print("Test1.1 failed")


def test1_2():
    # test 1.2: invalid url
    print("Starting Test1.2")
    try:
        MongoPersistenceManager.mongo_db = None
        db_MongoPersistenceManager = MongoPersistenceManager(url="localhost:27018", db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        print("Test1.2 failed: Able to connect to incorrect server URL")
        assert False
    except UnsuccessfulConnectionException:
        print("Test1.2 Successful")
        assert True


def test1_3():
    # test 1.3: server down initially, but restored within 10 seconds
    print("Starting Test1.3")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        # start service after some time
        print("Test1.3 Successful")
    except UnsuccessfulConnectionException:
        print("Test1.3 Failed")


def test2_1():
    # test 2.1: insert a record into Users collection
    print("Starting Test2.1")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.insert("Users", {"uid": "abc", "age": 50}, False)
        res = mongo_db["Users"].find({"uid": "abc"})
        assert res[0]["uid"] == "abc", "Test2.1 Failed: Incorrect name entered %s" % res[0]["name"]
        assert res[0]["age"] == 50, "Test2.1 Failed: Incorrect age entered %s" % res[0]["age"]
        print("Test2.1 Successful")
    except UnsuccessfulOperationException:
        print("Test2.1 Failed")


def test2_2():
    # test 2.1: insert a record into Clusters collection
    print("Starting Test2.2")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Clusters"].delete_many({})
        db_MongoPersistenceManager.insert("Clusters", {"name": "PROD"}, False)
        res = mongo_db["Clusters"].find({"name": "PROD"})
        assert res[0]["name"] == "PROD", "Test2.2 Failed: Incorrect name entered %s" % res[0]["name"]
        print("Test2.2 Successful")
    except IndexError:
        print("Test2.2 Failed")
    except UnsuccessfulOperationException:
        print("Test2.2 Failed")


def test2_3():
    # test 2.3: insert record into Users collection - server down initially, restored within 10 seconds
    print("Starting Test2.3")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.insert("Users", {"uid": "def", "age": 40}, True)
        res = mongo_db["Users"].find({"uid": "def"})
        # start service after some time
        assert res[0]["uid"] == "def", "Test2.1 Failed: Incorrect uid entered %s" % res[0]["uid"]
        assert res[0]["age"] == 40, "Test2.1 Failed: Incorrect age entered %s" % res[0]["age"]
        print("Test2.3 Successful")
    except UnsuccessfulOperationException:
        print("Test2.3 Failed")


def test2_4():
    # test 2.4: insert record into Users collection - server down for more than 30 seconds
    print("Starting Test2.4")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.insert("Users", {"uid": "def", "age": 40}, False)
        res = mongo_db["Users"].find({"uid": "def"})
        print("Test2.4 Failure")
    except UnsuccessfulConnectionException:
        print("Test2.4 Successful")


def test2_5():
    # test 2.5: insert duplicate record, with duplicate_ok = False
    print("Starting Test2.5")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.insert("users", {"uid": "ghi", "age": 50}, False)
        doc_id = db_MongoPersistenceManager.insert("users", {"uid": "ghi", "age": 50}, False)
        res = mongo_db["Users"].find({"uid": "ghi"})
        assert res.count() == 1, "Test2.5 Failed: Duplicate record entered"
        print("Test2.5 Successful")
    except UnsuccessfulOperationException:
        print("Test2.5 Failed")


def test2_6():
    # test 2.6: insert duplicate record, with duplicate_ok = True
    print("Starting Test2.6")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.insert("users", {"uid": "abc", "age": 50}, False)
        db_MongoPersistenceManager.insert("users", {'uid': 'abc', 'age': 40}, True)
        print("Test2.6 Failed")
        assert False
    except UnsuccessfulOperationException:
        res = mongo_db["users"].find({'uid': 'abc'})
        assert res.count() == 1, "Test2.6 Failed: Duplicate record entered"
        print("Test2.6 Successful")


def test3_1():
    # test 3.1: update an existing record in Users collection with upsert = False
    print("Starting Test3.1")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc"})
        db_MongoPersistenceManager.update("Users", {"uid": "abc"}, {"age": 40}, False)
        db_MongoPersistenceManager.update('users', {"uid": "xprdb_admin"},
                        {"token": "something", "loginStatus": True}, True)
        res = mongo_db["Users"].find({"uid": "abc"})
        assert res[0]["uid"] == "abc", "Test3.1 Failed: Incorrect name entered %s" % res[0]["name"]
        assert res[0]["age"] == 40, "Test3.1 Failed: Age not updated %s" % res[0]["age"]
        print("Test3.1 Successful")
    except UnsuccessfulOperationException:
        print("Test3.1 Failed")


def test3_2():
    # test 3.2: update an existing record in Users collection with upsert = True
    print("Starting Test3.2")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.update("Users", {"uid": "abc"}, {"age": 35}, True)
        res = mongo_db["Users"].find({"uid": "abc"})
        assert res[0]["uid"] == "abc", "Test3.2 Failed: Incorrect name entered %s" % res[0]["name"]
        assert res[0]["age"] == 35, "Test3.2 Failed: Age not updated %s" % res[0]["age"]
        print("Test3.2 Successful")
    except UnsuccessfulOperationException:
        print("Test3.2 Failed")


def test3_3():
    # test 3.3: update a non existing record in Users collection with upsert = False
    print("Starting Test3.3")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.update("Users", {"uid": "def"}, {"age": 35}, False)
        res = mongo_db["Users"].find({"uid": "def"})
        assert res.count() == 0, "Test3.3 Failed: Record entered"
        print("Test3.3 Successful")
    except UnsuccessfulOperationException:
        print("Test3.3 Failed")


def test3_4():
    # test 3.4: update a non existing record in Users collection with upsert = True
    print("Starting Test3.4")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.update("Users", {"uid": "def"}, {"age": 35}, True)
        res = mongo_db["Users"].find({"uid": "def"})
        assert res.count() > 0, "Test3.4 Failed: Record not entered"
        print("Test3.4 Successful")
    except UnsuccessfulOperationException:
        print("Test3.4 Failed")


def test3_5():
    # test 3.5: update record in Users collection - server down initially, restored within 10 seconds
    print("Starting Test3.5")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.update("Users", {"uid": "def"}, {"age": 55}, True)
        # start service after somme time
        res = mongo_db["Users"].find({"uid": "def"})
        assert res[0]["uid"] == "def", "Test3.5 Failed: Incorrect uid entered %s" % res[0]["uid"]
        assert res[0]["age"] == 55, "Test3.5 Failed: Age not updated %s" % res[0]["age"]
        print("Test3.5 Successful")
    except UnsuccessfulOperationException:
        print("Test3.5 Failed")


def test3_6():
    # test 3.6: insert record in Users collection - server down for more than 30 seconds
    print("Starting Test3.6")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.update("Users", {"uid": "def"}, {"age": 47}, True)
        res = mongo_db["Users"].find({"uid": "def"})
        print("Test3.6 Failure")
    except UnsuccessfulConnectionException:
        print("Test3.6 Successful")


def test4_1():
    # test 4.1: replace an existing record in Users collection with upsert = False
    print("Starting Test4.1")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc"})
        db_MongoPersistenceManager.replace("Users", {"uid": "abc"}, {"uid": "abc replaced", "age": 40}, False)
        res = mongo_db["Users"].find({"uid": "abc replaced"})
        assert res[0]["uid"] == "abc replaced", "Test4.1 Failed: Incorrect uid entered %s" % res[0]["uid"]
        assert res[0]["age"] == 40, "Test4.1 Failed: Age not updated %s" % res[0]["age"]
        print("Test4.1 Successful")
    except UnsuccessfulOperationException:
        print("Test4.1 Failed")


def test4_2():
    # test 4.2: replace an existing record in Users collection with upsert = True
    print("Starting Test4.2")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.replace("Users", {"uid": "abc"}, {"uid": "abc replaced again", "age": 35}, True)
        res = mongo_db["Users"].find({"uid": "abc replaced again"})
        assert res[0]["uid"] == "abc replaced again", "Test4.2 Failed: Incorrect uid entered %s" % res[0]["uid"]
        assert res[0]["age"] == 35, "Test4.2 Failed: Age not updated %s" % res[0]["age"]
        print("Test4.2 Successful")
    except UnsuccessfulOperationException:
        print("Test4.2 Failed")


def test4_3():
    # test 4.3: replace a non existing record in Users collection with upsert = False
    print("Starting Test4.3")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.replace("Users", {"uid": "ghi"}, {"age": 35}, False)
        res = mongo_db["Users"].find({"uid": "ghi"})
        assert res.count() == 0, "Test4.3 Failed: Record entered"
        print("Test4.3 Successful")
    except UnsuccessfulOperationException:
        print("Test4.3 Failed")


def test4_4():
    # test 4.4: replace a non existing record in Users collection with upsert = True
    print("Starting Test4.4")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        db_MongoPersistenceManager.replace("Users", {"uid": "ghi"}, {"uid": "ghi", "age": 35}, True)
        res = mongo_db["Users"].find({"uid": "ghi"})
        assert res.count() > 0, "Test4.4 Failed: Record not entered"
        print("Test4.4 Successful")
    except UnsuccessfulOperationException:
        print("Test4.4 Failed")


def test4_5():
    # test 4.5: replace record in Users collection - server down initially, restored within 10 seconds
    print("Starting Test4.5")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.replace("Users", {"uid": "ghi"}, {"uid": "ghi replaced", "age": 55}, True)
        # start service after somme time
        res = mongo_db["Users"].find({"uid": "ghi replaced"})
        assert res[0]["uid"] == "ghi replaced", "Test4.5 Failed: Incorrect uid entered %s" % res[0]["uid"]
        assert res[0]["age"] == 55, "Test4.5 Failed: Age not updated %s" % res[0]["age"]
        print("Test4.5 Successful")
    except UnsuccessfulOperationException:
        print("Test4.5 Failed")


def test4_6():
    # test 4.6: replace record in Users collection - server down for more than 30 seconds
    print("Starting Test4.6")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.update("Users", {"uid": "ghi replaced"}, {"uid": "ghi replaced again", "age": 47}, True)
        res = mongo_db["Users"].find({"uid": "ghi replaced again"})
        print("Test4.6 Failure")
    except UnsuccessfulConnectionException:
        print("Test4.6 Successful")


    '''test 5.1: find 1 record in Users collection - combination of 2 search criteria
    test 5.2: find 3 records in Users collection - combination of 2 search criteria
    test 5.3: try to find non-existent record in Users collection
    test 5.4: find record in Users collection - server down initially, restored within 10 seconds
    test 5.5: find record in Users collection - server down for more than 30 seconds
    '''


def test5_1():
    # test 5.1: find 1 record in Users collection - combination of 2 search criteria
    print("Starting Test5.1")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc", "age": 50})
        mongo_db["Users"].insert_one({"uid": "def", "age": 45})
        users = db_MongoPersistenceManager.find("Users", {"uid": "abc", "age": 50})
        assert len(users) == 1, "Test5.1 Failed: Incorrect number of records found"
        assert users[0]["uid"] == "abc", "Test5.1 Failed: Incorrect uid found: %s" % users[0]["uid"]
        assert users[0]["age"] == 50, "Test5.1 Failed: Incorrect age found: %s" % users[0]["age"]
        print("Test5.1 Successful")
    except UnsuccessfulOperationException:
        print("Test5.1 Failed")

def test5_2():
    # test 5.2: find 3 records in Users collection - combination of 2 search criteria
    print("Starting Test5.2")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc", "age": 50})
        mongo_db["Users"].insert_one({"uid": "def", "age": 50})
        mongo_db["Users"].insert_one({"uid": "fgh", "age": 40})
        mongo_db["Users"].insert_one({"uid": "ghi", "age": 50})
        users = db_MongoPersistenceManager.find("Users", {"age": 50})
        assert len(users) == 3, "Test5.2 Failed: Incorrect number of records found"
        assert users[0]["uid"] == "abc", "Test5.2 Failed: Incorrect uid found: %s" % users[0]["uid"]
        assert users[0]["age"] == 50, "Test5.2 Failed: Incorrect age found: %s" % users[0]["age"]
        assert users[1]["uid"] == "def", "Test5.2 Failed: Incorrect uid found: %s" % users[1]["uid"]
        assert users[1]["age"] == 50, "Test5.2 Failed: Incorrect age found: %s" % users[1]["age"]
        assert users[2]["uid"] == "ghi", "Test5.2 Failed: Incorrect uid found: %s" % users[2]["uid"]
        assert users[2]["age"] == 50, "Test5.2 Failed: Incorrect age found: %s" % users[2]["age"]
        print("Test5.2 Successful")
    except UnsuccessfulOperationException:
        print("Test5.2 Failed")


def test5_3():
    # test 5.3: try to find non-existent record in Users collection
    print("Starting Test5.3")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc", "age": 50})
        users = db_MongoPersistenceManager.find("Users", {"uid": "def"})
        assert len(users) == 0, "Test5.3 Failed: Spurious records found"
        print("Test5.3 Successful")
    except UnsuccessfulOperationException:
        print("Test5.3 Failed")


def test5_4():
    # test 5.4: find record in Users collection - server down initially, restored within 10 seconds
    print("Starting Test5.4")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        users = db_MongoPersistenceManager.find("Users", {"uid": "abc"})
        # start service after somme time
        assert len(users) == 1, "Test5.1 Failed: Incorrect number of records found"
        assert users[0]["uid"] == "abc", "Test5.4 Failed: Incorrect uid found: %s" % users[0]["uid"]
        assert users[0]["age"] == 50, "Test5.4 Failed: Incorrect age found: %s" % users[0]["age"]
        print("Test5.4 Successful")
    except UnsuccessfulOperationException:
        print("Test5.4 Failed")


def test5_5():
    # test 5.5: find record in Users collection - server down for more than 30 seconds
    print("Starting Test5.5")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.find("Users", {"uid": "abc"})
        print("Test5.5 Failure")
    except UnsuccessfulConnectionException:
        print("Test5.5 Successful")



def test6_1():
    # test 6.1: delete 1 record in Users collection
    print("Starting Test6.1")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc", "age": 50})
        mongo_db["Users"].insert_one({"uid": "def", "age": 50})
        db_MongoPersistenceManager.delete("Users", {"uid": "abc", "age": 50})
        users = mongo_db["Users"].find({"age": 50})
        assert users.count() == 1, "Test6.1 Failed: Incorrect number of records deleted"
        assert users[0]["uid"] == "def", "Test6.1 Failed: Incorrect uid found: %s" % users[0]["uid"]
        assert users[0]["age"] == 50, "Test6.1 Failed: Incorrect age found: %s" % users[0]["age"]
        print("Test6.1 Successful")
    except UnsuccessfulOperationException:
        print("Test6.1 Failed")

def test6_2():
    # test 6.2: delete 3 records in Users collection
    print("Starting Test6.2")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc", "age": 50})
        mongo_db["Users"].insert_one({"uid": "def", "age": 45})
        mongo_db["Users"].insert_one({"uid": "ghi", "age": 40})
        mongo_db["Users"].insert_one({"uid": "jkl", "age": 45})
        db_MongoPersistenceManager.delete("Users", {"uid": "abc"})
        result = mongo_db["Users"].find({"uid": "abc"})
        assert result.count() == 0, "Test6.2 Failed: Incorrect number of records found"
        print("Test6.2 Successful")
    except UnsuccessfulOperationException:
        print("Test6.2 Failed")


def test6_3():
    # test 6.3: try to delete non-existent record in Users collection
    print("Starting Test6.3")
    try:
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        mongo_db["Users"].delete_many({})
        mongo_db["Users"].insert_one({"uid": "abc", "age": 50})
        db_MongoPersistenceManager.delete("Users", {"uid": "def"})
        result = mongo_db["Users"].find({"uid": "abc"})
        assert result.count() == 1, "Test6.3 Failed: Incorrect records deleted"
        print("Test6.3 Successful")
    except UnsuccessfulOperationException:
        print("Test6.3 Failed")


def test6_4():
    # test 6.4: delete record in Users collection - server down initially, restored within 10 seconds
    print("Starting Test6.4")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.delete("Users", {"uid": "abc"})
        result = mongo_db["Users"].find({"uid": "abc"})
        # start service after somme time
        assert result.count() == 0, "Test6.4 Failed: Incorrect number of records found"
        print("Test6.4 Successful")
    except UnsuccessfulOperationException:
        print("Test6.4 Failed")


def test6_5():
    # test 6.5: delete record in Users collection - server down for more than 30 seconds
    print("Starting Test6.5")
    try:
        # stop Mongodb service
        db_MongoPersistenceManager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
        mongo_db = db_MongoPersistenceManager.connect()
        db_MongoPersistenceManager.delete("Users", {"uid": "abc"})
        print("Test6.5 Failure")
    except UnsuccessfulConnectionException:
        print("Test6.5 Successful")
