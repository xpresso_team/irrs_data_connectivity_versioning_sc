import pytest
from xpresso.ai.admin.controller.client.controller_client \
    import ControllerClient
from xpresso.ai.admin.controller.node_management.nodemanager import NodeManager
from xpresso.ai.admin.controller.persistence.mongopersistencemanager import \
    MongoPersistenceManager
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import XprExceptions

client = ControllerClient()
MONGO_SECTION = 'mongodb'
URL = 'mongo_url'
DB = 'database'
UID = 'mongo_uid'
PWD = 'mongo_pwd'
W = 'w'
config_path = XprConfigParser.DEFAULT_CONFIG_PATH

config = XprConfigParser(config_path)
mongo_persistence_manager = MongoPersistenceManager(
    url=config[MONGO_SECTION][URL],
    db=config[MONGO_SECTION][DB],
    uid=config[MONGO_SECTION][UID],
    pwd=config[MONGO_SECTION][PWD],
    w=config[MONGO_SECTION][W])
node = NodeManager(mongo_persistence_manager)

test_node_json1 = {
    "name": "testnode1",
    "address": "172.16.1.151",
    "nodetype": "CLUSTER_MASTER",
    "cluster": "testcluster"
}

test_cluster = {
    "name": "testcluster",
    "worker_nodes": [],
    "master_nodes": {}
}

test_node_json2 = {
    "name": "testnode152",
    "address": "172.16.1.152",
    "nodetype": "CLUSTER_WORKER",
    "masterip": "172.16.1.151"
}

test_node_json3 = {
    "name": "testnode153",
    "address": "172.16.1.153",
    "nodetype": "DEVVM",
    "flavor": "python"
}

test_node_json4 = {
    "name": "testnode154",
    "address": "172.16.1.154",
    "nodetype": "CLUSTER_WORKER",
    "masterip": "172.16.1.151"
}

su_credentials = {
    "username": "pi",
    "password": "1GOPI&krishna"
}

admin_credentials = {
    "username": "admin1",
    "password": "admin1"
}

MONGO_SECTION = 'mongodb'
URL = 'mongo_url'
DB = 'database'
UID = 'mongo_uid'
PWD = 'mongo_pwd'
W = 'w'
config_path = XprConfigParser.DEFAULT_CONFIG_PATH
config = XprConfigParser(config_path)

mongo_persistence_manager = MongoPersistenceManager(
                              url=config[MONGO_SECTION][URL],
                              db=config[MONGO_SECTION][DB],
                              uid=config[MONGO_SECTION][UID],
                              pwd=config[MONGO_SECTION][PWD],
                              w=config[MONGO_SECTION][W])

node_manager = NodeManager(MongoPersistenceManager)


def test_register_node_1():
    # simple test of adding new node as a superuser
    print("Starting test_register_node_1")
    try:
        node.register_node(test_node_json1)
        getnode = node.get_nodes({'name': 'testnode1'})
        assert getnode[0]['name'] == 'testnode1'
        assert getnode[0]['email'] == 'testnode@xprctl'
        print("test_register_node_1 successful")
    except XprExceptions as err:
        print("Exception error code is ", err.error_code)
        print("test_register_node_1 failed")


def test_register_node_2():
    # Adding an existing node
    print("Starting test_register_node_2")
    try:
        node.register_node(test_node_json1)
        print("test_register_node_2 failed")
    except XprExceptions as err:
        assert err.error_code == 212 or err.error_code == 146
        print("test_register_node_2 successful")


def test_register_node_3():
    # Adding a node with invalid ip address
    node_json = {
        "address": "(&#&Hsdg",
        "name": "test_register_node_3"
    }
    try:
        node.register_node(node_json)
        print("test_register_node_3 failed")
    except XprExceptions as err:
        assert err.error_code == 144
        print("test_register_node_3 successful")


def test_register_node_4():
    # Adding a node with non-existent ip address
    node_json = {
        "address": "172.172.172.172",
        "name": "test_register_node_4"
    }
    try:
        node.register_node(node_json)
        print("test_register_node_4 failed")
    except XprExceptions as err:
        assert err.error_code == 144
        print("test_register_node_4 successful")


def test_register_node_5():
    # Trying to add a node as a admin user
    try:
        client.login(
            admin_credentials["username"],
            admin_credentials["password"]
            )
        addnode = client.register_node(test_node_json1)
        assert addnode["error_code"] == 113
        print("test_register_node_5 successful")
    except XprExceptions as err:
        print("test_register_node_5 failed")


def test_register_node_6():
    # Trying to add a node without logging in
    try:
        client.logout()
        addnode = client.register_node(test_node_json1)
        assert addnode["error_code"] == 113
        print("test_register_node_6 successful")
    except XprExceptions as err:
        print("test_register_node_6 failed")


def test_register_node_7():
    # Trying to add a node as a admin user
    try:
        client.logout()
    except XprExceptions:
        print("Session not found..ignoring")
    client.login(
        su_credentials["username"],
        su_credentials["password"]
        )
    node_json = {
        "address": "",
        "name": "test_register_node_7"
    }
    try:
        addnode = client.register_node(node_json)
        assert addnode["error_code"] == 201
        print("test_register_node_7 successful")
    except XprExceptions as err:
        print("test_register_node_7 failed")


def test_get_node_1():
    # Get list of all nodes as admin
    print("Starting test_get_node_1")
    try:
        nodes = node.get_nodes({})
        assert len(nodes) > 0
        print("test_get_node_1 successful")
    except XprExceptions:
        print("test_get_node_1 failed")


def test_get_node_2():
    # get list of nodes after adding 2 new nodes
    print("Starting test_get_node_2")
    try:
        node.delete_node({"address": "172.16.1.100"})
    except XprExceptions as err:
        print(f"error in deleting node1 : \n {err}\n")
    try:
        node.delete_node({"address": "172.16.1.101"})
    except XprExceptions as err:
        print(f"error in deleting node2 : \n {err}\n")

    try:
        nodes_before = node.get_nodes({})
        node.register_node(test_node_json1)
        node.register_node(test_node_json2)
        nodes_after = node.get_nodes({})
        assert len(nodes_after) - len(nodes_before) == 2
        print("test_get_node_2 successful")
    except XprExceptions as err:
        print(f"\n\n error thrown is : \n {err} \n\n")
        print("test_get_node_2 failed")


def test_get_node_3():
    # get list of nodes witout admin access
    print("Starting test_get_node_3")
    try:
        client.logout()
        client.login('user1', 'user1')
        nodes = node.get_nodes({})
        assert nodes["error_code"] == 113
        print("test_get_node_3 successful")
    except:
        print("test_get_node_3 failed")


def test_get_node_4():
    # get list of nodes without logging in
    print("starting test_get_node_4")
    try:
        client.logout()
        nodes = client.get_nodes({})
        assert nodes["error_code"] == 113
        print("test_get_node_4 successful")
    except:
        print("test_get_node_4 failed")


def test_get_node_5():
    # Get the list of nodes with specific address
    try:
        node.delete_node({"address": "172.16.1.100"})
    except XprExceptions as err:
        print(f"Error in deleting node 172.16.1.100 is : \n{err}\n")
    try:
        node.register_node(test_node_json1)
        nodes = node.get_nodes({"address": test_node_json1["address"]})
        assert len(nodes) == 1
        print("test_get_node_5 successful")
    except XprExceptions as err:
        print(f"test_get_node_5 failed because of error : \n {err} \n")


def test_get_node_6():
    # Get the list of nodes with impossible filter json
    filter_json = {
        "nodetype": "junk"
    }
    try:
        nodes = node.get_nodes(filter_json)
        assert len(nodes) == 0
        print("test_get_node_6 successful")
    except XprExceptions:
        print("test_get_node_6 failed")


def test_get_node_7():
    # Get the list of nodes with invalid filter condition
    filter_json = {
        "junk": "junk"
    }
    try:
        nodes = node.get_nodes(filter_json)
        assert len(nodes) == 0
        print("test_get_node_7 successful")
    except XprExceptions:
        print("test_get_node_7 failed")


def test_get_node_8():
    # Get the list of nodes with multiple filters
    try:
        node.delete_node({"address": "172.16.1.100"})
    except XprExceptions as err:
        print(f"Error in deleting node 172.16.1.100 is : \n{err}\n")
    try:
        node.register_node(test_node_json1)
        nodes = node.get_nodes(
            {
                "address": test_node_json1["address"],
                "name": test_node_json1["name"]
            }
        )
        assert len(nodes) == 1
        print("test_get_node_8 successful")
    except XprExceptions as err:
        print(f"test_get_node_8 failed because of error : \n {err} \n")


def test_get_node_9():
    # Get the list of nodes with invalid ip address
    nodes = node.get_nodes({"address": "172.172.172.172"})
    assert len(nodes) == 0
    print("test_get_node_9 successful")


def test_get_node_10():
    # Get the list of nodes with invalid name
    node_name = test_node_json1["name"]+"^&@@BKJ"
    nodes = node.get_nodes({"name": node_name})
    assert len(nodes) == 0
    print("test_get_node_10 successful")


def test_provision_node_1():
    # Provision a node as development vm
    try:
        node.register_node(test_node_json3)
        node.provision_node(test_node_json3)
        provisioned_node = node.get_nodes(
            {"address": test_node_json3["address"]}
            )
        assert provisioned_node[0]["provisionStatus"] is True
        print("test_provision_node_1 successful")
    except XprExceptions as err:
        print("test_provision_node_1 failed")


def test_provision_node_2():
    # Provision a node as master_node
    try:
        node.delete_node({"address": test_node_json1["address"]})
    except XprExceptions as err:
        print(f"\n error in deleting the node is :\n{err}\n")

    try:
        client.register_cluster(test_cluster)
        node.register_node(test_node_json1)
        node.provision_node(test_node_json1)
        provisioned_node = node.get_nodes(
            {"address": test_node_json1["address"]}
            )
        assert provisioned_node[0]["provisionStatus"] is True
        print("test_provision_node_2 successful")
    except XprExceptions as err:
        print("test_provision_node_2 failed")


def test_provision_node_3():
    # Provision a node as worker_node
    try:
        node.delete_node({"address": test_node_json2["address"]})
    except XprExceptions as err:
        print(f"\n error in deleting the node is :\n{err}\n")

    try:
        node.register_node(test_node_json2)
        node.provision_node(test_node_json2)
        provisioned_node = node.get_nodes(
            {"address": test_node_json2["address"]}
            )
        assert provisioned_node[0]["provisionStatus"] is True
        print("test_provision_node_3 successful")
    except XprExceptions as err:
        print("test_provision_node_3 failed")


def test_provision_node_4():
    # Provision another node as worker_node to same cluster as before
    try:
        node.delete_node({"address": test_node_json4["address"]})
    except XprExceptions as err:
        print(f"\n error in deleting the node is :\n{err}\n")

    try:
        node.register_node(test_node_json4)
        node.provision_node(test_node_json4)
        provisioned_node = node.get_nodes(
            {"address": test_node_json4["address"]}
            )
        assert provisioned_node[0]["provisionStatus"] is True
        print("test_provision_node_4 successful")
    except XprExceptions as err:
        print("test_provision_node_4 failed")


def test_provision_node_5():
    # Provision a non existent node
    node_json = {
        "address": "172.172.172.172",
        "nodetype": "CLUSTER_WORKER",
        "masterip": "172.16.1.100",
        "name": "non_existent_node"
    }
    try:
        node.provision_node(node_json)
        print("test_node_provision_5 failed")
    except XprExceptions as err:
        assert err.error_code == 144
        print("test_node_provision_5 successful")


def test_provision_node_6():
    # Provision a node with invalid type
    node_json = {
        "address": "172.172.172.172",
        "type": "CLUSTER_WORKER",
        "masterip": "172.16.1.100",
        "name": "non_existent_node"
    }
    try:
        node.provision_node(node_json)
        print("test_node_provision_6 failed")
    except XprExceptions as err:
        assert err.error_code == 147
        print("test_node_provision_6 successful")


def test_provision_node_7():
    # provision a node with wrong cluster info
    pass


def test_provision_node_8():
    # Provision a node that is already provisioned
    try:
        node.provision_node(test_node_json2)
        print("test_provision_node_8 failed")
    except XprExceptions as err:
        assert err.error_code == 213 or err.error_code == 212
        print("test_provision_node_8 successful")


def test_provision_node_9():
    # Try to provision node by non superuser user
    client.login(
        admin_credentials["username"],
        admin_credentials["password"]
    )
    try:
        provision_status = client.provision_node(test_node_json1)
        assert provision_status["error_code"] == 113
        print("test_provision_node_9 successful")
    except XprExceptions as err:
        print("test_provision_node_9 failed")


def test_delete_node():
    # Deleting an node as a admin
    print("Starting test_delete_node")
    try:
        deletenode = node.deactivate_node('testnode1')
        nodes = node.get_nodes({'name': 'testnode1'})
        assert nodes[0]["activationStatus"] is False
        print("test_delete_node successful")
    except:
        print("test_delete_node failed")


def test_delete_node_2():
    # Deleting an node without logging in
    print("Starting test_delete_node_2")
    try:
        client.logout()
        delete_status = client.deactivate_node('testnode1')
        assert delete_status["error_code"] == 113
        print("test_delete_node_2 successful")
    except:
        print("test_delete_node_2 failed")


def test_delete_node_3():
    # Deleting an node who is not present
    print("Starting test_delete_node_3")
    try:
        node.deactivate_node('testnode1234')
        print("test_delete_node_3 failed")
    except XprExceptions as err:
        assert err.error_code == 144
        print("test_delete_node_3 successful")


def test_delete_node_4():
    # Deleting a node without access
    print("Starting test_delete_node_4")
    try:
        client.logout()
        client.login(
            admin_credentials["username"],
            admin_credentials["password"]
        )
        nodes = client.deactivate_node('workernode')
        assert nodes["error_code"] == 113
        print("test_delete_node_4 successful")
    except:
        print("test_delete_node_4 failed")
