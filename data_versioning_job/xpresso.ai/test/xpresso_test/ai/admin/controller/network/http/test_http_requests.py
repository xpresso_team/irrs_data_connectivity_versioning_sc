"""
This function contains test cases for accessing HTTP Request, HTTP Response
HTTP Request Handler
"""
from xpresso.ai.admin.controller.network.http.http_request import HTTPRequest
from xpresso.ai.admin.controller.network.http.http_request import HTTPMethod
from xpresso.ai.admin.controller.network.http.http_request_handler import \
    HTTPHandler
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    HTTPRequestFailedException, HTTPInvalidRequestException


def test_success_http():
    url = "http://httpbin.org/get?test=success"
    request = HTTPRequest(method=HTTPMethod.GET, url=url)
    handler = HTTPHandler()
    try:
        response = handler.send_request(request)
        json_response = response.get_data_as_json()
        assert ("args" in json_response and "test" in json_response["args"] and
                json_response["args"]["test"] == "success")
    except HTTPRequestFailedException:
        assert False
    except HTTPInvalidRequestException:
        assert False


def test_invalid_http():
    url = "htp://httpbin.org/get?test=success"
    request = HTTPRequest(method=HTTPMethod.GET, url=url)
    handler = HTTPHandler()
    try:
        response = handler.send_request(request)
        json_response = response.get_data_as_json()
        assert False
    except HTTPRequestFailedException:
        assert False
    except HTTPInvalidRequestException:
        assert True


def test_failed_http():
    url = "http://httpbin.org/get?test=success"
    request = HTTPRequest(method=HTTPMethod.POST, url=url)
    handler = HTTPHandler()
    try:
        response = handler.send_request(request)
        assert not response.ok()
    except HTTPRequestFailedException:
        assert True
    except HTTPInvalidRequestException:
        assert False


def test_request_failed_http():
    url = "https://wrong.host.badssl.com/"
    request = HTTPRequest(method=HTTPMethod.GET, url=url)
    handler = HTTPHandler()
    try:
        response = handler.send_request(request)
        assert False
    except HTTPRequestFailedException:
        assert True
    except HTTPInvalidRequestException:
        assert False
