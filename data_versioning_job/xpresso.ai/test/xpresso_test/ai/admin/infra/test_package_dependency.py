"""
Test suite for bundle dependencies
"""
from xpresso.ai.admin.infra.bundles.bundle_dependency import BundleDependency
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import BundleFailedException


def test_check_supported():
    config_path = "config/test/logger_test_config.json"
    bundle_dependency = BundleDependency(config_path=config_path)
    assert bundle_dependency.check_if_supported("NonExistingBundle") is False
    assert bundle_dependency.check_if_supported("DevelopmentVMBundle") is True


def test_list_all():
    config_path = "config/test/logger_test_config.json"
    bundle_dependency = BundleDependency(config_path=config_path)
    bundle_list = bundle_dependency.list_all()
    assert {"NFSBundle", "BaseUbuntuBundle", "DockerDistributionBundle", "DockerBundle", "PythonBundle",
            "AptRepositoryBundle", "BaseVMBundle", "DevelopmentVMBundle"} == set(bundle_list)


def test_cyclic_dependency():
    config_path = "config/test/dependency_test_config.json"
    try:
        bundle_dependency = BundleDependency(config_path=config_path)
        assert False
    except BundleFailedException:
        assert True


def test_dependency():
    config_path = "config/test/logger_test_config.json"
    bundle_dependency = BundleDependency(config_path=config_path)
    result = bundle_dependency.get_dependency("EmptyBundle")
    assert len(result) == 0
    result = bundle_dependency.get_dependency("None")
    print(result)
    assert len(result) == 0
    result = bundle_dependency.get_dependency("BaseVMBundle")
    assert {"BaseVMBundle", "PythonBundle", "DockerBundle", "NFSBundle", "BaseUbuntuBundle"} == set(result)
