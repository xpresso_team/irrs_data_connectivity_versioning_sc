from xpresso.ai.core.data.attribute_info import DataType
from xpresso.ai.core.data.visualize import Visualization
from xpresso.ai.core.data.structured_dataset import StructuredDataset
from xpresso.ai.core.data.dataset_explorer import Explorer
import os
import shutil

def remove_dir(path=None):
    if path is None:
        assert  False

    if os.path.exists(path):
        shutil.rmtree(path)
    return

def test_render_univariate():
    output_path = "./test_univariate"
    output_path = os.path.abspath(os.path.join(os.getcwd(), output_path))

    dataset = StructuredDataset()
    dataset.import_dataset("./config/test/data/test.csv")
    explorer = Explorer(dataset)
    explorer.understand()
    explorer.explore_attributes()
    explorer.explore_metrics()
    visualization = Visualization(dataset)
    visualization.render_univariate(attr_name="Numeric_float_col",
                                    format="png",path=output_path)

    for root, subdirs, files in os.walk(output_path):
        for filename in files:
            if "Numeric_float_col" in filename:
                remove_dir(output_path)
                assert True
                break
        if "Numeric_float_col" not in filename:
            remove_dir(output_path)
            assert False


def test_render_multivariate():
    output_path = "./test_multivariate"
    output_path = os.path.abspath(os.path.join(os.getcwd(),output_path))

    dataset = StructuredDataset()
    dataset.import_dataset("./config/test/data/test.csv")
    explorer = Explorer(dataset)
    explorer.understand()
    explorer.explore_attributes()
    explorer.explore_metrics()
    visualization = Visualization(dataset)
    visualization.render_multivariate(format="png",path=output_path)

    isPlotted = {"spearman": False, "pearson": False, "chi-square": False}
    for root, subdirs, files in os.walk(output_path):
        for filename in files:
            if "spearman" in filename:
                isPlotted["spearman"] = True
            if "pearson" in filename:
                isPlotted["pearson"] = True
            if "chi_square" in filename:
                isPlotted["chi-square"] = True
    if  sum(isPlotted.values()) < len(isPlotted.values()):
        remove_dir(output_path)
        assert False
    else:
        remove_dir(output_path)
        assert True

def test_render_all():
    number_of_plots = 0
    output_path = "./test_visualization_all"
    output_path = os.path.abspath(os.path.join(os.getcwd(),output_path))

    dataset = StructuredDataset()
    dataset.import_dataset("./config/test/data/test.csv")
    explorer = Explorer(dataset)
    explorer.understand()
    explorer.explore_attributes()
    explorer.explore_metrics()
    visualization = Visualization(dataset)
    visualization.render_all(format="png",path = output_path)

    mapping_type_with_count = {
        DataType.NUMERIC.value: 2,
        DataType.ORDINAL.value: 2,
        DataType.DATE.value: 6,
        DataType.NOMINAL.value: 2,
        DataType.STRING.value:0,
        DataType.TEXT.value:0,
    }
    for attr in visualization.attribute_info:
        visualization.render_univariate(attr_name=attr.name, format="png")
        number_of_plots += mapping_type_with_count[attr.type]

    correlation = ["pearson", "spearman", "chi_square"]
    for corr in correlation:
        if corr in dataset.info.metrics.keys():
            number_of_plots +=1

    if len(os.listdir(output_path)) == number_of_plots:
        remove_dir(output_path)
        assert True
    else:
        remove_dir(output_path)
        assert False