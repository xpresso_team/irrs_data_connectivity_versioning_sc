__author__ = "Srijan Sharma"

from xpresso.ai.core.data.structured_dataset import StructuredDataset
from xpresso.ai.core.data.dataset_info import DatasetInfo
from xpresso.ai.core.data.dataset_explorer import Explorer
from xpresso.ai.core.data.dataset_type import DECIMAL_PRECISION as decimal_percision

from scipy import stats
import pandas as pd
import numpy as np
import collections
from dateutil.parser import parse


def test_structured_data_understanding():
    data_path = "./config/test/data/test.csv"
    col_type_mapping = {"Numeric_int_col": "numeric", "Numeric_float_col":
        "numeric", "Category_int_col": "nominal", "Category_object_col":
                            "nominal", "Date_object_col": "date", "String_obj_col": "string",
                        "Text_obj_col": "text"}

    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    explorer = Explorer(dataset)
    explorer.understand()

    for attr in dataset.info.attributeInfo:
        if attr.name in col_type_mapping.keys() and col_type_mapping[
            attr.name] is not attr.type:
            assert False
    print("Data Understanding test case passed")
    assert True


def test_structured_data_exploration_univariate():
    data_path = "./config/test/data/test.csv"

    data = pd.read_csv(data_path)

    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    explorer = Explorer(dataset)
    explorer.understand()
    explorer.explore_attributes()
    for attr in dataset.info.attributeInfo:

        na_count = float(data[attr.name].isna().sum())
        na_count_percentage = round((na_count / data[attr.name].size) * 100, decimal_percision)
        attr_metrics = attr.metrics

        if attr.type is "numeric":

            min = round(np.min(data[attr.name]), decimal_percision)
            max = round(np.max(data[attr.name]), decimal_percision)
            mean = round(np.mean(data[attr.name]), decimal_percision)
            std = round(np.std(data[attr.name], ddof=1), decimal_percision)
            var = std*std
            quartiles = data[attr.name].quantile([.0, .25, .5, .75, 1]).round(
                decimal_percision).values
            median = quartiles[2]
            iqr = quartiles[3] - quartiles[1]
            mode = round(stats.mode(data[attr.name])[0][0],decimal_percision)
            deciles = pd.qcut(data[attr.name], 10, duplicates='drop',
                              retbins=True)[1].round(decimal_percision)
            kurtosis = round(data[attr.name].kurtosis(), decimal_percision)

            if min != attr_metrics["min"] or max != attr_metrics["max"] or \
                    mean != attr_metrics["mean"] or std != attr_metrics["std"] \
                    or na_count != attr_metrics["na_count"] or \
                    na_count_percentage != attr_metrics["na_count_percentage"] \
                    or collections.Counter(quartiles) != \
                    collections.Counter(attr_metrics["quartiles"]) or \
                    collections.Counter(deciles) != collections.Counter(
                attr_metrics["deciles"]) or kurtosis != attr_metrics[
                "kurtosis"] or mode != attr_metrics["mode"] or  var != \
                    attr_metrics["var"]  or median != attr_metrics["median"] \
                or iqr != attr_metrics["iqr"] :
                assert False

        elif attr.type is "nominal" or attr.type is "ordinal":
            freq_count = data[attr.name].value_counts().to_dict()
            mode = stats.mode(data[attr.name])[0][0]
            if na_count != attr_metrics["na_count"] or na_count_percentage \
                    != attr_metrics["na_count_percentage"] or freq_count != \
                    attr_metrics["freq_count"] or mode != attr_metrics["mode"]:
                assert False

        elif attr.type is "date":
            try:
                data[attr.name] = data[attr.name].apply(lambda x: x
                if pd.isna(x) else parse(x))
            except:
                assert False

            if na_count != attr_metrics["na_count"] or na_count_percentage \
                    != attr_metrics["na_count_percentage"] or data[
                attr.name].min() != attr_metrics["min"] and data[attr.name].max() != attr_metrics["max"]:
                assert False
    print("Univariate Data Exploration test case passed")
    assert True


def test_structured_data_exploration_multivariate():
    col_type_mapping = {"Numeric_int_col": "numeric", "Numeric_float_col":
        "numeric", "category_int_col": "nominal", "Category_object_col":
                            "nominal", "date_object_col": "date", "String_obj_col": "string",
                        "Text_obj_col": "text"}

    possible_metric = ["pearson", "spearman", "chi_square"]

    data_path = "./config/test/data/test.csv"
    data = pd.read_csv(data_path)

    dataset = StructuredDataset()
    dataset.import_dataset(data_path)

    explorer = Explorer(dataset)
    explorer.understand()
    explorer.explore_metrics()

    for corrs in dataset.info.metrics:
        if corrs is "pearson":
            for val in dataset.info.metrics[corrs]:
                if data[val[0]].corr(data[val[1]], method="pearson").round(
                        decimal_percision) != dataset.info.metrics[corrs][val]:
                    assert False
        elif corrs is "spearman":
            for val in dataset.info.metrics[corrs]:
                if data[val[0]].corr(data[val[1]], method="spearman").round(
                        decimal_percision) != dataset.info.metrics[corrs][val]:
                    assert False
        elif corrs is "chi_square":
            for val in dataset.info.metrics[corrs]:
                if DatasetInfo.ChiSquareTest(data, val[0], val[1]) != \
                        dataset.info.metrics[corrs][val]:
                    assert False

    print("Multivariate Data Exploration test case passed")
    assert True
