import time

from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger
import os
import json
from urllib.parse import urljoin
import requests


def test_XprLogger_File():
    """

    Returns:
        asserts True if the messages logged using XprLogger are present in the local log files
        generated, else asserts false

    """

    PROJECT_NAME = "Project Name"
    LOGGING_SECTION = "log_handler"
    LOG_TO_FILE_BOOL = "log_to_file"
    LOGS_FOLDER_PATH = "logs_folder_path"

    dev_config = XprConfigParser(XprConfigParser.DEFAULT_CONFIG_PATH_XPR_LOG)
    logger = XprLogger()

    info_msg = "Test Message Info Level File Handler"
    logger.info(info_msg, stack_info=True, exc_info=True)

    error_msg = "Test Message Error Level File Handler"
    logger.error(error_msg, stack_info=True, exc_info=True)

    warning_file = os.path.join(dev_config[LOGGING_SECTION][LOGS_FOLDER_PATH],
                                '.'.join((dev_config[PROJECT_NAME], "log")))
    error_file = os.path.join(dev_config[LOGGING_SECTION][LOGS_FOLDER_PATH],
                              '.'.join((dev_config[PROJECT_NAME], "err")))

    if dev_config[LOGGING_SECTION][LOG_TO_FILE_BOOL]:
        if not os.path.exists(warning_file) or not os.path.exists(error_file):
            assert False, "Unable to find the created log files in the directory specified in config"
        else:
            warning_file_content = open(warning_file).read()
            error_file_content = open(error_file).read()
            if info_msg not in warning_file_content or error_msg not in warning_file_content \
                or error_msg not in error_file_content or info_msg in error_file_content:
                try:
                    os.remove(warning_file)
                    os.remove(error_file)
                except OSError as err:
                    print("Unable to remove directory. \n{}".format(str(err)))
                # WARNING_LOG_FILE = "warning_log_file"
                # ERROR_LOG_FILE  = "error_log_file"
                assert False, "Unable to find the log messages in the log files"
    else:
        if os.path.exists(warning_file) or os.path.exists(error_file):
            assert False, "File handler False, but found log files"

    assert True, "Test Passed for XprLogger(FIleHandler)"


def check_message(endpoint, message, level):
    """
    Args:
        endpoint: Elastic search endpoint
        message: Log message to be used for filtering
        level: Level of the log message

    Returns:
        bool : True if document don exist and the retrieved level is same
        as the logged level, else returns False

    """

    data = {"query": {"match_phrase": {"msg": message}}}
    resp = requests.post(endpoint, data=json.dumps(data),
                         headers={'Content-Type': 'application/json'})
    resp = json.loads(resp.text)
    if len(resp["hits"]["hits"]) <= 0:
        return False
    else:
        resp_message = resp["hits"]["hits"][0]["_source"]["msg"]
        resp_level = resp["hits"]["hits"][0]["_source"]["levelname"]
        if resp_message != message or level != resp_level:
            return False

    return True


def check_existence(endpoint, message):
    """
    Check for the existence of a document in elastic search
    Args:
        endpoint(str): Elastic search endpoint
        message (str): Message to be used for searching

    Returns:
        bool : True if document is present in the index, else returns False

    """

    data = {"query": {"match_phrase": {"msg": message}}}
    resp = requests.post(endpoint, data=json.dumps(data),
                         headers={'Content-Type': 'application/json'})
    resp = json.loads(resp.text)
    if len(resp["hits"]["hits"]) <= 0:
        return True
    return False


def remove_test_logs(endpoint, info_msg, error_msg):
    """
    Removes the indexed logs matching the info_msg and error_msg type from the
    elastic search
    Args:
        endpoint  (str): delete url of elastic search
        info_msg  (str): message value to be removed from the index
        error_msg (str): message value to be removed from index

    Returns:
        bool :  True if documents removed successfully, else returns False
    """
    data_info = {"query": {"match_phrase": {"msg": info_msg}}}
    data_error = {"query": {"match_phrase": {"msg": error_msg}}}
    resp_info = requests.post(endpoint, data=json.dumps(data_info),
                              headers={'Content-Type': 'application/json'})
    resp_error = requests.post(endpoint, data=json.dumps(data_error),
                               headers={'Content-Type': 'application/json'})
    if resp_info.status_code != 200 or resp_error.status_code != 200:
        print("Unable to remove the test logs from the elastic search")
        return False
    return True


def test_Xprlogger_Logstash():
    """

    Returns:
        asserts True if the messages logged using XprLogger are present in
        the elastic search instance, else asserts false

    """
    dev_config = XprConfigParser(XprConfigParser.DEFAULT_CONFIG_PATH_XPR_LOG)
    default_test_config_path = \
            "/opt/xpresso.ai/config/test/logger_test_config.json"
    test_config = XprConfigParser(default_test_config_path)
    logger = XprLogger()

    info_msg = "Test Message Info Level Logstash Handler"
    logger.info(info_msg, stack_info=True, exc_info=True)

    error_msg = "Test Message Error Level Logstash Handler"
    logger.error(error_msg, stack_info=True, exc_info=True)

    LOG_HANDLER = "log_handler"
    LOG_TO_ELK_BOOL = "log_to_elk"
    ELASTIC_SEARCH_IP = "elastic_search_url"

    base_url = test_config[LOG_HANDLER][ELASTIC_SEARCH_IP]
    endpoint = urljoin(base_url, "xpresso_logs/_search")
    remove_log_endpoint = urljoin(base_url, "xpresso_logs/_delete_by_query")
    time.sleep(5)
    if dev_config[LOG_HANDLER][LOG_TO_ELK_BOOL]:
        if not check_message(endpoint, info_msg, "INFO") or not check_message(
                endpoint, error_msg, "ERROR"):
            remove_test_logs(remove_log_endpoint, info_msg, error_msg)
            assert False, "Unable to find the log messages indexed in " \
                          "elastic search"
        else:
            if not remove_test_logs(remove_log_endpoint, info_msg, error_msg):
                assert False, "unable to remove the test logs from " \
                              "the elastic search"
    else:
        if check_existence(endpoint, info_msg) or check_existence(endpoint,
                                                                  error_msg):
            assert False, "Log message found in the ES but log to elk " \
                          "parameter is set to False"
    assert True


if __name__ == "__main__":
    test_XprLogger_File()
    test_Xprlogger_Logstash()
