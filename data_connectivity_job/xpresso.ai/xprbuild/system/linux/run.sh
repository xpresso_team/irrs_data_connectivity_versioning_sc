#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application
export PYTHONPATH=${ROOT_FOLDER}

#TODO USE Gunicorn to run it
python3 ${ROOT_FOLDER}/xpresso/ai/admin/controller/server/api_server.py
