__all__ = ["Visualization"]
__author__ = ["Ashritha Goramane"]

from enum import Enum
import pandas as pd
from xpresso.ai.core.data.visualization import pie, bar, heatmap
from xpresso.ai.core.data.visualization import quartile
from xpresso.ai.core.data.attribute_info import DataType
from xpresso.ai.core.logging.xpr_log import XprLogger
import xpresso.ai.core.data.visualization.res.utils as utils

import os
from PyPDF2 import PdfFileMerger

class PlotType(Enum):
    """ 
    Enum class to standardize all the Plottype
    """
    PIE = "pie"
    QUARTILE = "quartile"
    BAR = "bar"
    HEATMAP = "heatmap"

    def __str__(self):
        return self.value


class Visualization:
    """Visualization class takes a dataset object and provide functions
    to render plots on the metrics of attribute"""

    def __init__(self, dataset):
        self.attribute_info = dataset.info.attributeInfo
        self.metric = dataset.info.metrics
        self.logger = XprLogger()

    def render_univariate(self, attr_name=None, plot_type=["quartile", "bar", "pie"],
                          format="html",path=None):
        isPlotted = False

        if attr_name is None and plot_type is None:
            self.logger.error("Both attr name and plot type cannot be  "
                              "None.Please provide at least one argument")

        elif attr_name is None and plot_type is not None:
            self.logger.info("Attribute name not provided. Plotting for all "
                             "possible attributes")

        elif plot_type is not  None and attr_name is None:
            self.logger.info("Plot type not provided. Using default plot type value :{}".format(plot_type))
            return

        for attr in self.attribute_info:
            if attr.name == attr_name or  attr_name is  None:
                if attr.type == DataType.NUMERIC.value:
                    if PlotType.QUARTILE.value in plot_type:
                        field = "quartiles"
                        if field in attr.metrics.keys():
                            self.logger.info("{} plot for Attribute : ({},{},{})".format(
                                PlotType.QUARTILE.value, attr.name, attr.type, field))
                            input_1 = list(attr.metrics[field])
                            quartile.new_plot(input_1,
                                              output_format=format,
                                              auto_render=True,
                                              plot_title="(" + attr.name + "," + attr.type + ")",
                                              output_path=path)
                            isPlotted = True
                        else:
                            self.logger.info("Attribute : {}, Field : {}. "
                                             "Unable to perform "
                                             "visualization. Key not present "
                                             "in metric".format(attr.name,field))

                    if PlotType.BAR.value in plot_type:
                        field = "pdf"
                        if field in attr.metrics.keys():
                            self.logger.info("{} plot for Attribute : ({},{},{})".format(
                                PlotType.BAR.value, attr.name, attr.type, field))
                            input_1 = list(attr.metrics[field].keys())
                            input_2 = list(attr.metrics[field].values())
                            bar.new_plot(input_1, input_2,output_format=format,
                                         auto_render=True,plot_title="(" + attr.name + "," + attr.type + ")",output_path=path)
                            isPlotted = True
                        else:
                            self.logger.info("Attribute : {}, Field : {}. "
                                             "Unable to perform "
                                             "visualization. Key not present "
                                             "in metric".format(attr.name,field))

                elif attr.type == DataType.NOMINAL.value or attr.type == DataType.ORDINAL.value:

                    field = "freq_count"
                    if field in attr.metrics.keys():
                        input_1 = list(attr.metrics[field].keys())
                        input_2 = list(attr.metrics[field].values())

                        if PlotType.PIE.value in plot_type:
                            self.logger.info("{} plot for Attribute : ({},{},{})".format(
                                PlotType.PIE.value, attr.name, attr.type, field))
                            pie.new_plot(input_1, input_2, output_format=format,
                                         auto_render=True,plot_title="(" + attr.name + "," + attr.type + ")",
                                         output_path=path)
                            isPlotted = True

                        if PlotType.BAR.value in plot_type:

                            print("{} plot for Attribute : ({},{},{})".format(
                                PlotType.BAR.value,attr.name,attr.type,field))
                            bar.new_plot(input_1, input_2,output_format=format,
                                         auto_render=True,plot_title="(" + attr.name + "," + attr.type + ")",
                                         output_path=path)

                            isPlotted = True
                    else:
                        self.logger.info("Attribute : {}, Field : {}. "
                                         "Unable to perform "
                                         "visualization. Key not present "
                                         "in metric".format(attr.name, field))


                elif attr.type == DataType.DATE.value:
                    plot_key = ["day_count", "month_count", "year_count"]
                    for field in plot_key:
                        if field in attr.metrics.keys():
                            input_1 = list(attr.metrics[field].keys())
                            input_2 = list(attr.metrics[field].values())

                            if PlotType.PIE.value in plot_type:

                                print("{} plot for Attribute : ({},{},{})".format(PlotType.PIE.value, attr.name,
                                                                                        attr.type,field))
                                pie.new_plot(input_1, input_2,output_format=format,
                                             auto_render=True,plot_title="(" + attr.name + "," + attr.type + ")",
                                             output_path=path)
                                isPlotted = True

                            if PlotType.BAR.value in plot_type:

                                print("{} plot for Attribute : ({},{},{})".format(PlotType.BAR.value, attr.name,
                                                                                        attr.type,field))
                                bar.new_plot(input_1, input_2,output_format=format,
                                             auto_render=True,plot_title="(" + attr.name + "," + attr.type + ")",
                                             output_path=path)

                                isPlotted = True
                        else:
                            self.logger.info("Attribute : {}, Field : {}. "
                                             "Unable to perform "
                                             "visualization. Key not present "
                                             "in metric".format(attr.name,field))

                if isPlotted is False:
                    self.logger.error("Unable to plot for Attribute : ({},{}) "
                                      "with provided plot types parameter: {}".format(
                        attr.name,attr.type, plot_type))
        return

    def render_multivariate(self, format="html",path=None):
        correlation = ["pearson", "spearman", "chi_square"]
        is_plotted = False
        for corr in correlation:
            if corr in self.metric.keys() :
                self.logger.info("{} plot for Correlation Type : {}".format(
                    PlotType.HEATMAP.value, corr))
                corr_df = pd.Series(list(self.metric[corr].values()),
                                    index=pd.MultiIndex.from_tuples(self.metric[corr].keys()))
                corr_df = corr_df.unstack().fillna(0)
                input_1 = corr_df.index.values.tolist()
                input_2 = corr_df.columns.values.tolist()
                input_3 = corr_df.to_numpy().tolist()
                heatmap.new_plot(input_1,input_2,input_3,output_format=format,
                                 auto_render=True,plot_title=corr,output_path=path)
                is_plotted = True
            else:
                self.logger.info("Unable to do Multivariate Visualisation for  Field : {}. "
                                 "Key not present in metric".format(corr))

        if not is_plotted:
            self.logger.error("No key found. Unable to plot for multivariate "
                              "metrics")

        return

    def render_all(self,format="html",path=None):
        for attr in self.attribute_info:
            self.render_univariate(attr_name=attr.name,format=format,path=path)
        self.render_multivariate(format=format,path=path)

        if format is "pdf":
            self.create_report(input_path=path,output_path=path)

    def create_report(self,input_path=utils.pdf_path,
                      output_path=utils.pdf_path):
        input_path = os.path.abspath(os.path.join(os.getcwd(),input_path))
        if input_path is None or not os.path.exists(input_path) or len(
                os.listdir(input_path))==0 or not any(fname.endswith('.pdf')
                                                    for fname in os.listdir(input_path)):
            self.logger.error("Invalid input path provided")

        if not os.path.exists(output_path):
            utils.make_container(output_path)

        merger = PdfFileMerger(strict=False)
        for file in os.listdir(input_path):
            if file.endswith("pdf"):
                self.logger.info("Adding file named {} to the report".format(
                    file))
                merger.append(os.path.join(input_path,file))

        merger.write(os.path.join(output_path,"report.pdf"))
        merger.close()