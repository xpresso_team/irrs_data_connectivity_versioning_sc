import alluxio
import alluxio.wire
from alluxio import option
from io import BytesIO
import pandas as pd
import telnetlib
import xlsxwriter


class AlluxioConnector:
    def __init__(self, alluxio_ip, alluxio_port):
        self.alluxio_ip = alluxio_ip
        self.alluxio_port = alluxio_port
        self.alluxio_ip = str(self.alluxio_ip)
        self.alluxio_port = str(self.alluxio_port)

    def get_data_raw(self, fileName):
        """Gets data with .txt extension from the File System and converts it into Dataframe"""
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        try:
            telnetlib.Telnet(self.alluxio_ip, self.alluxio_port)
            with client.open(fileName, 'r') as f:
                a = BytesIO(f.read())
            return a
        except OSError:
            print("check connection details")

    def get_data_text(self, fileName):
        """Gets data with .txt extension from the File System and converts it into Dataframe"""
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        try:
            telnetlib.Telnet(self.alluxio_ip, self.alluxio_port)
            with client.open(fileName, 'r') as f:
                a = BytesIO(f.read())
            df = pd.read_csv(a, sep="|")
            return df
        except OSError:
            print("check connection details")

    def get_data_csv(self, fileName):
        """Gets data with .csv extension from the File System and converts it into Dataframe"""
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        try:
            telnetlib.Telnet(self.alluxio_ip, self.alluxio_port)
            with client.open(fileName, 'r') as f:
                a = BytesIO(f.read())
            df = pd.read_csv(a, sep=",")
            return df
        except OSError:
            print("check connection details")

    def get_data_excel(self, fileName):
        """Gets data with .xlsx extension from the File System and converts it into Dataframe"""
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        try:
            telnetlib.Telnet(self.alluxio_ip, self.alluxio_port)
            with client.open(fileName, 'r') as f:
                a = BytesIO(f.read())
            df = pd.read_excel(a)
            return df
        except OSError:
            print("check connection details")

    # put data in text file
    def put_data_text(self, dataframe):
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        opt = alluxio.option.CreateFile(write_type=alluxio.wire.WRITE_TYPE_CACHE_THROUGH)
        with client.open('/abc.txt', 'w', opt) as abc:
            for i in range(dataframe.shape[0]):
                for j in range(dataframe.shape[1]):
                    abc.write(str(dataframe.iloc[i, j]))
                    if (j < dataframe.shape[1] - 1):
                        abc.write(' ')
                abc.write('\n')

    # put data in csv file
    def put_data_csv(self, dataframe):
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        opt = alluxio.option.CreateFile(write_type=alluxio.wire.WRITE_TYPE_CACHE_THROUGH)
        with client.open('/abc.csv', 'w', opt) as abc:
            for i in range(dataframe.shape[0]):
                for j in range(dataframe.shape[1]):
                    abc.write(str(dataframe.iloc[i, j]))
                    if (j < dataframe.shape[1] - 1):
                        abc.write(',')
                abc.write('\n')

    # put data in excel file
    def put_data_excel(self, dataframe):
        client = alluxio.Client(self.alluxio_ip, self.alluxio_port)
        writer = pd.ExcelWriter('output/excel.xlsx', engine='xlsxwriter')
        dataframe.to_excel(writer)
        writer.save()
        opt = alluxio.option.CreateFile(write_type=alluxio.wire.WRITE_TYPE_CACHE_THROUGH)
        with client.open('/abc.xlsx', 'w', opt) as xyzfile:
            with open('output/excel.xlsx', 'r') as inputfile:
                xyzfile.write(inputfile)
