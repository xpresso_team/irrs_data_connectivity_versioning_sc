import prestodb
import pandas as pd
from xpresso.ai.core.data.data_connectivity.Presto.presto_exception import PrestoConnectionException


class PrestoConnector:
    """ This class is used to interact with the any of the databases as per the configurations provided.
        The supported operation as of now are importing & exporting of data."""

    def __init__(self, presto_ip, presto_port, catalog, schema):
        self.presto_ip = presto_ip
        self.presto_port = presto_port
        self.catalog = catalog
        self.schema = schema

    def get_connector(self):
        """ This methods creates an connection to database with the provided configurations."""
        try:
            connector = prestodb.dbapi.connect(
                    host=self.presto_ip,
                    port=self.presto_port,
                    user="root",
                    catalog=self.catalog,
                    schema=self.schema
                )
            return connector
        except ConnectionError as exc:
            print(exc)
            raise PrestoConnectionException
        except Exception as exc:
            print(exc)
            pass

    def import_data(self, table, columns='*'):
        """ This method makes an select query"""
        try:
            connector = self.get_connector()
            query = "select " + columns + " from " + table
            cursor = connector.cursor()
            cursor.execute(query)

            results = cursor.fetchall()
            columns = []
            for column in cursor.description:
                columns.append(column[0])

            return pd.DataFrame(results,columns=columns)

        except ConnectionError as exc:
            print(exc)
            raise PrestoConnectionException
        except Exception as exc:
            print(exc)
            pass
        finally:
            connector.close()

    def export_data(self, table, dataframe):
        """ This method helps in writing the dataframe to provided database"""
        try:
            data_list = dataframe.values.tolist()
            list_of_tuples = list(map(tuple, data_list))
            batch_size = 5
            response = 0

            connector = self.get_connector()
            cursor = connector.cursor()

            for index in range(0, len(list_of_tuples), batch_size):
                input_list = str(list_of_tuples[index: index + batch_size])[1:-1]
                query = "INSERT INTO " + table + " VALUES " + input_list
                cursor.execute(query)
                response += cursor.fetchall()[0][0]

            return response
        except ConnectionError as exc:
            print(exc)
            raise PrestoConnectionException
        except Exception as exc:
            print(exc)
            pass
        finally:
            connector.close()