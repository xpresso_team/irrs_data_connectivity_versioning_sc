class Error(Exception):
    pass


class PrestoIPMissingInConfig(Error):
    pass


class PrestoPortMissingInConfig(Error):
    pass


class PrestoCatalogMissingInConfig(Error):
    pass


class PrestoSchemaMissingInConfig(Error):
    pass


class PrestoTableMissingInConfig(Error):
    pass


class PrestoConfigFileIsMissing(Error):
    pass


class PrestoConnectionException(Error):
    pass


class PrestoColumnsMissingInConfig(Error):
    pass
