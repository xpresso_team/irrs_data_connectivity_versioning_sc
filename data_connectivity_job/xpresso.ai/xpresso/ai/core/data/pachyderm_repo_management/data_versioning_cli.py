"""
    Class for Data Versioning using Pachyderm
"""

import os
import shutil
import click
from xpresso.ai.core.data.pachyderm_repo_management.pachyderm_repo_manager \
    import PachydermRepoManager
from xpresso.ai.core.data.data_connectivity.connector import DataConnector as Dc
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    CLICommandFailedException
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = "data_versioning_cli"
__author__ = "Sanyog Vyawahare"


class DataVersioning:
    """
        To push and pull data from pachyderm cluster
    """
    COMMAND_KEY_WORD = "command"
    REPO_ARGUMENT = "repo"
    COMMIT_ARGUMENT = "commit"
    DESCRIPTION_ARGUMENT = "desc"
    IN_PATH_ARGUMENT = "inpath"
    OUT_PATH_ARGUMENT = "outpath"
    BRANCH_ARGUMENT = "branch"
    DATASET_ARGUMENT = "dataset"

    TABLE_ARGUMENT = 'table'
    COLUMN_ARGUMENT = 'columns'
    CATALOG = "mysql"
    SCHEMA = "presto"

    def __init__(self):
        self.arguments = {}
        self.supported_commands = {}
        self.info_files = {}
        self.initialize_commands()
        self.logger = XprLogger()
        self.repomanager = PachydermRepoManager()
        self.xpr_con = Dc('./config/common.json')

    def import_data(self):
        """
        Import data from alluxio or presto
        :return:
            Returns a pandas DataFrame and output path
        """
        filename, table, column, output_path = self.fetch_arguments()
        if not output_path:
            raise CLICommandFailedException("Output path mandatory, Please specify using -out or --outpath")

        if filename:
            df = self.xpr_con.import_data(filename, None, None, table, column)
        else:
            if not column:
                raise CLICommandFailedException("Specify Columns to fetch")
            if not table:
                raise CLICommandFailedException("Please provide the table name")

            df = self.xpr_con.import_data(filename, self.CATALOG, self.SCHEMA, table, column)

        return df, output_path

    def import_raw_data(self):
        """ Get raw data """
        input_path = self.extract_argument(self.IN_PATH_ARGUMENT)
        output_path = self.extract_argument(self.OUT_PATH_ARGUMENT)
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        raw_bytes = self.xpr_con.import_raw_file(input_path)
        with open(output_path, 'rb') as f:
            f.write(raw_bytes)

    def import_data_as_excel(self):
        """
        Saves df as excel file
        """
        input_path = self.extract_argument(self.IN_PATH_ARGUMENT)

        if not input_path:
            table = self.extract_argument(self.TABLE_ARGUMENT)
            input_path = '/' + table + '.xlsx'

        df, output_path = self.import_data()

        if not os.path.exists(output_path):
            os.makedirs(output_path)
        df.to_excel(os.path.join(output_path, os.path.basename(input_path)))

    def import_data_as_csv(self):
        """
        Saves df as csv file
        """
        input_path = self.extract_argument(self.IN_PATH_ARGUMENT)
        if not input_path:
            table = self.extract_argument(self.TABLE_ARGUMENT)
            input_path = '/' + table + '.csv'

        df, output_path = self.import_data()

        if not os.path.exists(output_path):
            os.makedirs(output_path)
        df.to_csv(os.path.join(output_path, os.path.basename(input_path)))

    def import_data_as_txt(self):
        """
        Saves df as txt file
        """
        df, output_path = self.import_data()

        filename = self.extract_argument(self.IN_PATH_ARGUMENT)
        if not filename:
            raise CLICommandFailedException("Please specify text file path using -in or --inpath")
        file_obj = open(os.path.basename(filename), 'w+')
        file_obj.write(df.to_string())
        file_obj.close()
        # os.rename(filename, os.path.join(output_path, filename))

    def data_push(self):
        """
        Push the data into the pachyderm repo

        :return:
            new commit id
        """
        repo_name = self.extract_argument(self.REPO_ARGUMENT)
        branch_name = self.extract_argument(self.BRANCH_ARGUMENT)
        dataset = self.extract_argument(self.DATASET_ARGUMENT)
        description = self.extract_argument(self.DESCRIPTION_ARGUMENT)

        new_commit_id = self.repomanager.push_dataset(repo_name, branch_name, dataset, description)
        return new_commit_id

    def data_pull(self):
        """
        Pulls the data from the pachyderm repo
            Arguments to be passed
            Repo Name
            Branch Name
            Output Path
            Input Path(optional)
            Commit (optional)

        :return:
            path to dir on local machine
        """

        repo_name = self.extract_argument(self.REPO_ARGUMENT)
        branch_name = self.extract_argument(self.BRANCH_ARGUMENT)
        in_path = self.extract_argument(self.IN_PATH_ARGUMENT)
        out_path = self.extract_argument(self.OUT_PATH_ARGUMENT)
        commit = self.extract_argument(self.COMMIT_ARGUMENT)

        if not in_path:
            in_path = '/'

        new_dir_path = self.repomanager.pull_dataset(repo_name, branch_name, in_path, commit)
        print(f"Data Pull from {new_dir_path} to {out_path}")
        os.system(f"ls -laR {new_dir_path}")
        updated_new_dir_path = os.path.join(new_dir_path, in_path.lstrip('/'))
        if out_path:
            if not os.path.exists(out_path):
                os.makedirs(out_path)

            print(f"Copying {updated_new_dir_path} -> {out_path}")
            destination_folder = os.path.join(out_path, updated_new_dir_path.split("/")[-1])
            print(f"Removing if exists {destination_folder}")
            os.system(f'rm -rf {destination_folder}')
            shutil.move(updated_new_dir_path, out_path)

        print(f"Copy {updated_new_dir_path} -> {out_path} Done")
        return new_dir_path

    def execute(self, **kwargs):
        """
        Validates the command provided and calls the relevant function for
        execution

        Args:
            kwargs: It takes kwargs as argument which should contain the
                    argument passed in command line
        """
        self.arguments = kwargs
        if self.COMMAND_KEY_WORD not in self.arguments:
            raise CLICommandFailedException("No valid command provided")

        command = self.arguments[self.COMMAND_KEY_WORD]

        if command not in self.supported_commands:
            raise CLICommandFailedException("Invalid command found")
        try:
            result = self.supported_commands[command]()
            return result
        except TypeError as exception:
            raise CLICommandFailedException("Command cannot be executed {}".format(exception))

    def extract_argument(self, argument):
        """
        :param argument:
            Name of argument to extract
        :return:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def put_files(self):
        """
        Pushes the files into the pachyderm repo
        :return:
            new commit id
        """
        self.info_files['repo_name'] = self.extract_argument(self.REPO_ARGUMENT)
        self.info_files['branch_name'] = self.extract_argument(self.BRANCH_ARGUMENT)
        self.info_files['path'] = self.extract_argument(self.IN_PATH_ARGUMENT)
        self.info_files['dataset_name'] = self.extract_argument(self.DATASET_ARGUMENT)
        self.info_files['description'] = self.extract_argument(self.DESCRIPTION_ARGUMENT)

        new_commit_id = self.repomanager.push_files(self.info_files)

        return new_commit_id

    def initialize_commands(self):
        """
        Initialize the supported command variable

        """
        self.supported_commands = {
            "import_csv": self.import_data_as_csv,
            "import_excel": self.import_data_as_excel,
            "import_txt": self.import_data_as_txt,
            "import_raw": self.import_raw_data,

            "data_push": self.data_push,
            "put_files": self.put_files,

            "data_pull": self.data_pull
        }

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        :return:
            Returns arguments
        """
        filename = self.extract_argument(self.IN_PATH_ARGUMENT)
        table = self.extract_argument(self.TABLE_ARGUMENT)
        column = self.extract_argument(self.COLUMN_ARGUMENT)
        output_path = self.extract_argument(self.OUT_PATH_ARGUMENT)
        return filename, table, column, output_path


@click.command()
@click.argument('command')
@click.option('-r', '--repo', type=str, help='Name of the repo')
@click.option('-b', '--branch', type=str, help='Branch name')
@click.option('-c', '--commit', type=str, help='Commit Id')
@click.option('-d', '--dataset', help='Dataset object')
@click.option('-out', '--outpath', type=str,
              help='Path where you want to pull')
@click.option('-in', '--inpath', type=str,
              help='Path of the file you want to fetch')
@click.option('--desc', type=str, help='Description regarding the function to perform')
@click.option('-t', '--table', type=str, help='Name of the table in presto data source')
@click.option('-col', '--columns', type=str,
              help='Name of column inside the table stored in presto data source')
def cli_options(**kwargs):
    result = DataVersioning()
    try:
        result.execute(**kwargs)

    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    cli_options()
