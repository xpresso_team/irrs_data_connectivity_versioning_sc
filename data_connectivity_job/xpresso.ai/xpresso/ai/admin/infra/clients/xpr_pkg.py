""" Installs bundles within a machine
This is Deprecated. Use xprctl install_bundle instead
"""

import argparse
import ast

from xpresso.ai.admin.infra.bundles.bundle_manager import ExecutionType
from xpresso.ai.admin.infra.bundles.bundle_manager import BundleManager
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import BundleFailedException


CONFIG_SECTION = "xpr_pkg"
CONFIG_MANIFEST_KEY = "manifest_path"


def main():
    parser = parse_arguments()
    args = parser.parse_args()

    bundle_manager = BundleManager(args.conf)

    if args.type and args.parameters:
        # Check if args.parameters is a valid dict
        try:
            parameters = ast.literal_eval(args.parameters)
        except (ValueError, TypeError):
            raise BundleFailedException("Parameter provided is not valid dict")
        bundle_manager.run(bundle_to_install=str(args.bundle),
                           execution_type=args.type,
                           parameters=parameters)
    elif args.type:
        bundle_manager.run(bundle_to_install=str(args.bundle),
                           execution_type=args.type)
    elif args.list:
        bundle_list = bundle_manager.list()
        print("Supported Bundle List : {}".format(bundle_list))
    else:
        parser.print_usage()


def parse_arguments():
    """ Reads commandline argument to identify which clients group to
    install
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--conf",
                        required=True,
                        help="Root configuration file")
    parser.add_argument("--bundle",
                        type=str,
                        required=False,
                        help="Bundle group name to install. Must "
                             "match the name from manifest file")
    parser.add_argument("--type",
                        type=ExecutionType,
                        choices=list(ExecutionType),
                        required=False,
                        help="Bundle group name to install. Must "
                             "match the name from manifest file")
    parser.add_argument("--list",
                        action='store_true',
                        required=False,
                        default=False,
                        help="List supported bundle")
    parser.add_argument("--parameters",
                        type=str,
                        required=False,
                        help="Dictionary of Additional parameters")
    return parser


if __name__ == "__main__":
    main()
