""" Bundle to initialise the master node on a kubernetes cluster"""
from xpresso.ai.admin.controller.persistence import persistentce_connection

__all__ = ['KubeadmMasterBundle']
__author__ = 'Sahil Malav'

import os
import argparse
import time

from xpresso.ai.admin.infra.bundles.abstract_bundle import AbstractBundle
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils import linux_utils
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    CommandExecutionFailedException


class KubeadmMasterBundle(AbstractBundle):
    KUBE_SECTION = 'kubernetes'
    CIDR_KEY = 'pod_network_cidr'
    CONTROLLER_SECTION = 'controller'
    PACKAGES = 'bundles_setup'

    PARAMETER_ADDRESS_NAME = "address"
    PARAMETER_KEY = "parameters"


    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 executor=None):
        if not executor:
            executor = LocalShellExecutor()
        super().__init__(executor)
        self.config = XprConfigParser(config_path)
        self.persistence_manager = persistentce_connection.create_persistence_object(self.config)

    def get_address_from_parameter(self, **kwargs):
        address = ""
        if (self.PARAMETER_KEY in kwargs and kwargs[self.PARAMETER_KEY] and
              self.PARAMETER_ADDRESS_NAME in kwargs[self.PARAMETER_KEY]):
            address = kwargs[self.PARAMETER_KEY][self.PARAMETER_ADDRESS_NAME]

        return address


    def execute(self, **kwargs):
        """
        installs kubernetes master node on the machine.
        """

        logger = XprLogger()
        if not linux_utils.check_root():
            logger.fatal("Please run this as root")
        self.cleanup()
        logger.info("Initialising Kubernetes master node...")
        try:
            pod_network_cidr = self.config[self.PACKAGES][self.KUBE_SECTION][self.CIDR_KEY]
            init = 'kubeadm init --token-ttl=0 --pod-network-cidr={}'.format(
                pod_network_cidr)
            return_code = self.executor.execute(init, )
            if return_code != 0:
                raise CommandExecutionFailedException("Kubernetes init failed")

            # waiting time for master node to become active
            time.sleep(90)

            kube_config = 'echo "KUBECONFIG=/etc/kubernetes/admin.conf" >> /etc/environment'
            self.executor.execute(kube_config)
            os.environ["KUBECONFIG"] = "/etc/kubernetes/admin.conf"
            kube_directory = '$HOME/.kube'
            kube_config = f'mkdir -p {kube_directory} -m 755'
            self.executor.execute(kube_config)
            copy_config = 'sudo cp -f /etc/kubernetes/admin.conf' \
                          ' $HOME/.kube/config'
            self.executor.execute(copy_config, )
            chown = 'sudo chown $(id -u):$(id -g) $HOME/.kube/config'
            self.executor.execute(chown, )
            flannel = 'kubectl apply -f https://raw.githubusercontent.com' \
                      '/coreos/flannel/master/Documentation/kube-flannel.yml'
            self.executor.execute(flannel, )

            # Update DB with
            generate_api_token = "kubectl get secret $(kubectl get " \
                                 "serviceaccount default -o jsonpath=" \
                                 "'{.secrets[0].name}') -o jsonpath=" \
                                 "'{.data.token}' | base64 --decode"
            status, stdout, stderr = self.executor.execute_with_output(generate_api_token, )
            if status != 0 or len(stderr.decode('utf-8')):
                raise CommandExecutionFailedException("Token generation failed")

            join_command = "kubeadm token create --print-join-command"
            (return_code, join_output, _) = self.execute_command_with_output(join_command)
            if return_code != 0:
                raise CommandExecutionFailedException("Kubernetes join command print failed")
            join_command = join_output.decode("utf-8")

            token = stdout.decode("utf-8")
            master_ip = self.get_address_from_parameter(**kwargs)
            if not master_ip or master_ip == "localhost":
                master_ip = linux_utils.get_ip_address()
            if not self.persistence_manager.find("nodes", {"address": master_ip}):
                self.persistence_manager.insert("nodes", {"address": master_ip,
                                                          "nodetype": "CLUSTER_MASTER",
                                                          "name": f"node_{master_ip}",
                                                          "provisionStatus" : True, 
                                                          "activationStatus" : True,
                                                          "token": token,
                                                          "join_command": join_command},
                                                duplicate_ok=False)
            else:
                self.persistence_manager.update("nodes", {"address": master_ip},
                                                {"token": token,
                                                 "join_command": join_command})

            api_access = 'kubectl create clusterrolebinding permissive-binding \
                                  --clusterrole=cluster-admin \
                                  --user=admin \
                                  --user=kubelet \
                                  --group=system:serviceaccounts'
            self.executor.execute(api_access, )
            docker_secret = \
                'kubectl create secret docker-registry dockerkey ' \
                '--docker-server https://dockerregistry.xpresso.ai/ ' \
                '--docker-username xprdocker --docker-password Abz00ba@123'
            self.executor.execute(docker_secret, )

        except CommandExecutionFailedException as e:
            logger.error("Failed to initialise master. \n{}".format(str(e)))
            return False
        return True

    def status(self, **kwargs):
        self.logger.info("Checking if kubernetes master is installed"
                         "bundles are installed")
        return self.execute_command("kubectl cluster-info") == 0

    def cleanup(self):
        reset = 'yes | kubeadm reset'
        self.executor.execute(reset, )
        kube_restart = ('systemctl stop kubelet; systemctl stop docker;'
                        'rm -rf /var/lib/cni/; rm -rf /var/lib/kubelet/*; '
                        'rm -rf /etc/cni/; '
                        'ifconfig cni0 down; ifconfig flannel.1 down;'
                        'ifconfig docker0 down'
                        'systemctl start kubelet; systemctl start docker;'
                        'ip link delete cni0;ip link delete flannel.1;')
        self.executor.execute(kube_restart, )

    def install(self, **kwargs):
        self.execute(**kwargs)

    def uninstall(self, **kwargs):
        self.cleanup()

    def start(self, **kwargs):
        self.execute()

    def stop(self, **kwargs):
        self.cleanup()


def parse_arguments():
    """ Reads commandline argument to identify which clients group to
    install
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--run",
                        required=False,
                        help="Method to execute")

    return parser


if __name__ == "__main__":
    kube_master = KubeadmMasterBundle()

    parser = parse_arguments()
    args = parser.parse_args()
    if 'run' in args:
        if args.run == 'execute':
            kube_master.execute()
        elif args.run == 'clean':
            kube_master.cleanup()
        else:
            kube_master.execute()
    else:
        kube_master.execute()
