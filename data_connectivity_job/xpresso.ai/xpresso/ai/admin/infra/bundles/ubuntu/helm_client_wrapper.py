"""
Python wrapper for handling helm client through terminal
"""

__all__ = ["HelmClient"]
__author__ = ["Gopi Krishna"]

from copy import deepcopy

from xpresso.ai.admin.infra.bundles.local_shell_executor import LocalShellExecutor
from xpresso.ai.core.utils import linux_utils
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    CommandExecutionFailedException, HelmRemovalException, HelmSetupException
from xpresso.ai.core.logging.xpr_log import XprLogger


class HelmClient:
    """
    Helm client wrapper with methods to access helm packages and installations
    """
    SNAP_PACKAGE = "snap"
    SNAP_INSTALL_COMMAND = "apt update && apt install snapd"
    HELM_PACKAGE = "helm"
    HELM_INSTALL_COMMAND = "snap install helm --classic"
    HELM_INIT_COMMAND = "helm init"
    HELM_STOP_COMMAND = "helm reset"
    HELM_INSTALL_CHART_COMMAND = "helm install --namespace {NAMESPACE} "\
                                 "--name {RELEASE_NAME} -f {SPEC_PATH} " \
                                 "{CHART_NAME}"
    HELM_DELETE_CHART_COMMAND = "helm del --purge {RELEASE_NAME}"
    DELETE_HELM_COMMAND = "snap remove helm"

    def __init__(self):
        self.executor = LocalShellExecutor()
        self.logger = XprLogger()

    def setup_helm(self):
        """
        sets up helm and starts tiller server on the local kubernetes cluster
        """
        try:
            self.install_snap()
            self.install_helm()
            self.initiate_helm()
        except CommandExecutionFailedException as err:
            self.logger.error("error while setting up helm is: \n", err)
            raise HelmSetupException()

    def install_snap(self):
        """
        installs snap package manager in case not present
        """
        if not linux_utils.check_if_a_command_installed(self.SNAP_PACKAGE):
            self.logger.info("installing snap package manager")
            self.executor.execute(self.SNAP_INSTALL_COMMAND)

    def install_helm(self):
        """
        helm charts installation
        """
        if not linux_utils.check_if_a_command_installed(self.HELM_PACKAGE):
            self.logger.info("installing helm using snap")
            self.executor.execute(self.HELM_INSTALL_COMMAND)

    def initiate_helm(self):
        """
        initiate helm by deploying tiller server on kubernetes cluster
        """
        self.logger.info("initialising helm and starting tiller server")
        self.executor.execute(self.HELM_INIT_COMMAND)

    def stop_helm(self):
        """
        stops tiller server on kubernetes cluster i.e. indirectly
        stops helm
        """
        self.executor.execute(self.HELM_STOP_COMMAND)

    def install_new_chart(self, chart_name, namespace, release_name, spec_path):
        """

        :param chart_name:
        :param namespace:
        :param release_name:
        :param spec_path:
        :return:
        """
        install_chart_command = deepcopy(self.HELM_INSTALL_CHART_COMMAND)
        install_chart_command = install_chart_command.replace("{NAMESPACE}",
                                                              namespace)
        install_chart_command = install_chart_command.replace("{RELEASE_NAME}",
                                                              release_name)
        install_chart_command = install_chart_command.replace("{SPEC_PATH}",
                                                              spec_path)
        install_chart_command = install_chart_command.replace("{CHART_NAME}",
                                                              chart_name)
        self.logger.info("")
        self.executor.execute(install_chart_command)

    def delete_chart(self, release_name):
        """
        deletes a helm chart from the cluster

        :param release_name:
            name of the release in which chart is installed
        """
        delete_chart_command = deepcopy(self.HELM_DELETE_CHART_COMMAND)
        delete_chart_command = delete_chart_command.replace("{RELEASE_NAME}",
                                                            release_name)
        self.executor.execute(delete_chart_command)

    def delete_helm(self):
        """
        deletes helm and its installed resources from the kubernetes cluster
        """
        self.executor.execute(self.DELETE_HELM_COMMAND)