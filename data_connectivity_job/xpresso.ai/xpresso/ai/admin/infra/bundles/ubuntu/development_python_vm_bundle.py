"""Abstract base class for bundles object"""


__all__ = ['DevelopmentPythonVMBundle']
__author__ = 'Naveen Sinha'


from xpresso.ai.admin.infra.bundles.abstract_bundle import AbstractBundle
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser


class DevelopmentPythonVMBundle(AbstractBundle):
    """
    Installs Common Bundles in the Debian VM
    """

    CONFIG_SECTION = "development_vm"
    REQUIREMENT_KEY = "requirement_file"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 executor=None):
        if not executor:
            executor = LocalShellExecutor()
        super().__init__(executor)
        self.config = XprConfigParser(config_path)["bundles_setup"]

    def status(self, **kwargs):
        """
        Checks the status of bundles in the VM
        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        snap_list_check = "snap list | grep pycharm"
        (_, output, _) = self.execute_command_with_output(snap_list_check)
        if "pycharm" not in output.decode("utf-8"):
            return False

        snap_list_check = "pip3 freeze | cut -f1 -d="
        (_, output, _) = self.execute_command_with_output(snap_list_check)
        output_list = set(output.decode("utf-8").splitlines())
        req_file = self.config[self.CONFIG_SECTION][self.REQUIREMENT_KEY]
        req_list_check = "cat {}| cut -f1 -d=".format(req_file)
        (_, output, _) = self.execute_command_with_output(req_list_check )
        req_list = set(output.decode("utf-8").splitlines())
        if not req_list.issubset(output_list):
            return False
        return True

    def install(self, **kwargs):
        """
        Installs all bundles for development VM
        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """

        # Setup Python Bundles
        self.logger.info("Setting up base python bundles")
        req_file = self.config[self.CONFIG_SECTION][self.REQUIREMENT_KEY]
        self.execute_command("pip3 install --upgrade -r {}".format(req_file))
        self.execute_command("python3 -m spacy download en_core_web_sm")
        self.execute_command("python3 -m spacy download en")
        self.execute_command("python3 -m spacy download en_core_web_md")
        self.execute_command("python3 -c \"import nltk;nltk.download('punkt');"
                             "nltk.download('averaged_perceptron_tagger');"
                             "nltk.download('wordnet')\"")
        self.logger.info("Python bundles setup done")

        # Setup Pycharm
        self.logger.info("Setting up IDE Pycharm")
        self.execute_command("apt-get -y install snapd snapd-xdg-open")
        self.execute_command("snap install pycharm-community --classic")
        return True

    def uninstall(self, **kwargs):
        """
        Removes all extra bundles installed for development VM
        Returns:
            True, if setup is successful. False Otherwise
        Raises:
            BundleFailedException
        """
        self.logger.info("Uninstall base bundles")
        req_file = self.config[self.CONFIG_SECTION][self.REQUIREMENT_KEY]
        self.execute_command("pip3 uninstall -y -r {}".format(req_file))
        self.logger.info("All python bundles uninstalled")
        # Removing Dev IDE
        self.execute_command("snap remove pycharm-community")
        return True

    def start(self, **kwargs):
        return True

    def stop(self, **kwargs):
        return True
