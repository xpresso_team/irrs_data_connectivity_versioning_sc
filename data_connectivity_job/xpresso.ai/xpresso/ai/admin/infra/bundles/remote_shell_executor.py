""" Abstract CommandExecutor class which is used to run script on a server"""
from xpresso.ai.admin.controller.utils.sshutils import SSHUtils
from xpresso.ai.core.utils.generic_utils import check_if_valid_ip_address

from xpresso.ai.admin.infra.bundles.command_executor import CommandExecutor
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import CommandExecutionFailedException, XprExceptions
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ["RemoteShellExecutor"]
__author__ = "Naveen Sinha"


class RemoteShellExecutor(CommandExecutor):
    """ It is used to run shell commands on a remote linux environment"""

    DEFAULT_EXECUTOR = "/bin/bash"

    def __init__(self, ip_address):
        super().__init__()
        self.logger = XprLogger()
        if not check_if_valid_ip_address(ip_address):
            raise CommandExecutionFailedException("Invalid IP Address Provided")
        self.ip_address = ip_address

    def execute_with_output(self, command: str, **kwargs):
        """ It runs linux shell command on remote server and returns the
        output

        Args:
            **kwargs:
            command(str): command for execution

        Returns:
            tuple: (response code: int, stdout: str, stderr: str)
        """
        self.logger.debug(f"Running command {command}")
        try:
            connection = SSHUtils(self.ip_address)
            command_output = connection.exec(command=command)
            connection.close()
            self.logger.debug(f"Command {command}  completed")
            return int(command_output['status']), command_output['stdout'].encode('utf-8'), command_output['stderr'].encode('utf-8')
        except XprExceptions:
            self.logger.debug(f"Command {command}  completed")

    def execute(self, command: str, **kwargs):
        """ It runs linux shell command on remote server
        Args:
            **kwargs:
            command(str): command for execution
        Returns:
            int: response code
        """
        try:
            connection = SSHUtils(self.ip_address)
            status = connection.exec(command=command)
            connection.close()
            self.logger.debug(f"Command {command} completed")
            return int(status['status'])
        except XprExceptions:
            self.logger.debug(f"Command {command} completed")
        return -1

    def execute_same_shell(self, command: str, **kwargs):
        pass
