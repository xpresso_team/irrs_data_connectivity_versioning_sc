
__all__ = ['XprDbSetup']
__author__ = 'Sahil Malav'

import time
import configparser
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.admin.infra.bundles.local_shell_executor import \
    LocalShellExecutor
from xpresso.ai.admin.controller.utils.constants import replset_initiate_cmd
from xpresso.ai.core.utils.linux_utils import get_ip_address

from xpresso.ai.core.utils import linux_utils
from pymongo import MongoClient, ASCENDING
from pymongo.errors import PyMongoError

from passlib.hash import sha512_crypt


class XprDbSetup:
    """
        Class that provides tools to setup mongodb on a node
    """

    def __init__(self, executor=None):
        if not executor:
            self.executor = LocalShellExecutor()
        self.logger = XprLogger()
        self.service_path = '/lib/systemd/system/mongod.service'

    def install_mongo(self, address=""):
        """
        installs mongodb on the system
        """
        self.logger.info('entering install_mongo method')
        if not linux_utils.check_root():
            self.logger.fatal("Please run this as root")
        import_key = 'sudo apt-key adv --keyserver ' \
                     'hkp://keyserver.ubuntu.com:80 --recv ' \
                     '9DA31620334BD75D9DCB49F368818C72E52529D4'
        self.executor.execute(import_key)
        create_list = 'echo "deb [ arch=amd64 ] https://repo.mongodb.org/' \
                      'apt/ubuntu bionic/mongodb-org/4.0 multiverse" | ' \
                      'sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list'
        self.executor.execute(create_list)
        reload_packages = 'sudo apt-get update'
        self.executor.execute(reload_packages)
        self.logger.debug('installing mongo')
        install_mongo = 'sudo apt-get install -y mongodb-org'
        self.executor.execute(install_mongo)
        hold = """echo "mongodb-org hold" | sudo dpkg --set-selections
                  echo "mongodb-org-server hold" | sudo dpkg --set-selections
                  echo "mongodb-org-shell hold" | sudo dpkg --set-selections
                  echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
                  echo "mongodb-org-tools hold" | sudo dpkg --set-selections"""
        self.executor.execute(hold)
        if address == "localhost" or address == "127.0.0.1":
            address = get_ip_address()

        self.executor.execute(command=f'''echo  "[Unit]
                                            Description=MongoDB Database Server
                                            After=network.target
                                            Documentation=https://docs.mongodb.org/manual

                                            [Service]
                                            User=mongodb
                                            Group=mongodb
                                            EnvironmentFile=-/etc/default/mongod
                                            ExecStart=/usr/bin/mongod  --replSet rs0 --bind_ip localhost,{address} --config /etc/mongod.conf
                                            PIDFile=/var/run/mongodb/mongod.pid
                                            # file size
                                            LimitFSIZE=infinity
                                            # cpu time
                                            LimitCPU=infinity
                                            # virtual memory size
                                            LimitAS=infinity
                                            # open files
                                            LimitNOFILE=64000
                                            # processes/threads
                                            LimitNPROC=64000
                                            # locked memory
                                            LimitMEMLOCK=infinity
                                            # total threads (user+kernel)
                                            TasksMax=infinity
                                            TasksAccounting=false
                                            Restart=always

                                            # Recommended limits for for mongod as specified in
                                            # http://docs.mongodb.org/manual/reference/ulimit/#recommended-settings

                                            [Install]
                                            WantedBy=multi-user.target

                                            "> /lib/systemd/system/mongod.service''')
        self.executor.execute(command='systemctl daemon-reload')
        self.executor.execute(command='systemctl enable mongod')
        self.executor.execute(command='systemctl restart mongod')

        self.logger.info('exiting install_mongo')

    def initial_setup(self, db):
        """
        sets up the initial users and collections in the db
        :param db: database against which the setup is to be done
        :return: nothing
        """
        self.logger.info('entering initial_setup method')
        # initiate users collection
        users = db.users
        self.insert_default_users(users)
        db.users.create_index([('uid', ASCENDING)], unique=True)
        self.logger.debug('created index for users collection')

        # initiate nodes collection
        nodes = db.nodes
        self.logger.debug('setting up initial node')
        initial_node = {
            "name": "initial_node",
            "address": ""
        }
        nodes.insert_one(initial_node)
        nodes.create_index([('address', ASCENDING)], unique=True)
        self.logger.debug('created index for nodes collection')
        nodes.delete_one({"name": "initial_node"})

        # initiate clusters collection
        clusters = db.clusters
        self.logger.debug('setting up initial cluster')
        initial_cluster = {
            "name": "initial_cluster",
            "activationStatus": True,
            "master_nodes": [],
            "worker_nodes": []
        }
        clusters.insert_one(initial_cluster)
        clusters.create_index([('name', ASCENDING)], unique=True)
        self.logger.debug('created index for clusters collection')
        clusters.delete_one({"name": "initial_cluster"})

        # initiate projects collection
        projects = db.projects
        self.logger.debug('setting up initial project')
        initial_project = {
            "name": "initial_project",
            "projectDescription": "Initiates the collection",
            "owner": {},
            "developers": [],
            "components": []
        }
        projects.insert_one(initial_project)
        projects.create_index([('name', ASCENDING)], unique=True)
        self.logger.debug('created index for projects collection')
        projects.delete_one({"name": "initial_project"})

        # create xprdb_admin user in mongo
        self.logger.debug('creating xprdb user in mongo')
        db.command("createUser", "xprdb_admin", pwd="xprdb@Abz00ba",
                   roles=[{"role": "root", "db": "admin"}])
        self.logger.info('exiting initial_setup method')

    def insert_default_users(self, users):
        self.logger.debug('setting up default users')
        admin_user = {
            "uid": "xprdb_admin",
            "firstName": "Xpresso",
            "lastName": "Admin",
            "pwd": sha512_crypt.hash('xprdb@Abz00ba'),
            "email": "xprdb_admin@abzooba.com",
            "primaryRole": "Admin",
            "activationStatus": True,
            "loginStatus": False
        }
        users.insert_one(admin_user)
        superuser = {
            "uid": "superuser1",
            "firstName": "superuser1",
            "lastName": "superuser1",
            "pwd": sha512_crypt.hash('superuser1'),
            "email": "superuser1@abzooba.com",
            "primaryRole": "Su",
            "activationStatus": True,
            "loginStatus": False
        }
        users.insert_one(superuser)
        admin1_user = {
            "uid": "admin1",
            "firstName": "admin1",
            "lastName": "admin1",
            "pwd": sha512_crypt.hash('admin1'),
            "email": "admin1@abzooba.com",
            "primaryRole": "Admin",
            "activationStatus": True,
            "loginStatus": False
        }
        users.insert_one(admin1_user)

    def enable_replication(self):
        """
        installs replica set for the database
        :return: nothing
        """
        self.logger.info('entering enable_replication method')
        path = '/srv/mongodb/rs0-0'
        linux_utils.create_directory(path, 0o777)
        self.logger.debug('created directory for replica set')
        ip = linux_utils.get_ip_address()
        start = 'systemctl restart mongod'
        self.executor.execute(start)
        self.logger.debug('mongo daemon started')
        db = self.initiate_replica()
        self.logger.debug('Replica set initiated')
        time.sleep(5)
        self.initial_setup(db)
        # stop mongo to restart with auth
        stop_mongod = 'systemctl stop mongod'
        self.executor.execute(stop_mongod)
        self.logger.debug('stopping mongo daemon to restart with auth')
        time.sleep(10)
        restart_mongod = 'systemctl restart mongod'
        self.executor.execute(restart_mongod)
        self.logger.debug('db setup complete, exiting enable_replication')

    def initiate_replica(self):
        """ Initalizes the replica instance of the current installation

        Returns:
            MongoDB. returns instance of db from the Mongo Client
        """
        client = MongoClient('localhost', replicaset='rs0')
        db = client.xprdb
        try:
            client.admin.command(replset_initiate_cmd)
        except PyMongoError:
            self.logger.error("Replica set is not initiated")
        return db


if __name__ == "__main__":
    XprDbSetup().install_mongo()
    XprDbSetup().enable_replication()
