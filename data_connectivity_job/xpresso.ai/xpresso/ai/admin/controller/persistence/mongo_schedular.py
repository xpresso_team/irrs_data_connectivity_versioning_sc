import logging

from xpresso.ai.admin.controller.persistence.mongo_backup import MongoBackup as backup_class
from xpresso.ai.admin.controller.utils.job_schedular import XpressoSchedule as schedule_class

# A class with method that schedules periodic backup of mongodb databases and moves it to local file system
# two methods are run here:
# 1) a scheduling method that can schedule a backup
# 2) a backup method that takes backup to local path


class MongoSchedular:

    def backup_mongodb(self):
        try:
            name_of_database = 'admin'
            backup_object = backup_class()
            schedule_object = schedule_class()
            logging.info("Started periodic backup")
            mongo_schedule_value = schedule_object.process_periodically(backup_object.backup(name_of_database))
            return mongo_schedule_value
        except Exception as err:
            logging.info(err)


if __name__ == "__main__":
    schedularobject = MongoSchedular()
    schedularobject.backup_mongodb()