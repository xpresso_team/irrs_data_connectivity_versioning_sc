__all__ = 'KubeflowUtils'
__author__ = 'Sahil Malav'


import yaml
import kfp_server_api
from kfp_server_api.rest import ApiException as KFApiException
from kubernetes import client
from kubernetes.client.rest import ApiException
from xpresso.ai.admin.controller.project_management.kubernetes.\
    kubernetes_manager import KubernetesManager
from xpresso.ai.admin.controller.project_management.kubernetes.kube_deploy_helper \
    import KubernetesDeploy
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import *
from xpresso.ai.admin.controller.utils import constants, project_utils
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser


class KubeflowUtils:

    def __init__(self, persistence_manager):
        self.persistence_manager = persistence_manager
        self.kubernetes_manager = KubernetesManager(persistence_manager)
        self.kube_deploy = KubernetesDeploy(persistence_manager)
        self.logger = XprLogger()
        config_path = XprConfigParser.DEFAULT_CONFIG_PATH
        self.config = XprConfigParser(config_path)
        self.deployment_files_folder = self.config[constants.PROJECTS_SECTION][
            constants.DEPLOYMENT_FILES_FOLDER]

    def install_kubeflow(self, master_node, namespace):
        """
        installs kubeflow on a given node, in a given namespace
        Args:
            master_node: master node of the cluster
            namespace: namespace in which kubeflow is to be installed

        Returns: nothing

        """
        pass

    def fetch_ambassador_port(self, master_node, namespace):
        """
        Fetches the port on which ambassador is running
        Args:
            master_node: master node IP of the cluster
            namespace: namespace on which ambassador is deployed

        Returns: ambassador nodePort

        """
        self.logger.info('entering change_ambassador_port method')
        self.kubernetes_manager.set_api_config(master_node)
        k8s_beta = client.CoreV1Api()
        try:
            self.kubernetes_manager.set_api_config(master_node)
            s = k8s_beta.read_namespaced_service(name='ambassador',
                                                 namespace=namespace)
            ambassador_port = s.spec.ports[0].node_port
        except ApiException as e:
            self.logger.error(f'Ambassaddor port fetching failed. Details : '
                              f'{e.status, e.body}')
            raise AmbassadorPortFetchException('Failed to fetch pipeline port.')
        self.logger.info('exiting fetch_ambassador_port method')
        return ambassador_port

    def set_kubeflow_api_config(self, master_node, ambassador_port):
        """
        sets the Kubeflow API config
        Args:
            ambassador_port: ambassador's service nodePort
            master_node: address of the master node

        Returns: nothing

        """
        self.logger.info('entering set_kubeflow_api_config method')
        try:
            master_info = self.persistence_manager.find(
                'nodes', {"address": master_node})
            token = master_info[0]['token']
        except (IndexError, KeyError):
            self.logger.error("Token retrieval from master node failed.")
            raise IncorrectTokenException(
                "Token retrieval from master node failed.")
        config = kfp_server_api.configuration.Configuration()
        config.verify_ssl = False
        config.debug = True
        config.host = f'http://{master_node}:{ambassador_port}/pipeline'
        config.api_key = {"authorization": "Bearer " + token}
        self.logger.info('exiting set_kubeflow_api_config method')
        return config

    def upload_pipeline_to_kubeflow(self, master_node, namespace, pipeline_zip):
        """
        uploads given kubeflow pipeline on the given cluster
        Args:
            namespace: namespace on which kubeflow is installed
            master_node: master node IP of the cluster
            pipeline_zip: zip file containing the pipeline yaml

        Returns: ambassador nodePort

        """
        self.logger.info('entering upload_pipeline_to_kubeflow method')
        ambassador_port = self.fetch_ambassador_port(master_node, namespace)
        self.logger.debug('fetched ambassador port')
        config = self.set_kubeflow_api_config(master_node, ambassador_port)
        api_client = kfp_server_api.api_client.ApiClient(config)
        try:
            upload_client = kfp_server_api.api.PipelineUploadServiceApi(
                api_client)
            upload_client.upload_pipeline(pipeline_zip)
        except KFApiException as e:
            if e.status == 500:
                self.logger.error('Trying to upload already existing pipeline')
                raise PipelineUploadFailedException(
                    'Pipeline already exists. Please choose a different name.')
            else:
                self.logger.error(f'Pipeline upload failed. Reason : {e.body}')
                raise PipelineUploadFailedException(e.body)
        return ambassador_port

    def create_yaml_for_pipeline_pv(self, project_name):
        """
        creates yaml file for pipeline pv
        Args:
            project_name: project name of which pipeline is a part

        Returns: path of yaml generated

        """
        self.logger.info('entering create_yaml_for_pipeline_pv')
        project_name = project_utils.modify_string_for_deployment(
            project_name)
        with open(f"config/kubernetes-persistent-volume.yaml", "r") as f:
            content = f.read()
        content = content.replace("K8_XPRESSO_PERSISTENT_STORAGE_SIZE",
                                  constants.pv_size)
        content = content.replace(
            "K8_XPRESSO_COMPONENT_NAME",
            f"{project_name}--{constants.pipeline_pv_prefix}")
        content = content.replace("K8_XPRESSO_PROJECT_NAME",
                                  f"{project_name}/pipelines")
        yaml_content = yaml.safe_load(content)
        filename = (f"{self.deployment_files_folder}"
                    f"/pipeline-pv-file--{project_name}.yaml")
        with open(filename, "w+") as f:
            yaml.safe_dump(yaml_content, f)
        self.logger.info('exiting create_yaml_for_pipeline_pv')
        return filename

    def create_pv_for_pipelines(self, project_name, master_node):
        """
        Checks if persistent volume exists for project level pipelines.
        If not, creates it.
        Args:
            project_name: Project of which pipeline is a part
            master_node: IP of master node to be deployed on
        Returns: nothing
        """
        if not self.kube_deploy.check_for_persistent_volume(
                f"{project_name}--{constants.pipeline_pv_prefix}"):
            self.logger.debug('PV not found for project  pipelines. '
                              'Creating PV.')
            pv = self.create_yaml_for_pipeline_pv(project_name)
            self.kubernetes_manager.set_api_config(master_node)
            try:
                with open(pv) as f:
                    dep = yaml.safe_load(f)
                    k8s_beta = client.CoreV1Api()
                    # collecting response from API
                    r = k8s_beta.create_persistent_volume(
                        body=dep)
                    self.logger.debug(
                        f"Persistence Volume created. Details : {str(r)}")
            except ApiException as e:
                self.logger.error(f'Creation of PV failed. Reason : {e}.')
                raise DeploymentCreationFailedException(
                    'Error! Failed to create persistent volume for pipelines.')

