import os
import urllib
from copy import deepcopy
from shutil import copy2, copytree, rmtree, move

from xpresso.ai.admin.controller.exceptions.xpr_exceptions import *
from xpresso.ai.admin.controller.external import bitbucketapi
from xpresso.ai.admin.controller.external.jenkins_manager import JenkinsManager
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

config_path = XprConfigParser.DEFAULT_CONFIG_PATH
config = XprConfigParser(config_path)
username = config['bitbucket']['username']
password = config['bitbucket']['password']
escape_password = urllib.parse.quote(password)
skeleton_path = f"http://{username}:{escape_password}@bitbucket.org/"\
                f"abzooba-screpo/skeleton-build.git"

bitbucket = bitbucketapi.bitbucketapi()

logger = XprLogger()


def replace_string_in_folder(key: str, replacement: str, directory_path: str):
    """
    Replace the key with the replace string in all the files present in
    the given directory path. It is similar to sed command in linux
    Args:
        key(str): string to be replaced
        replacement(str): replacement string
        directory_path(str): directory folder where we will run the replacement

    """
    logger.debug(f"Replacing {key} with {replacement}")
    for dir_name, dirs, files in os.walk(directory_path):
        for file_name in files:
            file_path = os.path.join(dir_name, file_name)
            try:
                with open(file_path, "r", encoding="utf-8") as f:
                    s = f.read()
                s = s.replace(key, replacement)
                with open(file_path, "w", encoding="utf-8") as f:
                    f.write(s)
            except UnicodeDecodeError:
                # Not making any changes to the unicode errored file
                logger.warning("Unicode Decode Error Found")
    logger.debug(f"Replacement is completed")


def local_code_setup(reponame, newrepourl):
    if os.path.exists('/tmp/skeleton-build'):
        rmtree('/tmp/skeleton-build')

    if os.path.exists(f'/tmp/{reponame}'):
        rmtree(f'/tmp/{reponame}')
    logger.info("Cloning skeleton-build repo")
    bitbucket.clone_bitbucket_repo(
        skeleton_path, '/tmp/skeleton-build'
    )
    logger.info(f"Cloning {reponame}")
    bitbucket.clone_bitbucket_repo(newrepourl, f'/tmp/{reponame}')


def create_components(components, reponame, newrepourl, projectname):
    logger.debug("Creating components locally.")
    for component in components:
        component_name = component['name']
        component_type = component['type']
        flavor = component['flavor']
        logger.info(f"\n Adding new component : {component}\n")
        src_flavor_dir = f"/tmp/skeleton-build/{component_type}/{flavor}"
        dst_flavor_dir = f"/tmp/{reponame}/{component_name}"
        copytree(src_flavor_dir, dst_flavor_dir)

        replace_string_in_folder(
            "{{XPRESSO_PROJECT_NAME}}", component_name,
            f"/tmp/{reponame}/{component_name}"
        )

        # create pipeline
        try:
            jenkins_manager = JenkinsManager(config)
            jenkins_manager.create_pipeline(
                f'{projectname}__{component_name}', newrepourl)
            print("pipeline created")
        except:
            print("error in creating pipeline")


def push_repo(project_json, repo_url):
    """
        pushrepo pushes the code to bitbucket
    """
    try:
        components = project_json['components']
        name = project_json['name']
        repo_name = 'xp_' + name + '_sc'
        logger.debug(f"Repo url is : {repo_url}")
        bb_split = repo_url.split("//")
        bb_split[1] = f"{username}:{escape_password}@"+bb_split[1]
        new_repo_url = "//".join(bb_split)
        local_code_setup(repo_name, new_repo_url)
        dst_makefile_path = f"/tmp/{repo_name}/Makefile"
        if not os.path.exists(dst_makefile_path):
            src_makefile_path = f"/tmp/skeleton-build/Makefile"
            copy2(src_makefile_path, dst_makefile_path)
            print("Makefile added")
        create_components(components, repo_name, new_repo_url, name)
        bitbucket.push_repo_to_bitbucket(f"/tmp/{repo_name}")
        rmtree('/tmp/skeleton-build')
        rmtree(f'/tmp/{repo_name}')
        return True
    except Exception as e:
        print("caught exception.: ", e)
        return False


def setup_project(project_json):
    creation_response = bitbucket.create_bitbucket_project(project_json)
    return_response = {
        "status": 200,
        "project_json": project_json
    }
    if creation_response["type"] == "error":
        return_response['status'] = error_codes.project_creation_failed
        return return_response

    repo_json = bitbucket.create_bitbucket_repo(project_json)
    if repo_json['type'] == 'error':
        return_response['status'] = error_codes.repo_creation_failed
        return return_response

    print(repo_json)
    repo_url = deepcopy(repo_json['links']['html']['href'])+'.git'
    push_repo_code = push_repo(project_json, repo_url)
    if not push_repo_code:
        return_response['status'] = error_codes.project_push_failed
        return return_response
    # rep_ourl = repojson['links']['clone'][0]['href']
    project_json['giturl'] = deepcopy(repo_json['links']['html']['href'])
    return_response['project_json'] = project_json
    return return_response


def modify_project_locally(project_info, changes_json):
    if 'components' in changes_json:
        repo_url = deepcopy(project_info['giturl']) + '.git'
        push_repo_code = push_repo(changes_json, repo_url)
        if not push_repo_code:
            return error_codes.project_push_failed
    else:
        pass
    return 200
