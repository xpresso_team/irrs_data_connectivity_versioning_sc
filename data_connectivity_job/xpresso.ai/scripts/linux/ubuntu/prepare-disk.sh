#!/bin/bash

## Prepare new disk to mount on a specific directory.

DEVICE=${1}
MOUNT_DIR=${2}

mkfs -t ext4 ${DEVICE}
mount ${DEVICE} ${MOUNT_DIR}

echo "${DEVICE}	${MOUNT_DIR}	ext4 	defaults	1	3" >> /etc/fstab

