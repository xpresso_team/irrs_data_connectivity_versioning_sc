#!/bin/bash
#
# Script is used to create a local docker registry using Harbor. We can store any docker registry here

DOCKER_ROOT_LOCATION=/var/lib/docker
REGISTRY_DOMAIN=
STORAGE_DIR=/mnt/data/registry
REGISTRY_NAME=xpr-docker-distribution
CERTS_DIR=/etc/ssl/certs
AUTH_DIR=/mnt/data/auth
FUNCTION_TO_RUN=${1}
USERNAME=${2}
PASSWORD=${3}

setup_harbor(){
  echo "===================== Installing Harbor ==================="
  harbor_temp_dir=/tmp/harbor
  harbor_tar=harbor.tgz
  harbor_config_file=$PWD/config/harbor/harbor.cfg
  harbor_compose_file=$PWD/config/harbor/docker-compose.yml
  mkdir -p ${harbor_temp_dir}
  wget https://storage.googleapis.com/harbor-releases/release-1.7.0/harbor-online-installer-v1.7.2.tgz -O ${harbor_temp_dir}/${harbor_tar}
  cd ${harbor_temp_dir}
  tar xvf ${harbor_tar}
  cd ${harbor_temp_dir}/harbor
  cp $harbor_config_file .
  cp $harbor_compose_file .
  bash install.sh
}

stop_harbor(){
  echo "===================== Shutting Down Harbor ==================="
  cd $PWD/config/harbor
  docker-compose up -d

}

setup_docker_distribution(){

  rm -fr ${AUTH_DIR}
  mkdir -p ${AUTH_DIR}
  
  docker run --rm \
         --entrypoint htpasswd \
         registry:2 -Bbn ${USERNAME} ${PASSWORD} > ${AUTH_DIR}/htpasswd

  docker run -d \
             -p 443:443 \
             --restart=always \
             --name ${REGISTRY_NAME} \
             -e "REGISTRY_AUTH=htpasswd" \
             -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
             -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
             -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
             -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/wildcard.xpresso.ai.cert \
             -e REGISTRY_HTTP_TLS_KEY=/certs/wildcard.xpresso.ai.key \
             -v ${CERTS_DIR}:/certs \
             -v ${AUTH_DIR}:/auth \
             -v ${STORAGE_DIR}:/var/lib/registry \
             registry:2

}

stop_docker_distribution(){
  docker stop ${REGISTRY_NAME} || true && docker rm -v ${REGISTRY_NAME} || true
}

${FUNCTION_TO_RUN}
