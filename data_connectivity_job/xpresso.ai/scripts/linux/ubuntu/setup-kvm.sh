#! /bin/bash
## Script is used to setup KVM on a local linux machine ##


##################################
# Install KVM
##################################
setup_kvm(){
  sudo apt-get install -y qemu-kvm \
                          libvirt-bin \
                          virtinst \
                          bridge-utils \
                          cpu-checker \
                          virt-manager
}

setup_kvm