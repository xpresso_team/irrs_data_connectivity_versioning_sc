#!/bin/bash
export MAXPARAMETERS=255
export IFS=$'\n'

function array_contains_find_index() {
  local n=$#
  local i=0
  local value=${!n}

  for (( i=1; i < n; i++ )) {
    if [[ "${!i}" == "${value}" ]]; then
      echo "REMOVING $i: ${!i} = ${value}"
      return $i
    fi
  }
  return $MAXPARAMETERS
}

# Take all dependencies mentioned in the file, denoted by $1 and store it in an array
mapfile -d "\n" -t package_list < $1

# Store all reverse dependencies in an indexed array and calculate the length
list=( $( apt-rdepends ${package_list[*]} | grep -v "^ " ) )
list_len=${#list[@]}
echo ${list[*]}
echo "Packages that will be downloaded..."

# Try to download the dependencies
# If results contains any items that means we have packages that are
# problematic that need to be removed from list. (note: `|&` is shortform for `2>&1 |`)
# Array elements aren't removed so the size is constant
results=( $( apt-get download ${list[*]} |& cut -d' ' -f 8 ) )

while [[ ${#results[@]} -gt 0 ]]; do
  for (( i=0; i < $list_len; i++ )); do
    array_contains_find_index ${results[@]} ${list[$i]}
    ret=$?
    if (( $ret != $MAXPARAMETERS )); then
      unset list[$i]
    fi
  done
    
  full_results=$( apt-get download ${list[*]} 2>&1  )
  results=( $( echo $full_results |& cut -d' ' -f 11 | sed -r "s/'(.*?):(.*$)/\1/g" ) )
done

apt-get download ${list[*]}
