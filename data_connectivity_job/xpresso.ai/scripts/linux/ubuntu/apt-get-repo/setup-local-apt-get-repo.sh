#!/bin/bash

DOCKER_IMAGE_VERSION=0.0.1
DOCKER_IMAGE_NAME=xpr-apt-repo:${DOCKER_IMAGE_VERSION}
DOCKER_CONTAINER_NAME=xpr-apt-repo
PRIVATE_KEY_LOCATION=${1}
PRIVATE_KEY_PASSPHRASE="${2}"
PRIVATE_KEY_ID="${3}"
echo $#
if [[ $# -lt 3 ]];then
  echo "-Usage"
  echo "Args:- <private_key_location> <private_key_passphrase> <private_key_id>"
  exit
 fi

if [[ ! -f ${PRIVATE_KEY_LOCATION} ]];then
  echo "Please provide private rsa keys"
  echo "Run following packages to generate the keys"
  echo "> gpg --full-generate-key
        > gpg --list-keys"
  exit 
fi

docker build -t ${DOCKER_IMAGE_NAME}  \
             -f scripts/docker/apt-get-repo/apt-mirror.dockerfile \
             --build-arg PRIVATEKEY_PATH=${PRIVATE_KEY_LOCATION}\
             --build-arg KEYID=${PRIVATE_KEY_ID} \
             --build-arg PASSPHRASE=${PRIVATE_KEY_PASSPHRASE} \
             .

docker run -d --rm \
           -p 8500:80 \
           --name ${DOCKER_CONTAINER_NAME} \
	         -v /etc/ssl/certs/wildcard.xpresso.ai.cert:/etc/ssl/certs/wildcard.xpresso.ai.cert \
	         -v /etc/ssl/certs/wildcard.xpresso.ai.csr:/etc/ssl/certs/wildcard.xpresso.ai.csr \
	         -v /etc/ssl/certs/wildcard.xpresso.ai.key:/etc/ssl/certs/wildcard.xpresso.ai.key \
           ${DOCKER_IMAGE_NAME}
