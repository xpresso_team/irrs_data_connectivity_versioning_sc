#! /bin/bash

gpg --full-generate-key

gpg --list-keys
echo "Select public key ID and use it for export public id"
gpg --output public.key --armor --export <key_id>

gpg --output private.key --armor --export-secret-key <key_id>
