#! /bin/bash
# 1. Crete the username and setup for VNC
# 2. Mount  the nfs into xpresso VM

if [[ $EUID -ne 0 ]]; then
	echo "Please run as root"
	exit 1
fi

if [[ "$#" -ne 3 ]]; then
	echo "Provide mandatory argument: <username> <name> <hostname>"
	exit 1
fi

username=${1}
name=${2}
hostname=${3}

echo "In case of confluct with the user. Delete the user"
deluser $username
rm -rf /home/$username
echo "Deletion completed"

echo "Setting hostname: ${hostname}"
hostnamectl set-hostname ${hostname}
sed -i "s/template-uswv-udesk-800/$hostname/g" /etc/hosts
echo "Hostname setup done"

echo "Changing the user name and xpresso to $username"
systemctl stop vncserver@1
passwd xpresso
usermod -l $username -d /home/$username -m xpresso
groupmod -n $username xpresso
chfn -f "${name}" $username
sed -i "s/xpresso/$username/g" /etc/systemd/system/vncserver\@.service
rm -rf /tmp/.X*
su - $username -c "cd ~;ls;rm -rf .vnc/uswv* .vnc/passwd .PyCharmCE2019.1/ .eclipse .cache ; vncpasswd;"
systemctl daemon-reload
systemctl start vncserver@1

echo "Username and password has been changed"

echo "Starting the nfs mounting process..."
persisted_home=/home/${username}/persisted_home
mkdir -p $persisted_home
mkdir -p /mnt/nfs/data/users/${username}
umount /mnt/nfs/data | true
mount nfs2.xpresso.ai:/mnt/exports/vmdata1/xpresso_platform/users/${username} $persisted_home
head -n -1 /etc/fstab > /etc/fstab.tmp
echo "nfs2.xpresso.ai:/mnt/exports/vmdata1/xpresso_platform/users/${username}    $persisted_home   nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0" >> /etc/fstab.tmp
cp /etc/fstab /etc/fstab.backup
mv /etc/fstab.tmp /etc/fstab
echo "NFS Mount Successfull"

rm -rf /home/${username}/admin_user.json /home/${username}/dev_user.json
