#! /bin/bash

# Check if the user is logged in as root
if [[ $EUID -ne 0 ]]
then
    echo "ERROR! Please login as root before executing this script. Use 'sudo su' to login as root."
    exit 1
else 
	printf "%s\n" "======================== KUBEFLOW & SELDON SETUP ========================"
	printf "%s\n" "Please make sure that you're running this script on the master node."
fi

if [[ $# -le 1 ]]; then
	echo "Please provide installation type - (k) for Kubeflow + Seldon, (s) for s2i, or (h) for Helm :"
	exit 1
fi

_CHOICE=${1}


install_ksonnet(){
	printf "%s\n" "======================== INSTALLING KSONNET ========================"
	export KSONNET_TAG=0.13.1
	wget -O ks.tar.gz https://github.com/ksonnet/ksonnet/releases/download/v${KSONNET_TAG}/ks_${KSONNET_TAG}_linux_amd64.tar.gz
	gunzip ks.tar; tar xvf  ks.tar; 
	cp ks_*/ks /usr/local/bin/
}

install_kubeflow(){
	printf "%s\n" "======================== INSTALLING KUBEFLOW ========================"
	export KUBEFLOW_SRC=/packages/kubeflow
	mkdir -p ${KUBEFLOW_SRC}
	cd ${KUBEFLOW_SRC}
	export KUBEFLOW_TAG=v0.4.1
	curl https://raw.githubusercontent.com/kubeflow/kubeflow/${KUBEFLOW_TAG}/scripts/download.sh | bash

	export KFAPP=kubeflow_config
	${KUBEFLOW_SRC}/scripts/kfctl.sh init ${KFAPP} --platform none
	cd ${KFAPP}
	${KUBEFLOW_SRC}/scripts/kfctl.sh generate platform
	${KUBEFLOW_SRC}/scripts/kfctl.sh apply platform
	${KUBEFLOW_SRC}/scripts/kfctl.sh generate k8s
	${KUBEFLOW_SRC}/scripts/kfctl.sh apply k8s
}

install_seldon(){
	printf "%s\n" "======================== INSTALLING SELDON ========================"
	cd ${KUBEFLOW_SRC}/${KFAPP}/ks_app
	ks pkg install kubeflow/seldon
	ks generate seldon seldon
	ks apply default -c seldon 
}

install_s2i(){
	printf "%s\n" "======================== INSTALLING S2I ========================"
	export S2I_TAG=1.1.13
	wget https://github.com/openshift/source-to-image/releases/download/v${S2I_TAG}/source-to-image-v${S2I_TAG}-b54d75d3-linux-amd64.tar.gz
	tar -xvf source-to-image-v${S2I_TAG}-b54d75d3-linux-amd64.tar.gz
	cp ./s2i /usr/local/bin
}

install_helm(){
	printf "%s\n" "======================== INSTALLING HELM ========================"
	curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh
	chmod 700 get_helm.sh
	./get_helm.sh
	helm init
}


if [[ "$_CHOICE" == "k" ]]
then
	install_ksonnet
    install_kubeflow
    install_seldon
elif [[ "$_CHOICE" == "s" ]]
then 
    install_s2i
elif [[ "$_CHOICE" == "h" ]]
then 
    install_helm         
else
    printf "%s\n" "ERROR! Please provide a valid choice."
fi
