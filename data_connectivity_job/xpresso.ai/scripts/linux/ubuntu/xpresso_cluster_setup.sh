#! /bin/bash

set -e

# This script downloads and sets up the Xpresso Environment and Xpresso
# CLI on an Ubuntu system, along with all the required dependencies.

if [[ $EUID -ne 0 ]]
then
    echo "ERROR! Please login as root before executing this script. Use 'sudo su' to login as root."
    exit 1
fi

#parameters and constants
HOST_IP=$1  # sftp host IP
TARGET_FILE=$2  # location of file to be downloaded
XPRESSO_LOCATION=/opt
DOWNLOAD_LOCATION=${XPRESSO_LOCATION}/xpresso_package.zip
XPRESSO_BASE_FOLDER=xpresso.ai
SFTP_PORT=2222

#throw error if no parameters are provided
#if [[ -z "$1" ]] || [[ -z "$2" ]]
#then
#	echo "No input arguments provided for Host IP and/or Target File. Please run the script as :"
#	echo -e "\t\t bash xpresso_setup.sh <host_ip> <target_file>"
#	exit 1
#fi
#
setup_base_ubuntu(){
	apt-get -y update && apt-get -y upgrade
	apt-get -y install build-essential \
		vim \
		curl \
		libcurl4-openssl-dev \
		libssl-dev \
		libffi-dev \
		git \
		wget \
		tmux \
		libfreetype6 \
		apt-transport-https \
		ca-certificates \
		software-properties-common \
		locales \
		pkg-config \
		nano \
		cmake \
		libxml2-dev \
		libxmlsec1-dev \
		checkinstall \
		libsasl2-dev \
		libldap2-dev \
		unzip
}

install_python(){
	add-apt-repository -y ppa:ubuntu-toolchain-r/ppa | true
	apt-get -y install python3.7
	update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
	update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
	update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2
	cp /usr/lib/python3/dist-packages/apt_pkg.cpython-36m-x86_64-linux-gnu.so /usr/lib/python3/dist-packages/apt_pkg.so | true
}

setup_base_ubuntu
install_python
exit
#install sshpass to provide password on command line
apt-get -y install sshpass

#download and unzip package
cd ${XPRESSO_LOCATION}
printf "%s\n" "Downloading Xpresso Package... "
sshpass -p 'abzooba@123' sftp -P ${SFTP_PORT} -o StrictHostKeyChecking=no xpresso@${HOST_IP}:${TARGET_FILE} ${DOWNLOAD_LOCATION}
printf "%s\n" "Unpacking Xpresso Package... "
unzip -o ${DOWNLOAD_LOCATION}

#install requirements
cd ${XPRESSO_BASE_FOLDER}
printf "%s\n" "Installing requirements... "
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install --upgrade pip
pip install --upgrade setuptools
pip install --upgrade -r requirements.txt

#install xprctl
pip install -e .
printf "%s\n" "Setup complete."

# Setup Xpresso Cluster
xprctl install_bundle -f samples/install_bundle.json
