:: This script installs the Xpresso CLI and dependencies on a Windows machine
:: Pre-requisites -
:: 1. Xpresso Package should have been unzipped inside C:\opt folder.
:: 2. Python3.7 should have been already installed and added to path.
:: 3. PYTHONPATH should have been set to C:\opt\xpresso.ai

@SET XPRESSO_LOCATION=C:\opt\xpresso.ai
@cd %XPRESSO_LOCATION%
@pip3 install -r requirements.txt
@pip3 install -e .
@echo Setup Complete.