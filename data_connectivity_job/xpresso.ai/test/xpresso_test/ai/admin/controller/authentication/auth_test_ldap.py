from xpresso.ai.admin.controller.client.controller_client \
    import ControllerClient
from xpresso.ai.admin.controller.utils import error_codes
import json

def update_json():
    with open('/opt/xpresso.ai/config/common'
                   '.json','r') as f:
        data = json.load(f)
    f.close()

    data["authentication_type"] = "ldap"

    with open('/opt/xpresso.ai/config/common'
                   '.json','w') as f:
        json.dump(data,f)
    f.close()

update_json()

client = ControllerClient()

"""    
    already_logged_in = 101
    no_user = 102
    wrong_pwd = 103
    empty_uid = 104
    empty_pwd = 105
    True : Success
    False : Failed
"""

def test1():    # normal login

    print("Starting test1")
    try:
        result = client.login("user1", "user1")
        assert result is True, "test1 failed."
        print("test1 successful")
    except:
        print("Test1 failed")


def test2():    # relogin
    print("Starting test2")
    try:
        result = client.login("user1", "user1")
        assert result is error_codes.already_logged_in, "test2 failed."
        print("test2 successful")
    except:
        print("Test2 failed")


def test3():  # normal logout
    print("Starting test3")
    try:
        result = client.logout()
        assert result is True, "test3 failed."
        print("test3 successful")
    except:
        print("Test3 failed")


def test4():  # logout without any login
    print("Starting test4")
    try:
        result = client.logout()
        assert result is None, "test4 failed."
        print("test4 successful")
    except:
        print("Test4 failed")


def test5():    # wrong username
    print("Starting test5")
    try:
        result = client.login("user1111", "user1")
        assert result is error_codes.no_user, "test5 failed."
        print("test5 successful")
    except:
        print("test5 failed")


def test6():    # wrong pwd
    print("Starting test6")
    try:
        result = client.login("user1", "user1111")
        assert result is error_codes.wrong_pwd, "test6 failed."
        print("test6 successful")
    except:
        print("test6 failed")


def test7():    # server down
    print("Starting test7")
    try:
        result = client.login("user1", "user1")
        assert result is False, "test7 failed."
        print("test7 successful")
    except:
        print("test7 failed")


def test8():    # no username entered
    print("Starting test8")
    try:
        result = client.login("", "user1")
        assert result is error_codes.empty_uid, "test8 failed."
        print("test8 successful")
    except:
        print("test8 failed")


def test9():    # no pwd entered
    print("Starting test9")
    try:
        result = client.login("user1", "")
        assert result is error_codes.empty_pwd, "test9 failed."
        print("test9 successful")
    except:
        print("test9 failed")


def test10():   # no user, no pass
    print("Starting test10")
    try:
        result = client.login("", "")
        assert result is error_codes.empty_uid, "test10 failed."
        print("test10 successful")
    except:
        print("test10 failed")
