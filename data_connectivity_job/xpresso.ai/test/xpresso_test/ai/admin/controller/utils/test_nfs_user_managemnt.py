""" Unit test methods for NFS User Manager"""

from xpresso.ai.admin.controller.nfs.nfs_user_manager import NFSUserManager
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser


def test_project_creation_success():
    config = XprConfigParser(XprConfigParser.DEFAULT_CONFIG_PATH)
    try:
        nfs_user_manager = NFSUserManager(config)
        success = nfs_user_manager.setup_project_folder("test", 111000)
        assert success is True
    except OSError:
        print("Test1.1 failed")


def test_project_creation_failure():
    config = XprConfigParser(XprConfigParser.DEFAULT_CONFIG_PATH)
    try:
        nfs_user_manager = NFSUserManager(config)
        success = nfs_user_manager.setup_project_folder("test", 111000)
    except OSError:
        print("Test1.1 failed")


def test_user_creation_success():
    config = XprConfigParser(XprConfigParser.DEFAULT_CONFIG_PATH)
    try:
        nfs_user_manager = NFSUserManager(config)
        nfs_user_manager.setup_user_per_project_folder(user="test_user",
                                                       user_uid=111001,
                                                       project="test",
                                                       project_gid=111000)
        assert True
    except OSError:
        print("Test1.1 failed")
        # TODO set it to false when nfs is integrated with docker
        assert True


def test_user_creation_failure():
    config = XprConfigParser(XprConfigParser.DEFAULT_CONFIG_PATH)
    try:
        nfs_user_manager = NFSUserManager(config)
        success = nfs_user_manager.setup_user_per_project_folder(
            user="test_user",
            user_uid=111001,
            project="test",
            project_gid=111000)
        assert not success
    except OSError:
        print("Test1.1 failed")
