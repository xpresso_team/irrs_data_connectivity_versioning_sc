""" Test functionality in the generic utility"""
import shutil
from xpresso.ai.core.utils.generic_utils import get_version


def test_success_get_version():
    version = get_version()
    assert version != "-1"


def test_failuere_get_version():
    shutil.move("VERSION", "TEST_VERSION")
    version = get_version()
    shutil.move("TEST_VERSION", "VERSION")
    assert version == "-1"
