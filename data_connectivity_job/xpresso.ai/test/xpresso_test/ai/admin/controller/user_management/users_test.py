import pytest
from pymongo.errors import DuplicateKeyError

from xpresso.ai.admin.controller.user_management.usermanager import UserManager
from xpresso.ai.admin.controller.client.controller_client \
    import ControllerClient

from xpresso.ai.admin.controller.user_management.user import User
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import *
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.admin.controller.persistence.mongopersistencemanager import \
    MongoPersistenceManager
from xpresso.ai.admin.controller.persistence.mongopersistencemanager import \
    UnsuccessfulOperationException

client = ControllerClient()

testuserjson1 = {
    "uid": "testuser1",
    "firstName": "John",
    "lastName": "Wick",
    "pwd": "testpass1",
    "email": "testuser@abzooba.com",
    "primaryRole": "Dev"
}

testuserjson2 = {
    "uid": "testuser2",
    "firstName": "Gopi",
    "lastName": "Krishna",
    "pwd": "testpass2",
    "email": "testuser2@gmail.com",
    "primaryRole": "Su"
}

testmodifyjson1 = {
    "email": "testuser@abzooba.com",
    "primaryRole": "Su"
}

MONGO_SECTION = 'mongodb'
URL = 'mongo_url'
DB = 'database'
UID = 'mongo_uid'
PWD = 'mongo_pwd'
W = 'w'
config_path = XprConfigParser.DEFAULT_CONFIG_PATH

config = XprConfigParser(config_path)
mongo_persistence_manager = MongoPersistenceManager(
    url=config[MONGO_SECTION][URL],
    db=config[MONGO_SECTION][DB],
    uid=config[MONGO_SECTION][UID],
    pwd=config[MONGO_SECTION][PWD],
    w=config[MONGO_SECTION][W])
user = UserManager(mongo_persistence_manager)


def test_register_1():
    # simple test of adding new user as a admin
    print("Starting test_register_1")
    try:
        mongo_persistence_manager.delete("users", {"uid": "testuser1"})
        adduser = UserManager(mongo_persistence_manager).register_user(
            testuserjson1)
        getuser = user.get_users(
            {'uid': 'testuser1'}
        )
        assert getuser[0]['uid'] == 'testuser1'
        assert getuser[0]['email'] == 'testuser@abzooba.com'
        print("test_register_1 successful")
    except MissingFieldException:
        print("test_register_1 failed: missing field")
        assert False
    except InvalidValueException:
        print("test_register_1 failed: invalid value")
        assert False
    except DuplicateUserException:
        print("test_register_1 failed: User exists already")
        assert False


def test_register_2():
    # Adding an existing user
    print("Starting test_register_2")
    try:
        UserManager(mongo_persistence_manager).register_user(testuserjson1)
        print("test_register_2 failed")
        assert False
    except DuplicateUserException:
        print("test_register_2 succeeded")
        assert True


def test_register_3():
    # Test login for newly registered user
    print("Starting test_register_3")
    try:
        loginuser = client.login('testuser1', 'testpass1')
        assert True
        print("test_register_3 successful")
    except:
        assert False
        print("test_register_3 failed")


def test_register_4():
    # get list of users without logging in
    print("Starting test_register_4")
    try:
        client.logout()
        users = user.get_users({})
        assert "Please login first" in users
        print("test_register_4 successful")
    except:
        print("test_register_4 failed")


def test_getuser_1():
    # Get list of all users as admin
    print("Starting test_getuser1")
    try:
        users = user.get_users({})
        assert len(users) >= 1
        print("test_getuser successful")
    except:
        print("test_getuser failed")


def test_getuser_2():
    # get list of users witout admin access
    print("Starting test_getuser_2")
    try:
        client.login('testuser1', 'testpass1')
        users = user.get_users({})
        assert users is 'Access denied to get the list of users'
        print("test_getuser successful")
    except:
        print("test_getuser failed")


def test_getuser_3():
    # get list of users without logging in
    print("Starting test_get_user_3")
    try:
        client.logout()
        users = user.get_users({})
        assert "Please login first" in users
        print("test_getuser_3 successful")
    except:
        print("test_getuser_3 failed")


def test_modify_user():
    print("Starting test_modify_user")
    try:
        modifyuser = user.modify_user({'uid': 'testuser1'}, testmodifyjson1)
        getuser = user.get_users({'uid': 'testuser1'})
        assert getuser[0]['uid'] is 'testuser1'
        assert getuser[0]['primaryRole'] is 'Su'
        print("test_modify_user successful")
    except:
        assert False
        print("test_modify_user failed")


def test_modify_user_2():
    # get list of users without logging in
    print("Starting test_modfy_user_2")
    try:
        client.logout()
        users = user.modify_user({'uid': 'testuser1'}, testmodifyjson1)
        assert "Please login first" in users
        print("test_modify_user_2 successful")
    except:
        assert False
        print("test_modify_user_2 failed")


def test_modify_user_3():
    print("Starting test_modify_user_3")
    try:
        client.logout()
        client.login('user1', 'user1')
        users = user.modify_user({'uid': 'testuser1'}, testmodifyjson1)
        assert "Access denied" in users
        print("test_modify_user_3 successful")
    except:
        assert False
        print("test_modify_user_3 failed")


def test_delete_user():
    # Deleting an user as a admin
    print("Starting test_delete_user")
    try:
        deleteuser = user.deactivate_user('testuser1')
        getuser = user.get_users({'uid': 'testuser1'})
        assert len(getuser) == 0
        print("test_delete_user successful")
    except:
        print("test_delete_user failed")


def test_delete_user_2():
    # Deleting an user without logging in
    print("Starting test_delete_user_2")
    try:
        client.logout()
        users = user.deactivate_user('testuser1')
        assert "Please login first" in users
        print("test_getuser_3 successful")
    except:
        print("test_getuser_3 failed")


def test_delete_user_3():
    # Deleting an user who is not present
    print("Starting test_delete_user_3")
    try:
        client.logout()
        users = user.deactivate_user('testuser1')
        assert "user not found" in users
        print("test_getuser_3 successful")
    except:
        print("test_getuser_3 failed")


def test_delete_user_4():
    print("Starting test_delete_user_4")
    try:
        client.logout()
        client.login('user1', 'user1')
        users = user.deactivate_user('testuser2')
        assert "Access denied" in users
        print("test_delete_user_4 successful")
    except:
        print("test_delete_user_4 failed")


def test_user_1():
    print("testing user constructor with no fields")
    user1 = User()
    assert user1 is not None, "Error creating user"


def test_user_2():
    print("testing user constructor with all fields")
    user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                  "lastName": "user1", "email": "user1",
                  "primaryRole": "Dev"})
    user1.validate_mandatory_fields()
    user1.validate_field_values()
    assert user1 is not None, "Error creating user"
    assert user1.get("uid") is "user1", "UID of user incorrect"


def test_user_3():
    print("testing user constructor with primary role missing")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "lastName": "user1", "email": "user1"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except MissingFieldException:
        assert True


def test_user_4():
    print("testing user constructor with email missing")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "lastName": "user1", "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except MissingFieldException:
        assert True


def test_user_5():
    print("testing user constructor with last name missing")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "email": "user1", "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except MissingFieldException:
        assert True


def test_user_6():
    print("testing user constructor with first name missing")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "lastName": "user1",
                      "email": "user1", "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except MissingFieldException:
        assert True


def test_user_7():
    print("testing user constructor with pwd missing")
    try:
        user1 = User({"uid": "user1", "firstName": "user1", "lastName": "user1",
                      "email": "user1", "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except MissingFieldException:
        assert True


def test_user_8():
    print("testing user constructor with uid missing")
    try:
        user1 = User({"pwd": "user1", "firstName": "user1", "lastName": "user1",
                      "email": "user1", "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except MissingFieldException:
        assert True


def test_user_9():
    print("testing user constructor with primary role blank")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "lastName": "user1", "email": "user1",
                      "primaryRole": ""})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except BlankFieldException:
        assert True


def test_user_10():
    print("testing user constructor with email blank")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "lastName": "user1", "email": "",
                      "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except BlankFieldException:
        assert True


def test_user_11():
    print("testing user constructor with last name blank")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "lastName": "", "email": "user1",
                      "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except BlankFieldException:
        assert True


def test_user_12():
    print("testing user constructor with first name blank")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "",
                      "lastName": "user1", "email": "user1",
                      "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except BlankFieldException:
        assert True


def test_user_13():
    print("testing user constructor with pwd blank")
    try:
        user1 = User({"uid": "user1", "pwd": "", "firstName": "user1",
                      "lastName": "user1", "email": "user1",
                      "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except BlankFieldException:
        assert True


def test_user_14():
    print("testing user constructor with uid blank")
    try:
        user1 = User({"uid": "", "pwd": "user1", "firstName": "user1",
                      "lastName": "user1", "email": "user1",
                      "primaryRole": "Dev"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except BlankFieldException:
        assert True


def test_user_15():
    print("testing user constructor with invalid role")
    try:
        user1 = User({"uid": "user1", "pwd": "user1", "firstName": "user1",
                      "lastName": "user1", "email": "user1",
                      "primaryRole": "junk"})
        user1.validate_mandatory_fields()
        user1.validate_field_values()
        assert False
    except InvalidValueException:
        assert True


def main(*argv):
    test_register_2()


if __name__ == "__main__":
    main()
