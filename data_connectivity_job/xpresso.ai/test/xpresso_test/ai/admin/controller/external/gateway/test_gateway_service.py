""" Test cases of kong api server"""

import time
import requests

from xpresso.ai.admin.controller.exceptions.xpr_exceptions import APIGatewayExceptions
from xpresso.ai.admin.controller.external.gateway.gateway_manager import GatewayManager
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

config = XprConfigParser()


def test_gateway_external_service():
    gateway = GatewayManager()
    dummy_service_name = "dummy"

    try:
        _ = gateway.delete_external_service(component_name=dummy_service_name)
        time.sleep(50)
    except APIGatewayExceptions:
        print("service does not exist")

    proxy_url = gateway.setup_external_service(
        component_name=dummy_service_name,
        internal_service_url=["http://dummy.restapiexample.com/api/v1/employees"])
    time.sleep(50)
    dummy_resp = requests.get(proxy_url, headers={"User-Agent": "curl/7.58.0",
                                                  "Accept": "*/*"})
    if dummy_resp.status_code >= 400:
        assert False
    assert True


def test_gateway_duplicate_external_service():
    gateway = GatewayManager()
    dummy_service_name = "dummy"
    try:
        _ = gateway.delete_external_service(component_name=dummy_service_name)
        time.sleep(5)
    except APIGatewayExceptions:
        print("service does not exist")

    try:
        _ = gateway.setup_external_service(
            component_name=dummy_service_name,
            internal_service_url=["http://dummy.restapiexample.com/api/v1/employees"])
        assert True
    except APIGatewayExceptions:
        assert False


def test_kong_delete_service_pass():
    gateway = GatewayManager()
    dummy_service_name = "dummy"
    try:
        _ = gateway.delete_external_service(component_name=dummy_service_name)
        assert True
    except APIGatewayExceptions:
        assert False
    assert True

