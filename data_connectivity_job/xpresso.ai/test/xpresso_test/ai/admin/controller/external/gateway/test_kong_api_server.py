""" Test cases of kong api server"""

import os
import time
import requests

from xpresso.ai.admin.controller.exceptions.xpr_exceptions import APIGatewayExceptions
from xpresso.ai.admin.controller.external.gateway.kong_api_gateway import KongAPIGateway
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

config = XprConfigParser()
admin_url = config["gateway"]["admin_url"]
proxy_url = config["gateway"]["proxy_url"]


def test_kong_register_service():
    kong_api = KongAPIGateway(admin_url=admin_url,
                              proxy_url=proxy_url)
    dummy_service_name = "dummy"
    kong_api.register_new_service(upstream_url=["http://dummy.restapiexample.com/api/v1/employees"],
                                  service_name=dummy_service_name,
                                  route=f"/{dummy_service_name}")

    time.sleep(30)
    dummy_resp = requests.get(os.path.join(proxy_url, dummy_service_name), headers={"User-Agent": "curl/7.58.0","Accept": "*/*"})
    print(os.path.join(proxy_url, dummy_service_name))
    if dummy_resp.status_code >= 400:
        assert False
    assert True


def test_kong_duplicate_register_service():
    # Duplicate should not through error
    kong_api = KongAPIGateway(admin_url=admin_url,
                              proxy_url=proxy_url)
    dummy_service_name = "dummy"
    try:
        kong_api.register_new_service(upstream_url=["http://dummy.restapiexample.com/api/v1/employees"],
                                      service_name=dummy_service_name,
                                      route=f"/{dummy_service_name}")
        assert True
    except APIGatewayExceptions:
        assert False


def test_kong_get_service():
    kong_api = KongAPIGateway(admin_url=admin_url,
                              proxy_url=proxy_url)
    dummy_service_name = "dummy"
    resp_json = kong_api.get_service(service_name=dummy_service_name)
    assert resp_json["service"]
    assert resp_json["route"]


def test_kong_get_service_fail():
    kong_api = KongAPIGateway(admin_url=admin_url,
                              proxy_url=proxy_url)
    dummy_service_name = "dummy1"
    try:
        resp_json = kong_api.get_service(service_name=dummy_service_name)
        assert not resp_json["service"]
        assert not resp_json["route"]
    except APIGatewayExceptions:
        assert True


def test_kong_delete_service():
    kong_api = KongAPIGateway(admin_url=admin_url,
                              proxy_url=proxy_url)
    dummy_service_name = "dummy"
    kong_api.delete_service(service_name=dummy_service_name)
