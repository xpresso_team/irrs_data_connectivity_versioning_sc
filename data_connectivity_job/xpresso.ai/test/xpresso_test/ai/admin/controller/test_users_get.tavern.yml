---
# Tests for get_user
test_name: 4.1 Get list of users

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_1

---

test_name: 4.2 Get list of users after adding a user

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: register user "user2"

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: POST
      json:
        uid: "user2"
        pwd: "user2"
        firstName: "user2"
        lastName: "user2"
        email: "user2@abzooba.com"
        LoginStatus: false
        primaryRole: "Dev"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"

  - name: get list of users

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_2

---

test_name: 4.3 Attempt to get users by non-admin user

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as user1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"user1", "pwd":"user1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 113

---
test_name: 4.4 Attempt to get list of users without logging in

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: get  list  of users

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      headers:
        token: "junk"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "failure"
        error_code: 111

---
test_name: 4.5 Get list of administrators

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of administrators

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      json:
        primaryRole: "Admin"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_5

---
test_name: 4.6 Get list of users filtered by e-mail

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users filtered by e-mail

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      json:
        email: "user1@abzooba.com"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_6

---
test_name: 4.7 Get list of users using impossible filter condition

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users using impossible filter condition

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      json:
        uid: "junk"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_7

---
test_name: 4.8 Get list of users using invalid filter condition

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users using invalid filter condition

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      json:
        junk: "junk"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_7

---
test_name: 4.9 Get list of users using blank filter condition

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users using blank filter condition

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      json: {}
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_1

---
test_name: 4.10 Get list of users using multiple filter conditions

includes:
  - !include config.yml
marks:
  - usefixtures:
      - setup_users
# ...and each test has one or more stages (e.g. an HTTP request)
stages:
  - name: login as admin1

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/auth"
      method: POST
      json: {"uid":"admin1", "pwd":"admin1"}

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
      save:
        body:
          access_token: results.access_token

  - name: get list of users using blank filter condition

    # Define the request to be made...
    request:
      url: "{protocol:s}://{host:s}:{port:d}/users"
      method: GET
      json:
        firstName: "user1"
        lastName: "user1"
      headers:
        token: "{access_token}"

    # ...and the expected response code and body
    response:
      status_code: 200
      body:
        outcome: "success"
        $ext:
          function: test.xpresso_test.ai.admin.controller.validators:validate4_6

---
