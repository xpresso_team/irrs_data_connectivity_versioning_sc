import os
from copy import deepcopy

from xpresso.ai.core.data.pachyderm_repo_management.pachyderm_repo_manager \
    import PachydermRepoManager
from xpresso.ai.core.data.structured_dataset import StructuredDataset
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import *

repo_name = "unit_test_repo"
branch_name = "unit_test_branch"
root_path = os.path.dirname(os.path.abspath(__file__))
test_file_path = os.path.join(root_path, "test_file")
test_folder_path = os.path.join(root_path, "test_dir")
dataset_info1 = {
    "repo_name": repo_name,
    "branch_name": branch_name,
    "dataset_name": "test_dataset_1",
    "path": test_file_path,
    "description": "unit test file"
}

dataset_info2 = {
    "repo_name": repo_name,
    "branch_name": branch_name,
    "dataset_name": "test_dataset_2",
    "path": test_folder_path,
    "description": "unit test folder"
}

repo_manager = PachydermRepoManager()
logger = XprLogger()
dataset_object = StructuredDataset()
# TODO: ADD Delete all method at the end of the unit testing


def test_delete_repo_success():
    try:
        repo_manager.delete_repo(repo_name)
        repos = repo_manager.get_repos()
        status = True
        for repo_item in repos:
            if repo_item["name"] == repo_name:
                status = False
                break
        assert status
        logger.info("test_delete_repo_success passed")
    except XprExceptions as err:
        logger.error(err.message)
        assert False


def test_create_repo_success():
    status = False
    try:
        repo_manager.create_repo({
            "repo_name": repo_name,
            "description": "unit testing repo manager"
        })
        repos = repo_manager.get_repos()
        for repo_item in repos:
            if repo_item["name"] == repo_name:
                status = True
                break
        assert status is True
        logger.info("test_create_repo_success passed")
    except XprExceptions as err:
        # In case of repo_name clash or some other issues
        logger.error("test_create_repo_success failed")
        assert status


def test_create_repo_failure_no_name():
    # No repo
    try:
        repo_manager.create_repo({})
        logger.error("test_create_repo_failure_no_name failed")
        assert False
    except RepoNotProvidedException:
        assert True
        logger.info("test_create_repo_failure_no_name passed")


def test_create_repo_failure_invalid_name():
    # Invalid name check
    try:
        repo_manager.create_repo({"repo_name": "(Bjs89S**(&"})
        logger.error("test_create_repo_failure_invalid_name failed")
        assert False
    except PachydermFieldsNameException:
        assert True
        logger.info("test_create_repo_failure_invalid_name passed")


def test_create_branch_success():
    # success case
    try:
        repo_manager.create_branch({"repo_name": repo_name, "branch_name": branch_name})
        branches = repo_manager.get_branches(repo_name)
        assert branch_name in branches
        logger.info("test_create_branch_success passed")
    except XprExceptions as err:
        logger.error(err.message)
        logger.error("test_create_branch_success failed")
        assert False


def test_create_branch_failure_no_repo_name():
    # no repo
    try:
        repo_manager.create_branch({"branch_name": branch_name})
        logger.error("test_create_branch_failure_no_repo_name failed")
        assert False
    except BranchInfoException:
        assert True
        logger.info("test_create_branch_failure_no_repo_name passed")


def test_create_branch_failure_no_branch_name():
    # no branch
    try:
        repo_manager.create_branch({"repo_name": repo_name})
        logger.error("test_create_branch_failure_no_branch_name failed")
        assert False
    except BranchInfoException:
        assert True
        logger.info("test_create_branch_failure_no_branch_name passed")


def test_create_branch_failure_invalid_repo():
    # Invalid repo
    try:
        repo_manager.create_branch({
            "repo_name": "unit_test_repo_not_created",
            "branch_name": "test_branch"
        })
        logger.error("test_create_branch_failure_invalid_repo failed")
        assert False
    except BranchInfoException as err:
        assert True
        logger.info("test_create_branch_failure_invalid_repo passed")


def test_create_branch_failure_invalid_branch_name():
    # Invalid naming for branch
    try:
        repo_manager.create_branch({"repo_name": repo_name, "branch_name": "((*H90ad"})
        logger.error("test_create_branch_failure_invalid_branch_name failed")
        assert False
    except PachydermFieldsNameException:
        assert True
        logger.info("test_create_branch_failure_invalid_branch_name passed")


def test_delete_branch_success():
    try:
        repo_manager.create_branch({
            "repo_name": repo_name,
            "branch_name": "temp_branch"
        })
        repo_manager.delete_branch(repo_name, "temp_branch")
        branches = repo_manager.get_branches(repo_name)
        assert "temp_branch" not in branches
        logger.info("test_delete_branch_success passed")
    except XprExceptions as err:
        logger.error("test_delete_branch_success failed")
        assert False


def test_push_dataset_success_file_only():
    # success case for files
    try:
        commit_id = repo_manager.push_files(dataset_info1)
        pachyderm_path = "dataset/" + dataset_info1["dataset_name"]
        file_list = repo_manager.list_dataset(repo_name, branch_name,
                                              pachyderm_path, commit_id)
        assert file_list["dataset"] and len(file_list["dataset"])
        logger.info("test_push_dataset_success_file_only passed")
    except XprExceptions as err:
        logger.error(err.message)
        logger.error("test_push_dataset_success_file_only failed")
        assert False


def test_push_dataset_success_folder():
    # Success case for folder
    try:
        commit_id = repo_manager.push_files(dataset_info2)
        pachyderm_path = "dataset/" + dataset_info2["dataset_name"]
        file_list = repo_manager.list_dataset(repo_name, branch_name,
                                              pachyderm_path, commit_id)
        assert file_list["dataset"] and len(file_list["dataset"])
        logger.info("test_push_dataset_success_folder passed")
    except XprExceptions as err:
        logger.error(err)
        logger.error("test_push_dataset_success_folder failed")
        assert False


def test_push_dataset_failure_incomplete_info():
    # Failure case for incomplete info
    incomplete_info = {
        "repo_name": repo_name,
        "branch_name": branch_name
    }
    try:
        repo_manager.push_files(incomplete_info)
        logger.error("test_push_dataset_failure_incomplete_info failed")
        assert False
    except DatasetInfoException as err:
        assert True
        logger.info("test_push_dataset_failure_incomplete_info passed")


def test_push_dataset_failure_no_branch():
    # Failure case for incomplete info
    incomplete_info = {
        "repo_name": repo_name,
        "dataset_name": "new_unit_test_dataset",
        "path": "/"
    }
    try:
        repo_manager.push_files(incomplete_info)
        logger.error("test_push_dataset_failure_no_branch failed")
        assert False
    except DatasetInfoException as err:
        assert True
        logger.info("test_push_dataset_failure_no_branch passed")


def test_push_dataset_failure_invalid_repo():
    # Failure case for invalid info(repo or branch)
    temp_dataset = deepcopy(dataset_info1)
    temp_dataset["repo_name"] = "(&(&(*J90da"
    try:
        repo_manager.push_files(temp_dataset)
        logger.error("test_push_dataset_failure_invalid_repo failed")
        assert False
    except PachydermFieldsNameException as err:
        assert True
        logger.info("test_push_dataset_failure_invalid_repo passed")


def test_push_dataset_failure_invalid_path():
    # Failure case for invalid path
    temp_dataset = deepcopy(dataset_info1)
    temp_dataset["path"] = "./random_path"
    try:
        repo_manager.push_files(temp_dataset)
        logger.error("test_push_dataset_failure_invalid_path failed")
        assert False
    except DatasetPathException as err:
        assert True
        logger.info("test_push_dataset_failure_invalid_path passed")


def test_push_dataset_failure_wrong_repo():
    new_info = deepcopy(dataset_info1)
    new_info["repo_name"] = "unknown_repo_name"
    try:
        repo_manager.push_files(new_info)
        logger.error("test_push_dataset_failure_wrong_repo failed")
        assert False
    except PachydermOperationException as err:
        assert True
        logger.info("test_push_dataset_failure_wrong_repo passed")


def test_push_dataset_success_dataset_object():
    try:
        path = "/opt/xpresso.ai/config/test/data/test.csv"
        dataset_object.import_dataset(path)
        repo_manager.push_dataset(repo_name, branch_name, dataset_object,
                                  "push dataset object to pachyderm testing")
        assert True
        logger.info("test_push_dataset_success_dataset_object passed")
    except ControllerClientResponseException as err:
        assert True
        logger.info("test_push_dataset_success_dataset_object passed")
    except XprExceptions as err:
        logger.error("test_push_dataset_success_dataset object failed")
        assert False


def test_push_dataset_failure_invalid_object():
    try:
        repo_manager.push_dataset(repo_name, branch_name, {}, "failure case")
        logger.error("test_push_dataset_failure_invalid_object failed")
        assert False
    except DatasetInfoException as err:
        assert err.message == "Provided dataset is invalid"
        logger.info("test_push_dataset_failure_invalid_object passed")


# def test_push_dataset_failure_no_data():
#     try:
#         new_dataset = StructuredDataset()
#         repo_manager.push_dataset(repo_name, branch_name, new_dataset,
#                                   "failure case testing for no data")
#         logger.info("test_push_dataset_failure_no_data failed")
#         assert False
#     except XprExceptions as err:
#         logger.info(err.message)
#         assert True


def test_pull_dataset_success_file_only():
    # pull the dataset added in test_push_dataset_1
    temp_dataset = deepcopy(dataset_info1)
    temp_dataset["path"] = "dataset/" + temp_dataset["dataset_name"]
    try:
        local_dataset_path = repo_manager.manage_xprctl_dataset("pull", temp_dataset)
        assert True
        logger.info("test_pull_dataset_success_file_only passed")
    except XprExceptions as err:
        logger.error("test_pull_dataset_success_file_only failed")
        assert False


def test_pull_dataset_success_folder():
    # pull the dataset added in test_push_dataset_1
    temp_dataset = deepcopy(dataset_info2)
    temp_dataset["path"] = "dataset/" + temp_dataset["dataset_name"]
    try:
        local_dataset_path = repo_manager.manage_xprctl_dataset("pull", temp_dataset)
        assert True
        logger.info("test_pull_dataset_success_folder passed")
    except XprExceptions as err:
        logger.error("test_pull_dataset_success_folder failed")
        assert False


def test_pull_dataset_success_latest_push():
    # Success case for only repo, branch
    # pulls the dataset from latest push in a branch
    try:
        local_dataset_path = repo_manager.pull_dataset(repo_name, branch_name, "/")
        assert True
        logger.info("test_pull_dataset_success_latest_push passed")
    except XprExceptions as err:
        logger.error("test_pull_dataset_success_latest_push failed")
        assert False


def test_pull_dataset_success_with_commit_id():
    # Success case for only repo, commit
    commits = repo_manager.list_commit(repo_name, branch_name)
    random_commit_id = commits[0]["id"]
    try:
        local_dataset_path = repo_manager.pull_dataset(repo_name, None,
                                                       "/", random_commit_id)
        assert True
        logger.info("test_pull_dataset_success_with_commit_id passed")
    except XprExceptions as err:
        logger.error("test_pull_dataset_success_with_commit_id failed")
        assert False


def test_pull_dataset_success_complete_info():
    # success case for repo, commit, branch
    commits = repo_manager.list_commit(repo_name, branch_name)
    random_commit_id = commits[0]["id"]
    try:
        local_dataset_path = repo_manager.pull_dataset(repo_name, branch_name,
                                                       "/", random_commit_id)
        assert True
        logger.info("test_pull_dataset_success_complete_info passed")
    except XprExceptions as err:
        logger.error("test_pull_dataset_success_complete_info failed")
        assert False


def test_pull_dataset_failure_no_branch_commit():
    # Failure case for incomplete info on branch, commit
    try:
        repo_manager.pull_dataset(repo_name, None, "/")
        logger.error("test_pull_dataset_failure_no_branch_commit failed")
        assert False
    except DatasetInfoException as err:
        assert True
        logger.info("test_pull_dataset_failure_no_branch_commit passed")


def test_pull_dataset_failure_invalid_branch():
    # Failure case for invalid branch or repo
    try:
        repo_manager.pull_dataset(repo_name, "unknown_branch", "/")
        logger.error("test_pull_dataset_failure_invalid_branch failed")
        assert False
    except PachydermOperationException as err:
        assert True
        logger.info("test_pull_dataset_failure_invalid_branch passed")


def test_pull_dataset_failure_invalid_commit_id():
    # Failure case for invalid commit id
    try:
        repo_manager.pull_dataset(repo_name, None, "/", commit_id="123pipowpi")
        logger.error("test_pull_dataset_failure_invalid_commit_id failed")
        assert False
    except PachydermOperationException as err:
        assert True
        logger.info("test_pull_dataset_failure_invalid_commit_id passed")


def test_list_dataset_success_file_only():
    # success case for file
    # check the file added in test_push_dataset_1 test case
    temp_dataset = deepcopy(dataset_info1)
    temp_dataset["path"] = "dataset/" + dataset_info1["dataset_name"]
    try:
        file_list = repo_manager.manage_xprctl_dataset("list", temp_dataset)
        assert len(file_list["dataset"])
        logger.info("test_pull_dataset_failure_invalid_commit_id successful")
    except XprExceptions as err:
        logger.error("test_pull_dataset_failure_invalid_commit_id failed")
        assert False


def test_list_dataset_success_folder():
    # check the file added in test_push_dataset_1 test case
    file_path = "dataset/" + dataset_info2["dataset_name"]
    try:
        file_list = repo_manager.list_dataset(
            repo_name,
            branch_name,
            file_path
        )
        assert len(file_list["dataset"])
        logger.info("test_list_dataset_success_folder passed")
    except XprExceptions as err:
        logger.error("test_list_dataset_success_folder failed")
        assert False


def test_list_dataset_success_latest_push():
    # Success case for only repo, branch
    try:
        file_list = repo_manager.list_dataset(repo_name, branch_name, "/")
        assert len(file_list["dataset"])
        logger.info("test_list_dataset_success_latest_push passed")
    except XprExceptions as err:
        logger.error("test_list_dataset_success_latest_push failed")
        assert False


def test_list_dataset_success_with_commit_id():
    # Success case for only repo, commit
    commits = repo_manager.list_commit(repo_name, branch_name)
    random_commit_id = commits[0]["id"]
    try:
        local_dataset_path = repo_manager.list_dataset(repo_name, None,
                                                       "/", random_commit_id)
        assert True
        logger.info("test_list_dataset_success_with_commit_id passed")
    except XprExceptions as err:
        logger.error("test_list_dataset_success_with_commit_id failed")
        assert False


def test_list_dataset_success_complete_info():
    # success case for repo, commit, branch
    commits = repo_manager.list_commit(repo_name, branch_name)
    random_commit_id = commits[0]["id"]
    try:
        local_dataset_path = repo_manager.list_dataset(repo_name, branch_name,
                                                       "/", random_commit_id)
        assert True
        logger.info("test_list_dataset_success_complete_info passed")
    except XprExceptions as err:
        logger.error("test_list_dataset_success_with_commit_id failed")
        assert False


def test_list_dataset_failure_incomplete_info():
    # Failure case for incomplete info on branch, commit
    try:
        repo_manager.list_dataset(repo_name, None, "/")
        logger.error("test_list_dataset_failure_incomplete_info failed")
        assert False
    except DatasetInfoException as err:
        assert True
        logger.info("test_list_dataset_failure_incomplete_info passed")


def test_list_dataset_failure_invalid_branch():
    # Failure case for invalid branch or repo
    try:
        repo_manager.pull_dataset(repo_name, "unknown_branch", "/")
        logger.error("test_list_dataset_failure_invalid_branch failed")
        assert False
    except PachydermOperationException as err:
        assert True
        logger.info("test_list_dataset_failure_invalid_branch passed")


def test_list_dataset_failure_invalid_commit_id():
    # Failure case for invalid commit id
    try:
        repo_manager.list_dataset(repo_name, None, "/", commit_id="123pi314po92i")
        logger.error("test_list_dataset_failure_invalid_commit_id failed")
        assert False
    except PachydermOperationException as err:
        assert True
        logger.info("test_list_dataset_failure_invalid_commit_id passed")


def test_list_commit_failure_invalid_repo():
    try:
        repo_manager.list_commit(")(#JLLOAIHD", branch_name)
        logger.error("test_list_commit_failure_invalid_repo failed")
        assert False
    except RepoNotProvidedException as err:
        assert True
        logger.info("test_list_commit_failure_invalid_repo passed")


def test_list_commit_failure_invalid_branch():
    try:
        repo_manager.list_commit(repo_name, "(6973asd**&")
        logger.error("test_list_commit_failure_invalid_branch failed")
        assert False
    except BranchInfoException as err:
        assert True
        logger.info("test_list_commit_failure_invalid_branch passed")