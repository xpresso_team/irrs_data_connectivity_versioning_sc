from xpresso.ai.admin.controller.exceptions.xpr_exceptions import UnsuccessfulConnectionException
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import UnsuccessfulOperationException
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import NoClustersPresentException
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import InvalidEnvironmentException
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import IncorrectDeploymentException
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import XprExceptions
from xpresso.ai.admin.controller.persistence.mongopersistencemanager import MongoPersistenceManager
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.admin.controller.env_management.env_manager import EnvManager


config_path = XprConfigParser.DEFAULT_CONFIG_PATH
config = XprConfigParser(config_path)

MONGO_SECTION = 'mongodb'
URL = 'mongo_url'
DB = 'database'
UID = 'mongo_uid'
PWD = 'mongo_pwd'
W = 'w'
MONGOURL = config[MONGO_SECTION][URL]
MONGODB = config[MONGO_SECTION][DB]
MONGOUID = config[MONGO_SECTION][UID]
MONGOPWD = config[MONGO_SECTION][PWD]
MONGOW = config[MONGO_SECTION][W]

# dummy project json
test_project_name = "test_project_for_env_unit_testing"
test_project_json = {"name": test_project_name, "env": ["DEV"]}

# dummy cluster json
test_cluster_name_prefix = "test_cluster_for_env_unit_testing"

# test setup methods


def setup_clusters(clusters=1):
    """
    sets up test environment with pre-existing clusters
    :param clusters: no. of clusters to set up
    :return:
    """
    mongo_persistence_manager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
    mongo_persistence_manager.connect()
    mongo_persistence_manager.delete("environments", {})
    mongo_persistence_manager.delete("clusters", {})
    i = 0
    # set up clusters
    while i < clusters:
        test_cluster_name = test_cluster_name_prefix + str(i+1)
        mongo_persistence_manager.delete("clusters", {"name": test_cluster_name})
        mongo_persistence_manager.insert("clusters", {"name": test_cluster_name, "activationStatus": True}, False)
        i += 1


def setup_clusters_and_environments(clusters=3):
    """
    sets up test environment with pre-existing clusters and environments
    :return:
    """
    mongo_persistence_manager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
    mongo_persistence_manager.connect()
    mongo_persistence_manager.delete("environments", {})
    i = 0
    while i < clusters:
        test_cluster_name = test_cluster_name_prefix + str(i+1)
        mongo_persistence_manager.delete("clusters", {"name": test_cluster_name})
        mongo_persistence_manager.insert("clusters", {"name": test_cluster_name, "activationStatus": True}, False)
        i += 1

    # allocate 2 environments to 1 cluster, 1 to 2
    mongo_persistence_manager.insert("environments", {"project": "project1", "env": "DEV",
                                                      "cluster": test_cluster_name_prefix + "1"}, False)
    mongo_persistence_manager.insert("environments", {"project": "project1", "env": "QA",
                                                  "cluster": test_cluster_name_prefix + "1"}, False)
    mongo_persistence_manager.insert("environments", {"project": "project2", "env": "DEV",
                                                  "cluster": test_cluster_name_prefix + "2"}, False)
    mongo_persistence_manager.insert("environments", {"project": "project2", "env": "QA",
                                                  "cluster": test_cluster_name_prefix + "3"}, False)


def get_env_details_from_db(filter_json):
    mongo_persistence_manager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
    mongo_persistence_manager.connect()
    results = mongo_persistence_manager.find("environments", filter_json)
    return results


def get_env_count_by_cluster(results):
    """
    gets environment count by cluster
    :param results: database query results - contains environment-cluster mappings
    :return:
    """
    env_count = {}
    for record in results:
        cluster_name = record["cluster"]
        try:
            env_count[cluster_name] = env_count[cluster_name] + 1
        except KeyError:
            env_count[cluster_name] = 1
    return env_count


def validate_allocation_results(expected_count, actual_results, expected_results):
    """
    validates allocation results
    :param expected_count: no. of environments which should have been allocated
    :param actual_results: actual allocation results from database
    :param expected_results: expected results (dictionary)
    :return:
    """

    assert len(actual_results) == expected_count, "Test failed: incorrect number of environments allocated"
    count = 1
    for record in actual_results:
        assert record["project"] == expected_results["project"], "Test failed. Incorrect project name"
        assert record["env"] in expected_results["env"], "Test failed. Incorrect environment name"
        try:
            cluster_prefix = expected_results["cluster_prefix"]
            assert record["cluster"] == cluster_prefix + str(count), "Test failed. Incorrect cluster name"
        except KeyError:
            assert record["cluster"] == expected_results["cluster"], "Test failed. Incorrect cluster name"
        count += 1


def test_add_env1():
    """
    Test case for add_env method of EnvManager class - normal addition
    :return:
    """
    try:
        # normal addition
        # setup clusters
        setup_clusters(1)

        # allocate environment
        env_manager = EnvManager()
        env_manager.add_env(test_project_name, "DEV", test_cluster_name_prefix + "1")

        # check database to see if record is present
        check_json = {"project": test_project_name}
        results = get_env_details_from_db(check_json)
        expected_results = {"project": test_project_name, "env": ["DEV"], "cluster": test_cluster_name_prefix + "1"}
        validate_allocation_results(1, results, expected_results)

    except UnsuccessfulConnectionException:
        assert False, "test_add_env1 failed: Database connection failure"
    except UnsuccessfulOperationException:
        assert False, "test_add_env1 failed: Database operation failure"
    except XprExceptions:
        assert False, "test_add_env1 failed: Unable to add environments"


def test_allocate_env1():
    """
    Test case for EnvManager method allocate_env - normal allocation with 3 unloaded clusters available
    :return:
    """
    try:
        # normal allocation - 3 clusters present
        # setup clusters
        setup_clusters(3)

        # allocate environments
        env_manager = EnvManager()
        env_manager.allocate_env(test_project_name, ["DEV", "QA", "PROD"])

        # check database to see if all environments allocated to cluster
        check_json = {"project": test_project_name}
        results = get_env_details_from_db(check_json)

        # each cluster should have 1 environment allocated
        expected_results = {"project": test_project_name, "env": ["DEV", "QA", "PROD"], "cluster_prefix":
                                test_cluster_name_prefix}
        validate_allocation_results(3, results, expected_results)

    except UnsuccessfulConnectionException:
        assert False, "test_allocate_env1 failed: Database connection failure"
    except UnsuccessfulOperationException:
        assert False, "test_allocate_env1 failed: Database operation failure"
    except XprExceptions:
        assert False, "test_allocate_env1 failed: Unable to add environments"


def test_allocate_env2():
    """
    Test case for EnvManager allocate_env method - normal allocation with 3 pre-loaded clusters present
    :return:
    """
    try:
        # normal allocation - 3 clusters present with unequal load (1 cluster has 1 more environments allocated)
        # set up clusters
        setup_clusters(3)
        setup_clusters_and_environments(3)

        # allocate environments
        env_manager = EnvManager()
        env_manager.allocate_env(test_project_name, ["DEV", "QA", "PROD"])

        # check database to see if environment allocated to cluster - each should get 1
        results = get_env_details_from_db({})

        assert len(results) == 7, "test_allocate_env2 failed: incorrect number of environments allocated"
        # 1 cluster should have 3 environments allocated, others should have 2
        env_count = get_env_count_by_cluster(results)
        for key in env_count.keys():
            if env_count[key] not in (2, 3):
                assert False, "Incorrect number of environments assigned to cluster " + key

    except UnsuccessfulConnectionException:
        assert False, "test_allocate_env2 failed: Database connection failure"
    except UnsuccessfulOperationException:
        assert False, "test_allocate_env2 failed: Database operation failure"
    except XprExceptions:
        assert False, "test_allocate_env2 failed: Unable to add environments"


def test_allocate_env3():
    """
    Test case for EnvManager allocate_env method - allocation with no cluster available
    :return:
    """
    try:
        # no cluster available
        setup_clusters(0)

        # try to allocate environment
        env_manager = EnvManager()
        env_manager.allocate_env(test_project_name, ["DEV", "QA", "PROD"])

        # exception should be raised
        assert False, "Allocation succeeded though no clusters present"
    except NoClustersPresentException:
        assert True, "test_allocate_env3 successful"


def test_allocate_env4():
    """
    Test case for EnvManager allocate_env method - invalid environment name
    :return:
    """
    try:
        # invalid environment
        # set up cluster
        setup_clusters(1)

        # try to allocate
        env_manager = EnvManager()
        env_manager.allocate_env(test_project_name, ["DEV", "QA", "JUNK"])

        # exception should be raised
        assert False, "Allocation succeeded though environment invalid"
    except InvalidEnvironmentException:
        assert True, "test_allocate_env4 successful"


def test_allocate_env5():
    """
    Test case for EnvManager allocate_env method - no environment specified
    :return:
    """
    # no environments specified - none should be allocated

    # set up cluster
    setup_clusters(1)

    # try to allocate environment
    env_manager = EnvManager()
    env_manager.allocate_env(test_project_name, [])

    # check database to see that none allocated
    results = get_env_details_from_db({})
    assert len(results) == 0, "test_allocate_env5 failed: incorrect number of environments allocated"


def test_allocate_env6():
    """
    Test Case for EnvManager allocate_env method - No environments specified
    :return:
    """
    # no environments specified - none should be allocated

    # set up clusters
    setup_clusters(1)

    # try to allocate environments
    env_manager = EnvManager()
    env_manager.allocate_env(test_project_name, [])

    # check database to see that none allocated
    results = get_env_details_from_db({})
    assert len(results) == 0, "test_allocate_env6 failed: incorrect number of environments allocated"


def test_allocate_env7():
    """
    Test case for EnvManager allocate_env method - 3 environments specified, of which 2 already exist
    :return:
    """
    # 3 environments specified - 2 already exist, only 3rd should get created

    # set up clusters
    setup_clusters(1)

    # allocate environments
    env_manager = EnvManager()
    # allocate Dev and QA
    env_manager.allocate_env(test_project_name, ["DEV", "QA"])

    # now, try to allocate 3 of which 2 already exist
    env_manager.allocate_env(test_project_name, ["DEV", "QA", "PROD"])

    # check database to see that 3 environments allocated
    results = get_env_details_from_db({})
    expected_results = {"project": test_project_name, "env": ["DEV", "QA", "PROD"],
                        "cluster": test_cluster_name_prefix + "1"}
    validate_allocation_results(3, results, expected_results)


def test_remove_env1():
    """
    Test case for EnvManager remove_env method - normal removal
    :return:
    """
    # normal removal
    # set up clusters
    setup_clusters(1)

    # allocate environments
    env_manager = EnvManager()
    env_manager.add_env(test_project_name, "QA", test_cluster_name_prefix + "1")

    # remove QA environment
    env_manager.remove_env(test_project_name, "QA")

    # check database to see if record removed
    check_json = {"project": test_project_name}
    results = get_env_details_from_db(check_json)
    assert len(results) == 0, "test_remove_env1 failed: Environment not removed"


def test_remove_env2():
    """
    Test case for EnvManager remove_env method - remove environment which is not present
    :return:
    """
    # environment not present
    # set up clusters
    setup_clusters(1)

    # allocate environments
    env_manager = EnvManager()
    env_manager.add_env(test_project_name, "QA", test_cluster_name_prefix + "1")

    # remove non-existent environment
    env_manager.remove_env(test_project_name, "JUNK")

    check_json = {"project": test_project_name}
    results = get_env_details_from_db(check_json)
    expected_results = {"project": test_project_name, "env": ["QA"], "cluster": test_cluster_name_prefix + "1"}
    validate_allocation_results(1, results, expected_results)


def test_remove_env3():
    """
    Test case for EnvManager method remove_env - rmove all environments associated with a project
    :return:
    """
    # remove all environments for a project

    # set up clusters
    setup_clusters(1)

    # allocate environments
    env_manager = EnvManager()
    env_manager.allocate_env(test_project_name, ["DEV", "QA", "PROD"])

    # check database to see that they were allocated
    check_json = {"project": test_project_name}
    results = get_env_details_from_db(check_json)

    # check results
    assert len(results) == 3, "test_remove_env3 failed: Environments not allocated correctly"

    # remove all environments
    env_manager.remove_env(test_project_name)

    # check results again
    check_json = {"project": test_project_name}
    results = get_env_details_from_db(check_json)
    assert len(results) == 0, "Environments not removed"


def test_get_cluster1():
    """
    Tet case for EnvManager method get_clusters - normal operation
    :return:
    """
    # normal operation
    # setup clusters
    setup_clusters(2)

    # allocate environments
    env_manager = EnvManager()
    env_manager.allocate_env(test_project_name, ["QA", "PROD"])

    # get name of cluster associated with a specific environment
    qa_cluster_name = env_manager.get_cluster(test_project_name, "QA")

    # get name of cluster allocated to environment from database
    check_json = {"project": test_project_name, "env": "QA"}
    results = get_env_details_from_db(check_json)
    assert len(results) == 1, "test_get_cluster1 failed: environments not allocated correctly"
    assert results[0]["cluster"] == qa_cluster_name, "Incorrect cluster name obtained"


def test_get_cluster2():
    """
    Test case for EnvManager get_cluster method - environment requested not present
    :return:
    """
    # environment not present - null should be returned

    # set up clusters
    setup_clusters(2)

    # allocate PROD environment
    env_manager = EnvManager()
    env_manager.allocate_env(test_project_name, ["PROD"])

    # get cluster for QA environment
    qa_cluster_name = env_manager.get_cluster(test_project_name, "QA")

    assert qa_cluster_name is None, "Incorrect cluster name obtained"


def test_validate_deployment_target_env1():
    """
    Test case for validate_deployment_target_env method of EnvManager class - first deployment
    :return:
    """
    try:
        # first deployment

        # set up clusters
        setup_clusters(1)

        # deployment to DEV, no other depoyments present, 3 targets available for project
        env_manager = EnvManager()
        input_project = {"name": test_project_name, "target_environment": "DEV"}
        db_project = {"environments": ["DEV", "QA", "PROD"]}
        env_manager.validate_deployment_target_env(input_project, db_project)
        assert True, "test_validate_deployment_target_env1 successful"
    except IncorrectDeploymentException:
        assert False, "test_validate_deployment_target_env1 failed: Correct deployment flagged as incorrect"


def test_validate_deployment_target_env2():
    """
    Test case for validate_deployment_target_env method of EnvManager class - deployment to a higher env after
    deployment to lower env
    :return:
    """
    try:
        # second deployment after deployment to a lower environment done

        # set up clusters
        setup_clusters(1)

        # deployment to QA, prior deployment to DEV, 3 targets available for project
        env_manager = EnvManager()
        input_project = {"name": test_project_name, "target_environment": "QA"}
        db_project = {"environments": ["DEV", "QA", "PROD"], "deployedEnvironments": ["DEV"]}
        env_manager.validate_deployment_target_env(input_project, db_project)
        assert True, "test_validate_deployment_target_env1 successful"
    except IncorrectDeploymentException:
        assert False, "test_validate_deployment_target_env1 failed: Correct deployment flagged as incorrect"


def test_validate_deployment_target_env3():
    """
        Test case for validate_deployment_target_env method of EnvManager class - deployment to a higher env without
    deployment to lower env

    :return:
    """
    try:
        # second deployment without deployment to a lower environment

        # set up clusters
        setup_clusters(1)

        # deployment to PROD, prior deployment only to DEV (not QA), 3 targets available for project
        env_manager = EnvManager()
        input_project = {"name": test_project_name, "target_environment": "PROD"}
        db_project = {"environments": ["DEV", "QA", "PROD"], "deployedEnvironments": ["DEV"]}
        env_manager.validate_deployment_target_env(input_project, db_project)
        assert False, "test_validate_deployment_target_env1 failed: Incorrect deployment flagged as correct"
    except IncorrectDeploymentException:
        assert True, "test_validate_deployment_target_env1 successful"


def test_deactivate_cluster1():
    """
    Test for deactivate_cluster method of EnvManager - normal operation
    :return:
    """
    # set up 2 clusters
    setup_clusters(2)

    # allocate 4 environments across the 2 clusters
    env_manager = EnvManager()
    env_manager.allocate_env(test_project_name, ["DEV", "INT", "QA", "PROD"])

    # check if the environments were allocated - 2 to each cluster
    results = get_env_details_from_db({})
    assert len(results) == 4, "test_deactivate_cluster1 failed. Environments not allocated"

    # deactivate one of the clusters
    # simulate deactivation
    mongo_persistence_manager = MongoPersistenceManager(url=MONGOURL, db=MONGODB, uid=MONGOUID, pwd=MONGOPWD)
    mongo_persistence_manager.connect()
    mongo_persistence_manager.update("clusters", {"name": test_cluster_name_prefix + "1"}, {"activationStatus": False})

    env_manager.deactivate_cluster(test_cluster_name_prefix + "1")

    # check database - all environments should have moved to the remaining cluster
    results = get_env_details_from_db({})
    expected_results = {"project": test_project_name, "env": ["DEV", "INT", "QA", "PROD"],
                        "cluster": test_cluster_name_prefix + "2"}
    validate_allocation_results(4, results, expected_results)
