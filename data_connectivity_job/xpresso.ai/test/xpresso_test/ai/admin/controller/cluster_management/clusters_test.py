from xpresso.ai.admin.controller.client.controller_client \
    import ControllerClient
from xpresso.ai.admin.controller.persistence.mongopersistencemanager import \
    MongoPersistenceManager
from xpresso.ai.admin.controller.utils import error_codes
from xpresso.ai.admin.controller.cluster_management.xpr_clusters \
    import XprClusters
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

config_path = XprConfigParser.DEFAULT_CONFIG_PATH
config = XprConfigParser(config_path)

MONGO_SECTION = 'mongodb'
URL = 'mongo_url'
DB = 'database'
UID = 'mongo_uid'
PWD = 'mongo_pwd'
W = 'w'
mongo_persistence_manager = MongoPersistenceManager(
                              url=config[MONGO_SECTION][URL],
                              db=config[MONGO_SECTION][DB],
                              uid=config[MONGO_SECTION][UID],
                              pwd=config[MONGO_SECTION][PWD],
                              w=config[MONGO_SECTION][W])
client = ControllerClient()
clusters = XprClusters(persistence_manager=mongo_persistence_manager)

"""
    wrong_token = 111
    expired_token = 112
    permission_denied = 113
    
    cluster_not_found = 121
    cluster_already_exists = 122
    
    True : Success
    False : Failed
"""

# REGISTER CLUSTER TESTS #


def test1():    # register new cluster (all permissions okay)

    print("Starting test1")
    try:
        client.login("admin1", "admin1")
        result = client.register_cluster({"name": "test"})
        assert result is True, "test1 failed."
        print("test1 successful")
        client.logout()
    except:
        print("Test1 failed")


def test2():    # register new cluster without user login

    print("Starting test2")
    try:
        client.logout()
        result = client.register_cluster({"name": "test"})
        assert result is None, "test2 failed."
        print("test2 successful")
    except:
        print("test2 failed")


def test3():    # register new cluster with wrong token
    print("Starting test3")
    try:
        client.login("admin1", "admin1")
        path = '/home/abzooba/bin/.xpr/.current'
        file = open(path, 'a')
        file.write('abcdef')
        file.close()
        result = client.register_cluster({"name": "test"})
        assert result is error_codes.wrong_token, "test3 failed."
        print("test3 successful")
    except:
        print("test3 failed")


def test4():    # register new cluster without permission
    print("Starting test4")
    try:
        client.login("user1", "user1")
        result = client.register_cluster({"name": "test"})
        assert result is error_codes.permission_denied, "test4 failed."
        print("test4 successful")
        client.logout()
    except:
        print("test4 failed")


def test5():    # register existing cluster
    print("Starting test5")
    try:
        client.login("admin1", "admin1")
        result = client.register_cluster({"name": "test"})
        assert result is error_codes.cluster_already_exists, "test5 failed."
        print("test5 successful")
        client.logout()
    except:
        print("test5 failed")


# GET CLUSTER TESTS #


def test6():    # get all clusters (all checks okay)
    print("Starting test6")
    try:
        client.login("admin1", "admin1")
        result = client.get_clusters({})
        assert result is True, "test6 failed."
        print("test6 successful")
        client.logout()
    except:
        print("test6 failed")


def test7():    # get specific cluster (all checks okay)
    print("Starting test7")
    try:
        client.login("admin1", "admin1")
        result = client.get_clusters({"name": "test"})
        assert result is True, "test7 failed."
        print("test7 successful")
        client.logout()
    except:
        print("test7 failed")


def test8():    # get cluster without user login
    print("Starting test8")
    try:
        client.logout()
        result = client.get_clusters({"name": "test"})
        assert result is None, "test8 failed."
        print("test8 successful")
    except:
        print("test8 failed")


def test9():    # get cluster with wrong token
    print("Starting test9")
    try:
        client.login("admin1", "admin1")
        path = '/home/abzooba/bin/.xpr/.current'
        file = open(path, 'a')
        file.write('abcdef')
        file.close()
        result = client.get_clusters({"name": "test"})
        assert result is error_codes.wrong_token, "test9 failed."
        print("test9 successful")
    except:
        print("test9 failed")


def test10():    # get cluster without permission
    print("Starting test10")
    try:
        client.login("user1", "user1")
        result = client.get_clusters({"name": "test"})
        assert result is error_codes.permission_denied, "test10 failed."
        print("test10 successful")
        client.logout()
    except:
        print("test10 failed")


def test11():    # get non-existing cluster

    print("Starting test11")
    try:
        client.login("admin1", "admin1")
        result = client.get_clusters({"name": "test1111"})
        assert result is error_codes.cluster_not_found, "test11 failed."
        print("test11 successful")
        client.logout()
    except:
        print("test11 failed")


# DELETE CLUSTER TESTS #


def test12():    # deactivate specific cluster (all checks okay)
    print("Starting test12")
    try:
        client.login("admin1", "admin1")
        result = client.deactivate_cluster({"name": "test"})
        assert result is True, "test12 failed."
        print("test12 successful")
        client.logout()
    except:
        print("test12 failed")


def test13():    # deactivate cluster without user login
    print("Starting test12")
    try:
        result = client.deactivate_cluster({"name": "test"})
        assert result is None, "test13 failed."
        print("test13 successful")
    except:
        print("test13 failed")


def test14():    # deactivate cluster with wrong token
    print("Starting test14")
    try:
        client.login("admin1", "admin1")
        path = '/home/abzooba/bin/.xpr/.current'
        file = open(path, 'a')
        file.write('abcdef')
        file.close()
        result = client.deactivate_cluster({"name": "test"})
        assert result is error_codes.wrong_token, "test14 failed."
        print("test14 successful")
    except:
        print("test14 failed")


def test15():    # deactivate cluster without permission
    print("Starting test15")
    try:
        client.login("user1", "user1")
        result = client.deactivate_cluster({"name": "test"})
        assert result is error_codes.permission_denied, "test15 failed."
        print("test15 successful")
        client.logout()
    except:
        print("test15 failed")


def test16():    # deactivate non-existing cluster

    print("Starting test16")
    try:
        client.login("admin1", "admin1")
        result = client.deactivate_cluster({"name": "test11111"})
        assert result is error_codes.cluster_not_found, "test16 failed."
        print("test16 successful")
        client.logout()
    except:
        print("test16 failed")
