"""
Test suite for local shell executor
"""
import os

from xpresso.ai.admin.infra.bundles.local_shell_executor import LocalShellExecutor
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import CommandExecutionFailedException


def test_success_execute_with_output():
    executor = LocalShellExecutor()
    echo_string = "helloworld"
    (return_code, stdout, stderr) = executor.execute_with_output(command=f"echo {echo_string}")
    assert return_code == 0
    assert echo_string in str(stdout)


def test_failure_execute_with_output():
    executor = LocalShellExecutor()
    echo_string = "helloworld"
    try:
        (return_code, stdout, stderr) = executor.execute_with_output(command=f"echo{echo_string}")
        assert return_code != 0
    except CommandExecutionFailedException:
        assert True


def test_success_execute():
    executor = LocalShellExecutor()
    sample_file = "test_execute.test"
    return_code = executor.execute(command=f"touch {sample_file}")
    assert return_code == 0
    assert os.path.exists(sample_file)
    os.remove(sample_file)


def test_failure_execute():
    executor = LocalShellExecutor()
    sample_file = "test_execute.test"
    try:
        rc = executor.execute(command=f"touch{sample_file}")
        assert rc != 0
    except CommandExecutionFailedException:
        assert True


def test_success_execute_same_shell():
    executor = LocalShellExecutor()
    test_env = "123"
    return_code, stdout, stderr = executor.execute_same_shell(command=f"export TEST={test_env}")
    assert return_code == 0
    return_code, stdout, stderr = executor.execute_with_output(command=f"echo $TEST")
    assert return_code == 0


def test_failure_execute_same_shell():
    executor = LocalShellExecutor()
    try:
        rc, stdout, stderr = executor.execute_same_shell(command=f"la")
        assert len(stdout) == 0
    except CommandExecutionFailedException:
        assert True
