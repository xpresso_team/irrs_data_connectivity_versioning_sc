"""
Test suite for bundle manager
"""
from xpresso.ai.admin.infra import bundles
from xpresso.ai.admin.infra.bundles.bundle_manager import BundleManager
from xpresso.ai.admin.infra.bundles.bundle_manager import ExecutionType
from xpresso.ai.admin.infra.bundles.ubuntu.basevm.docker_bundle import DockerBundle


def test_bundle_manager_list():
    config_path = "config/test/logger_test_config.json"
    bundle_manager = BundleManager(config_path=config_path)
    bundle_list = bundle_manager.list()
    assert {"NFSBundle","BaseUbuntuBundle", "DockerDistributionBundle", "DockerBundle", "PythonBundle",
            "AptRepositoryBundle", "BaseVMBundle", "DevelopmentVMBundle"} == set(bundle_list)


def test_bundle_manager_run():
    config_path = "config/test/logger_test_config.json"
    bundle_manager = BundleManager(config_path=config_path)
    assert bundle_manager.run("EmptyBundle", ExecutionType.INSTALL) is False


def test_bundle_str_to_class():
    config_path = "config/test/logger_test_config.json"
    bundle_manager = BundleManager(config_path=config_path)
    assert bundle_manager.bundle_str_to_class("EmptyBundle", bundles) is None
    assert bundle_manager.bundle_str_to_class("DockerBundle", bundles) is DockerBundle
