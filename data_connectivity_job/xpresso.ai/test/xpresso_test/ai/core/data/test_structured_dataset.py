""" This contains the test cases for dataset objects """

import os
import pandas as pd

from xpresso.ai.admin.controller.authentication.authenticationmanager import \
    AuthenticationManager
from xpresso.ai.admin.controller.client.controller_client import \
    ControllerClient
from xpresso.ai.admin.controller.exceptions.xpr_exceptions import \
    SerializationFailedException, DeserializationFailedException, \
    ControllerClientResponseException
from xpresso.ai.admin.controller.persistence import persistentce_connection
from xpresso.ai.core.data.structured_dataset import StructuredDataset
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

data_path = "./config/test/data/test.csv"
data_path_invalid = "./config/test/data/test_invalid.csv"
config = XprConfigParser()


def test_import_dataset_structured_success():
    """ Check if dataset can be imported """
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    assert len(dataset.data) > 0


def test_import_dataset_structured_failure():
    """ Check if dataset can be imported """
    dataset = StructuredDataset()
    try:
        dataset.import_dataset(data_path_invalid)
        print(dataset.data)
        assert False
    except pd.errors.ParserError:
        assert True


def test_serialization_deserialization_structured_success():
    """ Check dataset serialize/deserialize success """
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    try:
        test_serialized_dataset = dataset.serialize()
        test_deserialized_dataset = dataset.deserialize(test_serialized_dataset)
        # Check if returned dataset is equal
        assert test_deserialized_dataset.data.equals(dataset.data)
        empty_dataset = StructuredDataset()
        _ = empty_dataset.deserialize(test_serialized_dataset, update_self=True)
        # Check if dataset itself is updated and equal
        assert dataset.data.equals(empty_dataset.data)
    except (SerializationFailedException, DeserializationFailedException):
        assert False


def test_serialization_structured_failure():
    """ Fail dataset serialization """
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    try:
        dataset.temp = XprLogger()
        _ = dataset.serialize()
        assert False
    except (SerializationFailedException, DeserializationFailedException):
        assert True


def test_deserialization_structured_failure():
    """ Fail dataset desiaralization """
    dataset = StructuredDataset()
    try:
        test_deserialized_dataset = b"some_random_character_to_fail_deserialize"
        _ = dataset.deserialize(test_deserialized_dataset)
        assert False
    except DeserializationFailedException:
        assert True


def authenticate():
    authentication_manager = AuthenticationManager(
        persistentce_connection.create_persistence_object(config=config))
    resp = authentication_manager.login(credentials={"uid": "admin1",
                                                     "pwd": "admin1"})
    client = ControllerClient()
    client.save_token(resp.results["access_token"])


def test_save_structured_success():
    """ Success  dataset save """
    authenticate()
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    try:
        pickle_file = dataset.save()
        assert os.path.exists(pickle_file)
        assert os.stat(pickle_file).st_size > 0
    except (SerializationFailedException, DeserializationFailedException):
        assert False


def test_save_structured_failed():
    """ Failed  dataset save """
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    try:
        dataset.temp = XprLogger()
        _ = dataset.save()
        assert False
    except (ControllerClientResponseException,
            SerializationFailedException, DeserializationFailedException):
        assert True


def test_load_structured_success():
    """ Success dataset  load"""
    authenticate()
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    try:
        dataset.load()
        assert not dataset.data.empty
    except (SerializationFailedException, DeserializationFailedException):
        assert False


def test_load_structured_fail():
    """ Failed dataset load"""
    authenticate()
    dataset = StructuredDataset()
    dataset.import_dataset(data_path)
    try:
        dataset.load(pickle_file_name="/random_path")
        assert False
    except (FileNotFoundError,
            SerializationFailedException, DeserializationFailedException):
        assert True
