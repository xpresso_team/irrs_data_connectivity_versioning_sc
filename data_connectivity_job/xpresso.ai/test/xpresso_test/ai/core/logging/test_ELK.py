import requests
import  urllib.parse
from xpresso.ai.core.utils.xpr_config_parser import XprConfigParser

def uri_validator(x):
    try:
        result = urllib.parse.urlparse(x)
        return all([result.scheme, result.netloc, result.path])
    except:
        return False

def test_elastic_search():
    """

    Returns:
        asserts True if the elastic search instance is running else asserts False

    """
    default_test_config_path = "/opt/xpresso.ai/config/test/logger_test_config.json"
    config = XprConfigParser(default_test_config_path)

    elastic_section = "elastic_search"
    url= "url"
    base_url = config[elastic_section][url]
    endpoint = base_url
    # endpoint = urllib.parse.urljoin(base_url)
    if uri_validator(endpoint):
        status_code =  requests.get(endpoint).status_code
        assert status_code is 200, "Elastic Search Test Case Passed."
    else:
        assert False, "Invalid Elastic Search URL Passed"

def test_kibana():
    """

    Returns:
        asserts True if the kibana server is running else asserts False

    """
    default_test_config_path = "/opt/xpresso.ai/config/test/logger_test_config.json"
    config = XprConfigParser(default_test_config_path)

    kibana_section = "kibana"
    url= "url"

    base_url = config[kibana_section][url]

    endpoint = urllib.parse.urljoin(base_url,"app/kibana")
    if uri_validator(endpoint):
        status_code =  requests.get(endpoint).status_code
        assert status_code is 200, "Kibana server test case passed."
    else:
        assert False, "Invalid Kibana URL Passed."

def test_logstash():
    """

    Returns:
        asserts True if the logstash server is running else asserts False

    """
    default_test_config_path = "/opt/xpresso.ai/config/test/logger_test_config.json"
    config = XprConfigParser(default_test_config_path)

    logstash_section = "logstash"
    url= "url"
    base_url = config[logstash_section][url]
    endpoint = urllib.parse.urljoin(base_url,"?pretty")

    if uri_validator(endpoint):
        status_code =  requests.get(endpoint).status_code
        assert status_code is 200, "Logstash server test case passed."
    else:
        assert False, "Invalid Logstash URL Passed."


if __name__ == "__main__":
    test_elastic_search()
    test_kibana()
    test_logstash()